#include "Pass.h"

Pass::Pass() : pixelShader(NULL), vertexShader(NULL), geometryShader(NULL) { }

Pass::~Pass() { 
	Shutdown(); 
}

void Pass::Shutdown() {
	if(pixelShader != NULL) {
		delete pixelShader;
		pixelShader = NULL;
	}

	if(vertexShader != NULL) {
		delete vertexShader;
		vertexShader = NULL;
	}

	if(geometryShader != NULL) {
		delete geometryShader;
		geometryShader = NULL;
	}
}

void Pass::SetPixelShader(Shader * pixelShader) {
	this->pixelShader = pixelShader;
}

Shader * Pass::GetPixelShader() {
	return pixelShader;
}

void Pass::SetVertexShader(Shader * vertexShader) {
	this->vertexShader = vertexShader;
}

Shader * Pass::GetVertexShader() {
	return vertexShader;
}

void Pass::SetGeometryShader(Shader * GeometryShader) {
	this->geometryShader = GeometryShader;
}

Shader * Pass::GetGeometryShader() {
	return geometryShader;
}

void Pass::Activate(ID3D11DeviceContext * dX11DeviceContext) {
	if(vertexShader == NULL)
		dX11DeviceContext->VSSetShader(0, 0, 0);
	else
		vertexShader->Activate(dX11DeviceContext);

	if(geometryShader == NULL)
		dX11DeviceContext->GSSetShader(0, 0, 0);
	else
		geometryShader->Activate(dX11DeviceContext);

	if(pixelShader == NULL)
		dX11DeviceContext->PSSetShader(0, 0, 0);
	else
		pixelShader->Activate(dX11DeviceContext);
}

void Pass::Deactivate(ID3D11DeviceContext * dX11DeviceContext) {
	if(vertexShader != NULL)
		vertexShader->Deactivate(dX11DeviceContext);

	if(geometryShader != NULL)
		geometryShader->Deactivate(dX11DeviceContext);

	if(pixelShader != NULL)
		pixelShader->Deactivate(dX11DeviceContext);
}

void Pass::PreDraw(ID3D11DeviceContext * dX11DeviceContext) {
	if(vertexShader != NULL)
		vertexShader->SetShaderData(dX11DeviceContext);

	if(geometryShader != NULL)
		geometryShader->SetShaderData(dX11DeviceContext);

	if(pixelShader != NULL)
		pixelShader->SetShaderData(dX11DeviceContext);
}