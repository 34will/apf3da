#include "PixelShader.h"

PixelShader::PixelShader(WCHAR * file, LPCSTR entryPoint, LPCSTR version) : Shader(file, entryPoint, version), shader(NULL) { }

PixelShader::~PixelShader() {
	if(shader)
		shader->Release();
}

HRESULT PixelShader::Initialise(ID3D11Device * dX11Device) {
	ID3DBlob * shaderBlob = NULL;
	HRESULT hr = Shader::CompileShaderFromFile(file, entryPoint, version, &shaderBlob);

	if(FAILED(hr)) {
		MessageBox(NULL, L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return hr;
	}

	hr = dX11Device->CreatePixelShader(shaderBlob->GetBufferPointer(), shaderBlob->GetBufferSize(), NULL, &shader);
	shaderBlob->Release();

	if(FAILED(hr))
		return hr;

	hr = GenerateConstantBuffers(dX11Device);

	if(FAILED(hr))
		return hr;

	return hr;
}

ID3D11PixelShader * PixelShader::getShaderHandle() { return shader; }

void PixelShader::Activate(ID3D11DeviceContext * dX11DeviceContext) {
	dX11DeviceContext->PSSetShader(shader, NULL, 0);
}

void PixelShader::SetConstantBuffers(ID3D11DeviceContext * dX11DeviceContext, const unsigned int & StartSlot, const unsigned int & NumBuffers, ID3D11Buffer * const * constantBuffers) const {
	dX11DeviceContext->PSSetConstantBuffers(StartSlot, NumBuffers, constantBuffers);
}

void PixelShader::SetTextures(ID3D11DeviceContext * dX11DeviceContext, const unsigned int & StartSlot, const unsigned int & NumBuffers, ID3D11ShaderResourceView * const * srvs) const {
	dX11DeviceContext->PSSetShaderResources(StartSlot, NumBuffers, srvs);
}

PixelShader& PixelShader::operator=(const PixelShader& other) {
	shader = other.shader;
	return *this;
}
