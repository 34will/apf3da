#ifndef PASS_H
#define PASS_H

#include <d3d11.h>
#include <vector>
#include "Shader.h"

using namespace std;

class Pass {
private:
	Shader * pixelShader, * vertexShader, * geometryShader;

public:
	Pass();
	~Pass();

	void Shutdown();

	void SetPixelShader(Shader * pixelShader);
	Shader * GetPixelShader();

	void SetVertexShader(Shader * vertexShader);
	Shader * GetVertexShader();

	void SetGeometryShader(Shader * GeometryShader);
	Shader * GetGeometryShader();

	void Activate(ID3D11DeviceContext * dX11DeviceContext);
	void Deactivate(ID3D11DeviceContext * dX11DeviceContext);

	void PreDraw(ID3D11DeviceContext * dX11DeviceContext);
};

#endif