#ifndef MESHLOADER_H
#define MESHLOADER_H

#include <d3d11.h>
#include <string>
#include "tinyxml2.h"

class Mesh;
class AnimatedMesh;

using namespace std;
using namespace tinyxml2;

class MeshLoader {
private:
	static HRESULT LoadMD5Anim(AnimatedMesh *& mesh, string const & animName, string const & fileName, unsigned int animNum, float scale);
	static HRESULT LoadFromMTL(Mesh * m, string const & path, string const & fileName);
	static HRESULT LoadMD5Mesh(Mesh *& outMesh, string const & fileName, float scale, XMLElement * meshConfig);
	static HRESULT LoadOBJMesh(Mesh *& outMesh, string const & fileName, float scale);

public:
	static HRESULT LoadMesh(Mesh *& outMesh, string const & fileName, float scale, XMLElement * meshConfig);
};

#endif