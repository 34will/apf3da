#ifndef MESH_STRUCTS_H
#define MESH_STRUCTS_H

#include <DirectXMath.h>
#include <string>
#include "Material.h"

using namespace std;
using namespace DirectX;

struct Vertex {
	XMFLOAT3 Position;
	XMFLOAT3 Normal;
	XMFLOAT3 Tangent;
	XMFLOAT2 TexCoord;

	Vertex() : Position(0.0f, 0.0f, 0.0f), Normal(0.0f, 0.0f, 0.0f), TexCoord(0.0f, 0.0f), Tangent(0.0f, 0.0f, 0.0f) { }
	Vertex(const XMFLOAT3 & Position, const XMFLOAT3 & Normal, const XMFLOAT2 & TexCoord) : Position(Position), Normal(Normal), TexCoord(TexCoord), Tangent(0.0f, 0.0f, 0.0f) { }
	Vertex(const XMFLOAT3 & Position, const XMFLOAT3 & Normal, const XMFLOAT2 & TexCoord, const XMFLOAT3 & Tangent) : Position(Position), Normal(Normal), TexCoord(TexCoord), Tangent(Tangent) { }
};

struct FSQVertex {
	XMFLOAT3 Position;
	XMFLOAT2 TexCoord;
	unsigned int Index;
};

struct SubObject {
	string matName;
	Material * material;
	unsigned int vertexStart, vertexCount;

	SubObject() : matName(""), material(NULL), vertexStart(0), vertexCount(0) { }
	SubObject(string matName, unsigned int  vertexStart, unsigned int  vertexCount) : matName(matName), material(NULL), vertexStart(vertexStart), vertexCount(vertexCount) { }
};

struct BoundingBox {
	float minX, maxX, minY, maxY, minZ, maxZ;

	float XLength() { return maxX - minX; }
	float YLength() { return maxY - minY; }
	float ZLength() { return maxZ - minZ; }
};

struct Joint {
	XMFLOAT3 Position;
	XMFLOAT4 Orientation;

	string Name;
	unsigned int ID, StartID;
	int ParentID, Flag;
};

struct MD5Vertex {
	/*XMFLOAT3 Position;
	XMFLOAT3 Normal;
	XMFLOAT3 Tangent;
	XMFLOAT2 TexCoord;*/

	unsigned int WeightStartID;
	unsigned int WeightCount;
};

struct Weight {
	unsigned int JointID;
	float Influence;

	XMFLOAT3 LocalPosition;
	XMFLOAT3 Normal;
};

struct Animation {
	string Name = "";
	Joint ** FrameJoints = NULL;
	BoundingBox * BoundingBoxes = NULL;

	unsigned int NumberOfJoints = 0, NumberOfFrames = 0, FrameRate = 0;
	float AnimationLength = 0.0f, AnimationFrameLength = 0.0f;
};

#endif