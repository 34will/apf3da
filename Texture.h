#ifndef TEXTURE_H
#define TEXTURE_H

#include <string>
#include <d3d11.h>

using namespace std;

struct Colour {
	float R, G, B, A;

	const static Colour NullColour;
	static bool IsNullColour(const Colour & c);

	Colour & operator=(const unsigned char & rhs);
};

class Texture {
protected:
	string path;

public:
	Texture(const string & path);
	virtual ~Texture();

	virtual bool Load() = 0;
	virtual void Cleanup();

	virtual unsigned int GetWidth() const = 0;
	virtual unsigned int GetHeight() const = 0;

	virtual Colour ** GetPixels() const = 0;
	virtual Colour GetPixel(const unsigned int & X, const unsigned int & Y) const = 0;
	virtual Colour GetLocalisedPixel(const float & X, const float & Y) const = 0;
};

#endif