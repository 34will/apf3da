#ifndef DX11_TEXTURE_H
#define DX11_TEXTURE_H

#include "Texture.h"

class DX11Texture : public Texture {
private:
	ID3D11Texture2D * resource;
	ID3D11ShaderResourceView * srv;

	bool CPUAccess;
	unsigned int width, height;
	Colour ** pixels;

public:
	DX11Texture();
	DX11Texture(const string & path, bool CPUAccess = false);

	bool Load();
	void Cleanup();

	unsigned int GetWidth() const;
	unsigned int GetHeight() const;

	Colour ** GetPixels() const;
	Colour GetPixel(const unsigned int & X, const unsigned int & Y) const;
	Colour GetLocalisedPixel(const float & X, const float & Y) const;

	ID3D11Texture2D * const GetTexture() const;
	ID3D11ShaderResourceView * const GetShaderResourceView() const;
	ID3D11ShaderResourceView * const * GetShaderResourceViewPtr() const;
};

#endif