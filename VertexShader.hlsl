cbuffer ProjectionBuffer : register(b0) {
	float4x4 Projection;
};

cbuffer ViewBuffer : register(b1) {
	float4x4 View;
};

cbuffer WorldBuffer : register(b2) {
	float4x4 World;
};

struct VIn {
	float3 position			: POSITION;
	float3 normal			: NORMAL;
	float3 tangent			: TANGENT;
	float2 texel			: TEXCOORD;
};

struct VOut {
	float4		Position	: SV_POSITION;
	float4		WVPosition	: WV_POSITION;
	float2		Texel		: TEXCOORD0;
	float3x3	TBN			: TBN;
};

VOut main(VIn input) {
	VOut output = (VOut)0;

	float4 pos = float4(input.position, 1.0f);
	pos = mul(pos, transpose(World));
	pos = mul(pos, transpose(View));
	output.WVPosition = pos;

	pos = mul(pos, transpose(Projection));
	output.Position = pos;

	float3 Binormal = cross(input.normal, input.tangent);

	output.TBN[0] = mul(input.tangent, transpose((float3x3)World));
	output.TBN[0] = normalize(output.TBN[0]);

	output.TBN[1] = mul(Binormal, transpose((float3x3)World));
	output.TBN[1] = normalize(output.TBN[1]);

	output.TBN[2] = mul(input.normal, transpose((float3x3)World));
	output.TBN[2] = normalize(output.TBN[2]);

	output.Texel = input.texel;

	return output;
}