#include "RotateComponent.h"

RegisterComponentType(RotateComponent);

RotateComponent::RotateComponent() : Component(), transform(NULL), speed(0) { }

RotateComponent::~RotateComponent() { }

bool RotateComponent::Initialise(XMLElement * init) {
	if(init == NULL)
		return false;

	XMLError err = init->QueryFloatAttribute("speed", &speed);
	if(err != XML_SUCCESS)
		return false;

	transform = sceneObject->GetComponent<TransformComponent>();

	return true;
}

void RotateComponent::Start() { }

void RotateComponent::Shutdown() { }

void RotateComponent::Update(float dt) {
	transform->LocalRotate(0, dt * speed, 0, false);
}

string RotateComponent::GetTypename() const {
	return "RotateComponent";
}