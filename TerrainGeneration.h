#ifndef TERRAIN_GENERATION_H
#define TERRAIN_GENERATION_H

#include "Mesh.h"
#include "tinyxml2.h"
#include "Parser.h"
#include "Material.h"

using namespace tinyxml2;

enum class TerrainGenerationType {
	None,
	Normal,
	Random,
	Heightmap,
	DiamondSquare,
	FaultLine,
	PerlinNoise
};

struct PlaneData {
	float width, depth;
	unsigned int numRows, numCols;

	float dX, dZ, halfWidth, halfDepth;
	unsigned int m, n;
};

class TerrainGeneration {
private:
	static const Parser<TerrainGenerationType> tgtParser;

	static void DiamondSquare(Vertex ** vertices, unsigned int xLength, unsigned int yLength, float heightScale, float offset);

	static bool GenerateNormalPlane(Mesh * mesh, XMLElement * config, Material * const materials, const unsigned int & numMaterials, PlaneData * outPlaneData = NULL);
	static bool GenerateRandomPlane(Mesh * mesh, XMLElement * config, Material * const materials, const unsigned int & numMaterials);
	static bool GenerateHeightmapPlane(Mesh * mesh, XMLElement * config, Material * const materials, const unsigned int & numMaterials);
	static bool GenerateDiamondSquarePlane(Mesh * mesh, XMLElement * config, Material * const materials, const unsigned int & numMaterials);
	static bool GenerateFaultLinePlane(Mesh * mesh, XMLElement * config, Material * const materials, const unsigned int & numMaterials);
	static bool GeneratePerlinNoisePlane(Mesh * mesh, XMLElement * config, Material * const materials, const unsigned int & numMaterials);

	static bool GenerateNormalCubeSphere(Mesh * mesh, XMLElement * config, Material * const materials, const unsigned int & numMaterials);
	static bool GeneratePerlinNoiseCubeSphere(Mesh * mesh, XMLElement * config, Material * const materials, const unsigned int & numMaterials);

public:
	static bool GenerateTerrainMesh(Mesh * mesh, XMLElement * config, Material * const materials, const unsigned int & numMaterials);

	static TerrainGenerationType StringToGenType(const string & type);
};

#endif