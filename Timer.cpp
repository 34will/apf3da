#include "Timer.h"

Timer::Timer(bool startTimer) {
	ZeroMemory(&frequency, sizeof(LARGE_INTEGER));
	QueryPerformanceFrequency(&frequency);

	Reset();

	if (startTimer)
		Start();
}

Timer::~Timer() {
	Reset();
}

void Timer::Reset() {
	ZeroMemory(&start, sizeof(LARGE_INTEGER));
	ZeroMemory(&end, sizeof(LARGE_INTEGER));
}

void Timer::Start() {
	QueryPerformanceCounter(&start);
	end = start;
	prevTemp = start;
}

TimeSpan Timer::Stop() {
	QueryPerformanceCounter(&end);

	float totalS = SecondsBetween(start, end, frequency);
	float totalMs = MillisecondsBetween(start, end, frequency);

	start = end;
	return { totalS, totalMs };
}

TimeSpan Timer::Update() {
	LARGE_INTEGER temp;
	QueryPerformanceCounter(&temp);

	float dtS = SecondsBetween(prevTemp, temp, frequency);
	float dtMs = MillisecondsBetween(prevTemp, temp, frequency);

	prevTemp = temp;
	return{ dtS, dtMs };
}

float Timer::SecondsBetween(const LARGE_INTEGER & a, const LARGE_INTEGER & b, const LARGE_INTEGER & frequency) {
	return (((float)b.QuadPart - (float)a.QuadPart) / (float)frequency.QuadPart);
}

float Timer::MillisecondsBetween(const LARGE_INTEGER & a, const LARGE_INTEGER & b, const LARGE_INTEGER & frequency) {
	return ((((float)b.QuadPart - (float)a.QuadPart) * 1000.0f) / (float)frequency.QuadPart);
}