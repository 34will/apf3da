#include "RenderComponent.h"
#include "SceneGraph.h"
#include "SceneObject.h"
#include "CameraComponent.h"

RegisterComponentType(RenderComponent);

RenderComponent::RenderComponent() : Component(), transform(NULL), mesh(NULL) { }

RenderComponent::~RenderComponent() { }

bool RenderComponent::Initialise(XMLElement * init) {
	if(init == NULL)
		return false;

	int meshID = 0;
	if(init->QueryIntAttribute("meshid", &meshID) == XML_SUCCESS) {
		mesh = SceneGraph::CurrentScene()->GetMesh(meshID);

		if(mesh == NULL)
			return false;
	}

	return true;
}

void RenderComponent::Start() {
	transform = sceneObject->GetComponent<TransformComponent>();
}

void RenderComponent::Shutdown() { }

void RenderComponent::Draw(ID3D11DeviceContext * dX11DeviceContext, Pass * pass, CameraComponent * currentCam) {
	if(mesh != NULL) {
		if(currentCam->BoundingBoxInViewFrustrum(transform->GetWorldPosition(), mesh->GetAABoundingBox(transform->GetWorld()))) {
			Shader * vShader = pass->GetVertexShader();

			vShader->SetVariable("World", transform->GetWorldPtr());

			mesh->Draw(dX11DeviceContext, pass);
		}
	}
}

string RenderComponent::GetTypename() const {
	return typeof<RenderComponent>::name();
}
