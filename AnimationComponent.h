#ifndef ANIMATION_COMPONENT_H
#define ANIMATION_COMPONENT_H

#include <DirectXMath.h>
#include "Component.h"
#include "AnimatedMesh.h"
#include "ComponentRegistrar.h"

using namespace std;
using namespace DirectX;
using namespace tinyxml2;

class AnimationComponent : public Component {
private:
	AnimatedMesh * mesh = NULL;

	AnimationComponent();

public:
	~AnimationComponent();

	bool Initialise(XMLElement * init);

	void Start();
	void Shutdown();

	void Update(float dt);

	string GetTypename() const;

	SetupComponentType(AnimationComponent);
};

DeclareType(AnimationComponent);

#endif


