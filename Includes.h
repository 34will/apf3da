#ifndef INCLUDES_H
#define INCLUDES_H

#define CallMemberFunction(object, ptrToMember) ((object).*(ptrToMember))
#define UnusedVariable(x) (void)(x)

#ifndef NULL
#define NULL 0
#endif

template<typename T> struct typeof {
	static const char * name() { static_assert(false, "You are missing a DeclareType()"); }
};

#define DeclareType(x) template<> struct typeof<x> { static const char * name() { return #x; } }

DeclareType(bool);
DeclareType(char);
DeclareType(int);
DeclareType(float);
DeclareType(double);

#define Random01() ((float)rand() / (float)RAND_MAX)

#endif