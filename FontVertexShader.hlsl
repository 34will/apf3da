cbuffer FontVertexMatrixBuffer : register(b0) {
	float4x4 Ortho;
};

struct VSInput {
	float2	Position		: POSITION;
	float2	Texel			: TEXCOORD0;
};

struct VSOutput {
	float4	Position		: SV_POSITION;
	float2	Texel			: TEXCOORD0;
};

VSOutput main(VSInput input) {
	VSOutput output = (VSOutput)0;

	float4 pos = float4(input.Position.x, input.Position.y, 0.0f, 1.0f);

	output.Position = mul(pos, transpose(Ortho));
	output.Position.x -= 1.0f;
	output.Position.y -= 1.0f;

	output.Position.z = 0.5f;

	output.Texel = input.Texel;

	return output;
}