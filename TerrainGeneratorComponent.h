#ifndef TERRAIN_GENERATOR_COMPONENT_H
#define TERRAIN_GENERATOR_COMPONENT_H

#include "Component.h"
#include "RenderComponent.h"
#include "ComponentRegistrar.h"

using namespace std;
using namespace DirectX;
using namespace tinyxml2;

class TerrainGeneratorComponent : public Component {
private:
	RenderComponent * render;

	TerrainGeneratorComponent();

public:
	~TerrainGeneratorComponent();

	bool Initialise(XMLElement * init);

	void Start();
	void Shutdown();

	string GetTypename() const;

	SetupComponentType(TerrainGeneratorComponent);
};

DeclareType(TerrainGeneratorComponent);

#endif