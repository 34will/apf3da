#include "SplineMovementComponent.h"

XMFLOAT3 SplineSegement::GeneratePoint(float u) {
	if(u <= 0)
		return p1;
	else if(u >= 1)
		return p4;
	else {
		XMFLOAT3 ret = XMFLOAT3(0, 0, 0);
		XMVECTOR outP = XMLoadFloat3(&ret), outP1 = XMLoadFloat3(&p1), outP2 = XMLoadFloat3(&p2), outP3 = XMLoadFloat3(&p3), outP4 = XMLoadFloat3(&p4);
		outP1 *= pow(1.0f - u, 3);
		outP2 *= (3.0f * u) * pow(1.0f - u, 2);
		outP3 *= (3.0f * pow(u, 2)) * (1.0f - u);
		outP4 *= pow(u, 3);
		outP = outP1 + outP2 + outP3 + outP4;
		XMStoreFloat3(&ret, outP);
		return ret;
	}
}

void LineSegment::Calculate(XMFLOAT3 p1, XMFLOAT3 p2) {
	this->p1 = p1;
	this->p2 = p2;

	XMFLOAT3 offset = DXHelper::Subtract(p2, p1);
	XMVECTOR outOff = XMLoadFloat3(&offset), outDist;
	outDist = XMVector3Length(outOff);
	XMStoreFloat3(&offset, outDist);
	length = offset.x;
	outOff /= length;
	XMStoreFloat3(&direction, outOff);
}

RegisterComponentType(SplineMovementComponent);

SplineMovementComponent::SplineMovementComponent() : Component(), transform(NULL), currentPoint(0), t(0), scaledSpeed(1.0f) { }

SplineMovementComponent::~SplineMovementComponent() { }

bool SplineMovementComponent::Initialise(XMLElement * init) {
	if(init == NULL)
		return false;

	transform = sceneObject->GetComponent<TransformComponent>();

	XMLError err = init->QueryUnsignedAttribute("steps", &numSteps);
	if(err != XML_SUCCESS)
		return false;

	err = init->QueryFloatAttribute("speed", &speed);
	if(err != XML_SUCCESS)
		return false;

	XMLElement * segment = init->FirstChildElement("Segment");

	while(segment != NULL) {
		XMFLOAT3 p1, p2, p3, p4;
		XMLElement * point = segment->FirstChildElement("Point");
		int p = 0;

		while(point != NULL) {
			float x = 0, y = 0, z = 0;

			err = point->QueryFloatAttribute("x", &x);
			if(err != XML_SUCCESS)
				return false;

			err = point->QueryFloatAttribute("y", &y);
			if(err != XML_SUCCESS)
				return false;

			err = point->QueryFloatAttribute("z", &z);
			if(err != XML_SUCCESS)
				return false;

			if(p == 0)
				p1 = XMFLOAT3(x, y, z);
			else if(p == 1)
				p2 = XMFLOAT3(x, y, z);
			else if(p == 2)
				p3 = XMFLOAT3(x, y, z);
			else if(p == 3)
				p4 = XMFLOAT3(x, y, z);
			else
				return false;

			p++;
			point = point->NextSiblingElement("Point");
		}

		AddSegment(p1, p2, p3, p4);

		segment = segment->NextSiblingElement("Segment");
	}

	return true;
}

void SplineMovementComponent::Start() {
	ComputePath();
	oldPos = transform->GetLocalPosition();
}

void SplineMovementComponent::Shutdown() { }

void SplineMovementComponent::Update(float dt) {
	t += dt * scaledSpeed;

	if(t >= 1.0f) {
		transform->SetLocalPosition(curveSegments[currentPoint].p2);
		transform->SetForward(curveSegments[currentPoint].direction);
		oldDir = curveSegments[currentPoint].direction;

		currentPoint++;

		if(currentPoint >= curveSegments.size())
			currentPoint = 0;

		t = 0;
		scaledSpeed = speed / curveSegments[currentPoint].length;
		oldPos = curveSegments[currentPoint].p1;
	} else {
		transform->SetLocalPosition(DXHelper::Lerp(oldPos, curveSegments[currentPoint].p2, t));
		transform->SetForward(DXHelper::Lerp(oldDir, curveSegments[currentPoint].direction, t));
	}
}

void SplineMovementComponent::AddSegment(XMFLOAT3 p1, XMFLOAT3 p2, XMFLOAT3 p3, XMFLOAT3 p4) {
	int id = segments.size();
	segments.push_back(SplineSegement());
	segments[id].p1 = p1;
	segments[id].p2 = p2;
	segments[id].p3 = p3;
	segments[id].p4 = p4;
}

void SplineMovementComponent::AddSegment(SplineSegement s) {
	segments.push_back(s);
}

void SplineMovementComponent::ComputePath() {
	curveSegments = vector<LineSegment>();
	curveSegments.reserve(numSteps * segments.size());

	for(auto s : segments) {
		vector<XMFLOAT3> calcPoints = vector<XMFLOAT3>();
		calcPoints.reserve(numSteps + 1);

		float u = 0.0f, step = 1.0f / numSteps;

		for(unsigned int i = 0; i <= numSteps; i++, u += step)
			calcPoints.push_back(s.GeneratePoint(u));

		for(unsigned int i = 0; i < numSteps; i++) {
			LineSegment ls;
			ls.Calculate(calcPoints[i], calcPoints[i + 1]);
			curveSegments.push_back(ls);
		}
	}

	currentPoint = 0;
	t = 0;
	scaledSpeed = speed / curveSegments[currentPoint].length;
	oldPos = curveSegments[currentPoint].p1;
	oldDir = curveSegments[currentPoint].direction;
	transform->SetLocalPosition(curveSegments[currentPoint].p1);
	transform->SetForward(curveSegments[currentPoint].direction);
}

string SplineMovementComponent::GetTypename() const {
	return "SplineMovementComponent";
}