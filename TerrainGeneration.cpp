#include "TerrainGeneration.h"
#include "DirectX11App.h"
#include "Texture.h"
#include "DX11Texture.h"
#include "MeshGeneration.h"
#include "libnoise\noise.h"

using namespace noise::module;

const Parser<TerrainGenerationType> TerrainGeneration::tgtParser = {
		{
			{ "None", TerrainGenerationType::None },
			{ "Normal", TerrainGenerationType::Normal },
			{ "Random", TerrainGenerationType::Random },
			{ "Heightmap", TerrainGenerationType::Heightmap },
			{ "DiamondSquare", TerrainGenerationType::DiamondSquare },
			{ "FaultLine", TerrainGenerationType::FaultLine },
			{ "PerlinNoise", TerrainGenerationType::PerlinNoise }
		}
};

bool TerrainGeneration::GenerateNormalPlane(Mesh * mesh, XMLElement * config, Material * const materials, const unsigned int & numMaterials, PlaneData * outPlaneData) {
	float width, depth;
	unsigned int numRows, numCols;
	XMLError err;

	XMLElement * subElement = config->FirstChildElement("Width");
	if(subElement == NULL || (err = subElement->QueryFloatText(&width)) != XML_SUCCESS)
		return false;

	subElement = config->FirstChildElement("Depth");
	if(subElement == NULL || (err = subElement->QueryFloatText(&depth)) != XML_SUCCESS)
		return false;

	subElement = config->FirstChildElement("NumberOfColumns");
	if(subElement == NULL || (err = subElement->QueryUnsignedText(&numCols)) != XML_SUCCESS)
		return false;

	subElement = config->FirstChildElement("NumberOfRows");
	if(subElement == NULL || (err = subElement->QueryUnsignedText(&numRows)) != XML_SUCCESS)
		return false;

	if(!MeshGeneration::GeneratePlane(mesh, materials, numMaterials, width, depth, numRows, numCols, { 0.0f, 0.0f, 0.0f }))
		return false;

	if(outPlaneData != NULL) {
		outPlaneData->m = (numCols + 1);
		outPlaneData->n = (numRows + 1);

		outPlaneData->dX = width / numCols;
		outPlaneData->dZ = depth / numRows;

		outPlaneData->width = width;
		outPlaneData->depth = depth;

		outPlaneData->halfWidth = (0.5f * width);
		outPlaneData->halfDepth = (0.5f * depth);

		outPlaneData->numCols = numCols;
		outPlaneData->numRows = numRows;
	}

	return true;
}

bool TerrainGeneration::GenerateRandomPlane(Mesh * mesh, XMLElement * config, Material * const materials, const unsigned int & numMaterials) {
	PlaneData pd;
	if(!GenerateNormalPlane(mesh, config, materials, numMaterials, &pd))
		return false;

	float heightScale = 0.0f;
	XMLError err;
	XMLElement * subElement = config->FirstChildElement("HeightScale");
	if(subElement == NULL || (err = subElement->QueryFloatText(&heightScale)) != XML_SUCCESS)
		return false;

	Vertex ** vertices = new Vertex *[mesh->GetNumVertices()];
	ZeroMemory((void *)vertices, sizeof(Vertex *) * mesh->GetNumVertices());
	const unsigned int * indices = mesh->GetIndices();

	if(!mesh->BeginModify(vertices, NULL))
		return false;

	for(unsigned int i = 0; i < mesh->GetNumVertices(); i++) {
		vertices[i]->Position.y = ((float)rand() / RAND_MAX) * heightScale;
		vertices[i]->Normal = XMFLOAT3(0.0f, 0.0f, 0.0f);
	}

	for(unsigned int a = 0; a < mesh->GetNumIndices(); a += 3) {
		unsigned int i1 = indices[a];
		unsigned int i2 = indices[a + 1];
		unsigned int i3 = indices[a + 2];

		XMFLOAT3 e1 = DXHelper::Subtract(vertices[i1]->Position, vertices[i2]->Position);
		XMFLOAT3 e2 = DXHelper::Subtract(vertices[i3]->Position, vertices[i2]->Position);
		XMFLOAT3 cross = DXHelper::Cross(e1, e2);

		if(cross.y < 0.0f) {
			cross.x = -cross.x;
			cross.y = -cross.y;
			cross.z = -cross.z;
		}

		vertices[i1]->Normal = DXHelper::Add(vertices[i1]->Normal, cross);
		vertices[i2]->Normal = DXHelper::Add(vertices[i2]->Normal, cross);
		vertices[i3]->Normal = DXHelper::Add(vertices[i3]->Normal, cross);
	}

	for(unsigned int a = 0; a < mesh->GetNumVertices(); a++) {
		XMVECTOR normal = XMLoadFloat3(&vertices[a]->Normal);
		normal = XMVector3Normalize(normal);
		XMStoreFloat3(&vertices[a]->Normal, normal);
	}

	mesh->EndModify();
	mesh->CalculateAndAssignTangents();

	delete[] vertices;

	return true;
}

bool TerrainGeneration::GenerateHeightmapPlane(Mesh * mesh, XMLElement * config, Material * const materials, const unsigned int & numMaterials) {
	PlaneData pd;
	if(!GenerateNormalPlane(mesh, config, materials, numMaterials, &pd))
		return false;

	float heightScale = 0.0f;
	XMLError err;
	XMLElement * subElement = config->FirstChildElement("HeightScale");
	if(subElement == NULL || (err = subElement->QueryFloatText(&heightScale)) != XML_SUCCESS)
		return false;

	subElement = config->FirstChildElement("Heightmap");
	if(subElement == NULL)
		return false;

	Texture * t = new DX11Texture(subElement->GetText(), true);
	if(!t->Load())
		return false;

	Vertex ** vertices = new Vertex *[mesh->GetNumVertices()];
	ZeroMemory((void *)vertices, sizeof(Vertex *) * mesh->GetNumVertices());

	if(!mesh->BeginModify(vertices, NULL))
		return false;

	unsigned int k = 0;
	for(unsigned int j = 0; j < pd.n; j++) {
		for(unsigned int i = 0; i < pd.m; i++) {
			float u = ((float)i / (float)pd.numCols), v = ((float)j / (float)pd.numRows);
			Colour c = t->GetLocalisedPixel(u, v);
			if(Colour::IsNullColour(c))
				return false;

			vertices[k]->Position.y = c.R * heightScale;
			vertices[k]->TexCoord = { u, v };

			k++;
		}
	}

	const unsigned int * indices = mesh->GetIndices();
	for(unsigned int a = 0; a < mesh->GetNumVertices(); a++)
		vertices[a]->Normal = XMFLOAT3(0.0f, 0.0f, 0.0f);

	for(unsigned int a = 0; a < mesh->GetNumIndices(); a += 3) {
		unsigned int i1 = indices[a];
		unsigned int i2 = indices[a + 1];
		unsigned int i3 = indices[a + 2];

		XMFLOAT3 e1 = DXHelper::Subtract(vertices[i1]->Position, vertices[i2]->Position);
		XMFLOAT3 e2 = DXHelper::Subtract(vertices[i3]->Position, vertices[i2]->Position);
		XMFLOAT3 cross = DXHelper::Cross(e1, e2);

		if(cross.y < 0.0f) {
			cross.x = -cross.x;
			cross.y = -cross.y;
			cross.z = -cross.z;
		}

		vertices[i1]->Normal = DXHelper::Add(vertices[i1]->Normal, cross);
		vertices[i2]->Normal = DXHelper::Add(vertices[i2]->Normal, cross);
		vertices[i3]->Normal = DXHelper::Add(vertices[i3]->Normal, cross);
	}

	for(unsigned int a = 0; a < mesh->GetNumVertices(); a++) {
		XMVECTOR normal = XMLoadFloat3(&vertices[a]->Normal);
		normal = XMVector3Normalize(normal);
		XMStoreFloat3(&vertices[a]->Normal, normal);
	}

	mesh->EndModify();
	mesh->CalculateAndAssignTangents();

	delete[] vertices;

	return true;
}

void TerrainGeneration::DiamondSquare(Vertex ** vertices, unsigned int xLength, unsigned int yLength, float heightScale, float offset) {
	if(vertices != NULL && offset > 1) {
		unsigned int iOffset = (int)offset, halfOffset = iOffset / 2;
		for(unsigned y = iOffset; y < yLength; y += iOffset) {
			unsigned int yLength = y * xLength, yOffsetLength = (y - iOffset) * xLength, yHalfOffsetLength = (y - halfOffset) * xLength;
			for(unsigned x = iOffset; x < xLength; x += iOffset) {
				float average = vertices[yLength + x]->Position.y;
				average += vertices[yLength + (x - iOffset)]->Position.y;
				average += vertices[yOffsetLength + x]->Position.y;
				average += vertices[yOffsetLength + (x - iOffset)]->Position.y;

				vertices[yHalfOffsetLength + (x - halfOffset)]->Position.y = (average * 0.25f) + (((Random01() * 2.0f) - 1.0f) * heightScale);
			}
		}

		for(unsigned y = (2 * iOffset); y < yLength; y += iOffset) {
			unsigned int yOffsetLength = (y - iOffset) * xLength, yHalfOffsetLength = (y - halfOffset) * xLength;
			for(unsigned x = (2 * iOffset); x < xLength; x += iOffset) {
				float average1 = vertices[yOffsetLength + (x - iOffset)]->Position.y;
				average1 += vertices[yHalfOffsetLength + (x - halfOffset)]->Position.y;
				float average2 = average1;

				average1 += vertices[(y * xLength) + (x - iOffset)]->Position.y;
				average1 += vertices[yHalfOffsetLength + (x - (3 * halfOffset))]->Position.y;

				average2 += vertices[yOffsetLength + x]->Position.y;
				average2 += vertices[((y - (3 * halfOffset)) * xLength) + (x - halfOffset)]->Position.y;

				vertices[yHalfOffsetLength + (x - iOffset)]->Position.y = (average1 * 0.25f) + (((Random01() * 2.0f) - 1.0f) * heightScale);
				vertices[yOffsetLength + (x - halfOffset)]->Position.y = (average2 * 0.25f) + (((Random01() * 2.0f) - 1.0f) * heightScale);
			}
		}

		DiamondSquare(vertices, xLength, yLength, heightScale * 0.5f, offset * 0.5f);
	}
}

bool TerrainGeneration::GenerateDiamondSquarePlane(Mesh * mesh, XMLElement * config, Material * const materials, const unsigned int & numMaterials) {
	PlaneData pd;
	if(!GenerateNormalPlane(mesh, config, materials, numMaterials, &pd))
		return false;

	float mainPart = 0.0f;
	float logFraction = modf(log2f((float)(pd.m - 1)), &mainPart);
	if(logFraction != 0.0f)
		return false;

	unsigned int iterations = (unsigned int)mainPart;
	mainPart = 0.0f;

	logFraction = modf(log2f((float)(pd.n - 1)), &mainPart);
	if(logFraction != 0.0f)
		return false;

	if(iterations != (unsigned int)mainPart)
		return false;

	float heightScale = 0.0f;
	XMLError err;
	XMLElement * subElement = config->FirstChildElement("HeightScale");
	if(subElement == NULL || (err = subElement->QueryFloatText(&heightScale)) != XML_SUCCESS)
		return false;

	unsigned int seed = 0;
	subElement = config->FirstChildElement("Seed");
	if(subElement == NULL || (err = subElement->QueryUnsignedText(&seed)) != XML_SUCCESS)
		seed = (unsigned int)time(0);

	Vertex ** vertices = new Vertex *[mesh->GetNumVertices()];
	ZeroMemory((void *)vertices, sizeof(Vertex *) * mesh->GetNumVertices());

	if(!mesh->BeginModify(vertices, NULL))
		return false;

	srand(seed);

	DiamondSquare(vertices, pd.m, pd.n, heightScale, (float)pd.n);

	const unsigned int * indices = mesh->GetIndices();
	for(unsigned int a = 0; a < mesh->GetNumVertices(); a++)
		vertices[a]->Normal = XMFLOAT3(0.0f, 0.0f, 0.0f);

	for(unsigned int a = 0; a < mesh->GetNumIndices(); a += 3) {
		unsigned int i1 = indices[a];
		unsigned int i2 = indices[a + 1];
		unsigned int i3 = indices[a + 2];

		XMFLOAT3 e1 = DXHelper::Subtract(vertices[i1]->Position, vertices[i2]->Position);
		XMFLOAT3 e2 = DXHelper::Subtract(vertices[i3]->Position, vertices[i2]->Position);
		XMFLOAT3 cross = DXHelper::Cross(e1, e2);

		if(cross.y < 0.0f) {
			cross.x = -cross.x;
			cross.y = -cross.y;
			cross.z = -cross.z;
		}

		vertices[i1]->Normal = DXHelper::Add(vertices[i1]->Normal, cross);
		vertices[i2]->Normal = DXHelper::Add(vertices[i2]->Normal, cross);
		vertices[i3]->Normal = DXHelper::Add(vertices[i3]->Normal, cross);
	}

	for(unsigned int a = 0; a < mesh->GetNumVertices(); a++) {
		XMVECTOR normal = XMLoadFloat3(&vertices[a]->Normal);
		normal = XMVector3Normalize(normal);
		XMStoreFloat3(&vertices[a]->Normal, normal);
	}

	mesh->EndModify();
	mesh->CalculateAndAssignTangents();

	delete[] vertices;

	return true;
}

bool TerrainGeneration::GenerateFaultLinePlane(Mesh * mesh, XMLElement * config, Material * const materials, const unsigned int & numMaterials) {
	PlaneData pd;
	if(!GenerateNormalPlane(mesh, config, materials, numMaterials, &pd))
		return false;

	unsigned int iterations = 0;
	XMLError err;
	XMLElement * subElement = config->FirstChildElement("Iterations");
	if(subElement == NULL || (err = subElement->QueryUnsignedText(&iterations)) != XML_SUCCESS)
		return false;

	float displacement = 0.0f;
	subElement = config->FirstChildElement("Displacement");
	if(subElement == NULL || (err = subElement->QueryFloatText(&displacement)) != XML_SUCCESS)
		return false;

	unsigned int seed = 0;
	subElement = config->FirstChildElement("Seed");
	if(subElement == NULL || (err = subElement->QueryUnsignedText(&seed)) != XML_SUCCESS)
		seed = (unsigned int)time(0);

	Vertex ** vertices = new Vertex *[mesh->GetNumVertices()];
	ZeroMemory((void *)vertices, sizeof(Vertex *) * mesh->GetNumVertices());

	if(!mesh->BeginModify(vertices, NULL))
		return false;

	srand(seed);
	float d = sqrt(pd.width * pd.width + pd.depth * pd.depth), halfD = d * 0.5f;

	for(unsigned int i = 0; i < iterations; i++) {
		int v = rand();
		float a = sinf((float)v), b = cosf((float)v), c = (Random01() * d) - halfD;

		for(unsigned y = 0; y < pd.n; y++) {
			unsigned int yLength = y * pd.m;
			for(unsigned x = 0; x < pd.m; x++) {
				Vertex * vert = vertices[yLength + x];
				vert->Position.y += ((a * vert->Position.x) + (b * vert->Position.z) - c > 0 ? 1 : -1) * displacement;
			}
		}
	}

	const unsigned int * indices = mesh->GetIndices();
	for(unsigned int a = 0; a < mesh->GetNumVertices(); a++)
		vertices[a]->Normal = XMFLOAT3(0.0f, 0.0f, 0.0f);

	for(unsigned int a = 0; a < mesh->GetNumIndices(); a += 3) {
		unsigned int i1 = indices[a];
		unsigned int i2 = indices[a + 1];
		unsigned int i3 = indices[a + 2];

		XMFLOAT3 e1 = DXHelper::Subtract(vertices[i1]->Position, vertices[i2]->Position);
		XMFLOAT3 e2 = DXHelper::Subtract(vertices[i3]->Position, vertices[i2]->Position);
		XMFLOAT3 cross = DXHelper::Cross(e1, e2);

		if(cross.y < 0.0f) {
			cross.x = -cross.x;
			cross.y = -cross.y;
			cross.z = -cross.z;
		}

		vertices[i1]->Normal = DXHelper::Add(vertices[i1]->Normal, cross);
		vertices[i2]->Normal = DXHelper::Add(vertices[i2]->Normal, cross);
		vertices[i3]->Normal = DXHelper::Add(vertices[i3]->Normal, cross);
	}

	for(unsigned int a = 0; a < mesh->GetNumVertices(); a++) {
		XMVECTOR normal = XMLoadFloat3(&vertices[a]->Normal);
		normal = XMVector3Normalize(normal);
		XMStoreFloat3(&vertices[a]->Normal, normal);
	}

	mesh->EndModify();
	mesh->CalculateAndAssignTangents();

	delete[] vertices;

	return true;
}

bool TerrainGeneration::GeneratePerlinNoisePlane(Mesh * mesh, XMLElement * config, Material * const materials, const unsigned int & numMaterials) {
	PlaneData pd;
	if(!GenerateNormalPlane(mesh, config, materials, numMaterials, &pd))
		return false;

	float heightScale = 0.0f;
	XMLError err;
	XMLElement * subElement = config->FirstChildElement("HeightScale");
	if(subElement == NULL || (err = subElement->QueryFloatText(&heightScale)) != XML_SUCCESS)
		return false;

	double frequency = 1.0;
	subElement = config->FirstChildElement("Frequency");
	if(subElement == NULL || (err = subElement->QueryDoubleText(&frequency)) != XML_SUCCESS)
		frequency = 1.0;

	int octaves = 1;
	subElement = config->FirstChildElement("Octaves");
	if(subElement == NULL || (err = subElement->QueryIntText(&octaves)) != XML_SUCCESS)
		octaves = 1;

	unsigned int seed = 0;
	subElement = config->FirstChildElement("Seed");
	if(subElement == NULL || (err = subElement->QueryUnsignedText(&seed)) != XML_SUCCESS)
		seed = (unsigned int)time(0);

	srand(seed);

	float xOffset;
	subElement = config->FirstChildElement("XOffset");
	if(subElement == NULL || (err = subElement->QueryFloatText(&xOffset)) != XML_SUCCESS)
		xOffset = Random01() * 12345.0f;

	float yOffset;
	subElement = config->FirstChildElement("YOffset");
	if(subElement == NULL || (err = subElement->QueryFloatText(&yOffset)) != XML_SUCCESS)
		yOffset = Random01() * 12345.0f;

	Vertex ** vertices = new Vertex *[mesh->GetNumVertices()];
	ZeroMemory((void *)vertices, sizeof(Vertex *) * mesh->GetNumVertices());

	if(!mesh->BeginModify(vertices, NULL))
		return false;

	Perlin perlinNoise;
	perlinNoise.SetFrequency(frequency);
	perlinNoise.SetOctaveCount(octaves);

	unsigned int k = 0;
	for(unsigned int j = 0; j < pd.n; j++) {
		for(unsigned int i = 0; i < pd.m; i++) {
			vertices[k]->Position.y = (float)perlinNoise.GetValue(xOffset + vertices[k]->TexCoord.x, yOffset + vertices[k]->TexCoord.y, 0.0) * heightScale;
			k++;
		}
	}

	const unsigned int * indices = mesh->GetIndices();
	for(unsigned int a = 0; a < mesh->GetNumVertices(); a++)
		vertices[a]->Normal = XMFLOAT3(0.0f, 0.0f, 0.0f);

	for(unsigned int a = 0; a < mesh->GetNumIndices(); a += 3) {
		unsigned int i1 = indices[a];
		unsigned int i2 = indices[a + 1];
		unsigned int i3 = indices[a + 2];

		XMFLOAT3 e1 = DXHelper::Subtract(vertices[i1]->Position, vertices[i2]->Position);
		XMFLOAT3 e2 = DXHelper::Subtract(vertices[i3]->Position, vertices[i2]->Position);
		XMFLOAT3 cross = DXHelper::Cross(e1, e2);

		if(cross.y < 0.0f) {
			cross.x = -cross.x;
			cross.y = -cross.y;
			cross.z = -cross.z;
		}

		vertices[i1]->Normal = DXHelper::Add(vertices[i1]->Normal, cross);
		vertices[i2]->Normal = DXHelper::Add(vertices[i2]->Normal, cross);
		vertices[i3]->Normal = DXHelper::Add(vertices[i3]->Normal, cross);
	}

	for(unsigned int a = 0; a < mesh->GetNumVertices(); a++) {
		XMVECTOR normal = XMLoadFloat3(&vertices[a]->Normal);
		normal = XMVector3Normalize(normal);
		XMStoreFloat3(&vertices[a]->Normal, normal);
	}

	mesh->EndModify();
	mesh->CalculateAndAssignTangents();

	delete[] vertices;

	return true;
}

bool TerrainGeneration::GenerateNormalCubeSphere(Mesh * mesh, XMLElement * config, Material * const materials, const unsigned int & numMaterials) {
	float diameter;
	unsigned int numSubDivs;
	CubeMapTextureType texType = CubeMapTextureType::OneTexture;

	XMLError err;
	XMLElement * subElement = config->FirstChildElement("Diameter");
	if(subElement == NULL || (err = subElement->QueryFloatText(&diameter)) != XML_SUCCESS)
		return false;

	subElement = config->FirstChildElement("NumberOfSubdivisions");
	if(subElement == NULL || (err = subElement->QueryUnsignedText(&numSubDivs)) != XML_SUCCESS)
		return false;

	subElement = config->FirstChildElement("CubeMapTextureType");
	if(subElement == NULL || (texType = MeshGeneration::StringToCubeMapType(subElement->GetText())) == CubeMapTextureType::None)
		texType = CubeMapTextureType::OneTexture;

	if(!MeshGeneration::GenerateCubeSphere(mesh, materials, numMaterials, texType, diameter, numSubDivs, { 0.0f, 0.0f, 0.0f }))
		return false;

	return true;
}

bool TerrainGeneration::GeneratePerlinNoiseCubeSphere(Mesh * mesh, XMLElement * config, Material * const materials, const unsigned int & numMaterials) {
	if(!GenerateNormalCubeSphere(mesh, config, materials, numMaterials))
		return false;

	float heightScale, diameter;
	XMLError err;
	XMLElement * subElement = config->FirstChildElement("HeightScale");
	if(subElement == NULL || (err = subElement->QueryFloatText(&heightScale)) != XML_SUCCESS)
		return false;

	subElement = config->FirstChildElement("Diameter");
	if(subElement == NULL || (err = subElement->QueryFloatText(&diameter)) != XML_SUCCESS)
		return false;

	double frequency = 1.0;
	subElement = config->FirstChildElement("Frequency");
	if(subElement == NULL || (err = subElement->QueryDoubleText(&frequency)) != XML_SUCCESS)
		frequency = 1.0;

	int octaves = 1;
	subElement = config->FirstChildElement("Octaves");
	if(subElement == NULL || (err = subElement->QueryIntText(&octaves)) != XML_SUCCESS)
		octaves = 1;

	unsigned int seed = 0;
	subElement = config->FirstChildElement("Seed");
	if(subElement == NULL || (err = subElement->QueryUnsignedText(&seed)) != XML_SUCCESS)
		seed = (unsigned int)time(0);

	srand(seed);

	float xOffset;
	subElement = config->FirstChildElement("XOffset");
	if(subElement == NULL || (err = subElement->QueryFloatText(&xOffset)) != XML_SUCCESS)
		xOffset = Random01() * 12345.0f;

	float yOffset;
	subElement = config->FirstChildElement("YOffset");
	if(subElement == NULL || (err = subElement->QueryFloatText(&yOffset)) != XML_SUCCESS)
		yOffset = Random01() * 12345.0f;

	float zOffset;
	subElement = config->FirstChildElement("ZOffset");
	if(subElement == NULL || (err = subElement->QueryFloatText(&zOffset)) != XML_SUCCESS)
		zOffset = Random01() * 12345.0f;

	Vertex ** vertices = new Vertex *[mesh->GetNumVertices()];
	ZeroMemory((void *)vertices, sizeof(Vertex *) * mesh->GetNumVertices());

	if(!mesh->BeginModify(vertices, NULL))
		return false;

	Perlin perlinNoise;
	perlinNoise.SetFrequency(frequency);
	perlinNoise.SetOctaveCount(octaves);

	unsigned int numVerts = mesh->GetNumVertices();
	for(unsigned int i = 0; i < numVerts; i++) {
		float value = diameter + (float)(perlinNoise.GetValue(xOffset + vertices[i]->Position.x, xOffset + vertices[i]->Position.y, xOffset + vertices[i]->Position.z) * (double)heightScale);
		XMVECTOR normal = XMLoadFloat3(&vertices[i]->Position);
		normal = XMVector3Normalize(normal);
		normal *= value;
		XMStoreFloat3(&vertices[i]->Position, normal);
		vertices[i]->Normal = XMFLOAT3(0.0f, 0.0f, 0.0f);
	}

	const unsigned int * indices = mesh->GetIndices();

	for(unsigned int a = 0; a < mesh->GetNumIndices(); a += 3) {
		unsigned int i1 = indices[a];
		unsigned int i2 = indices[a + 1];
		unsigned int i3 = indices[a + 2];

		XMFLOAT3 e1 = DXHelper::Subtract(vertices[i1]->Position, vertices[i2]->Position);
		XMFLOAT3 e2 = DXHelper::Subtract(vertices[i3]->Position, vertices[i2]->Position);
		XMFLOAT3 cross = DXHelper::Cross(e1, e2);

		vertices[i1]->Normal = DXHelper::Add(vertices[i1]->Normal, cross);
		vertices[i2]->Normal = DXHelper::Add(vertices[i2]->Normal, cross);
		vertices[i3]->Normal = DXHelper::Add(vertices[i3]->Normal, cross);
	}

	for(unsigned int a = 0; a < mesh->GetNumVertices(); a++) {
		float dot = DXHelper::Dot(vertices[a]->Position, vertices[a]->Normal);
		XMVECTOR normal = XMLoadFloat3(&vertices[a]->Normal);
		normal = XMVector3Normalize(normal);
		normal *= (dot < 0 ? -1 : 1);
		XMStoreFloat3(&vertices[a]->Normal, normal);
	}

	mesh->EndModify();
	mesh->CalculateAndAssignTangents();

	delete[] vertices;

	return true;
}

bool TerrainGeneration::GenerateTerrainMesh(Mesh * mesh, XMLElement * config, Material * const materials, const unsigned int & numMaterials) {
	if(mesh == NULL || config == NULL || materials == NULL || numMaterials == 0)
		return false;

	string meshType = config->Attribute("meshtype");
	if(meshType.length() == 0)
		return false;

	MeshGenerationType outMeshType = MeshGeneration::StringToGenType(meshType);
	switch(outMeshType) {
	case MeshGenerationType::Plane: {
		string type = config->Attribute("type");
		if(type.length() == 0)
			return false;

		TerrainGenerationType outType = StringToGenType(type);
		switch(outType) {
		case TerrainGenerationType::Normal:
			return GenerateNormalPlane(mesh, config, materials, numMaterials);
		case TerrainGenerationType::Random:
			return GenerateRandomPlane(mesh, config, materials, numMaterials);
		case TerrainGenerationType::Heightmap:
			return GenerateHeightmapPlane(mesh, config, materials, numMaterials);
		case TerrainGenerationType::DiamondSquare:
			return GenerateDiamondSquarePlane(mesh, config, materials, numMaterials);
		case TerrainGenerationType::FaultLine:
			return GenerateFaultLinePlane(mesh, config, materials, numMaterials);
		case TerrainGenerationType::PerlinNoise:
			return GeneratePerlinNoisePlane(mesh, config, materials, numMaterials);
		default:
			return false;
		}

		break;
	}
	case MeshGenerationType::Cube: {
		break;
	}
	case MeshGenerationType::CubeSphere: {
		string type = config->Attribute("type");
		if(type.length() == 0)
			return false;

		TerrainGenerationType outType = StringToGenType(type);
		switch(outType) {
		case TerrainGenerationType::Normal:
			return GenerateNormalCubeSphere(mesh, config, materials, numMaterials);
		case TerrainGenerationType::PerlinNoise:
			return GeneratePerlinNoiseCubeSphere(mesh, config, materials, numMaterials);
			/*case TerrainGenerationType::Random:
				return GenerateRandomPlane(mesh, config, materials, numMaterials);
				case TerrainGenerationType::Heightmap:
				return GenerateHeightmapPlane(mesh, config, materials, numMaterials);*/
		default:
			return false;
		}

		break;
	}
	default:
		return false;
	}

	return false;
}

TerrainGenerationType TerrainGeneration::StringToGenType(const string & type) {
	TerrainGenerationType outType = TerrainGenerationType::None;
	tgtParser.Parse(type, outType);
	return outType;
}