#ifndef DIRECTX_11_HELPER_STRUCTS_H
#define DIRECTX_11_HELPER_STRUCTS_H

#include <d3d11.h>
#include <DirectXMath.h>
#include "Parser.h"

using namespace DirectX;
using namespace std;

class DXHelper {
private:
	const static Parser<D3D11_CULL_MODE> cullMode;
	const static Parser<D3D11_FILL_MODE> fillMode;
	const static Parser<D3D11_COMPARISON_FUNC> compareFunc;
	const static Parser<D3D11_STENCIL_OP> stencilOp;
	const static Parser<D3D11_DEPTH_WRITE_MASK> depthWriteMask;
	const static Parser<D3D11_BLEND_OP> blendOp;
	const static Parser<D3D11_BLEND> blend;
	const static Parser<UINT8> mask;
	const static UINT8 RGBAChannels;

public:
	const static unsigned int ShaderBoolTrue;
	const static unsigned int ShaderBoolFalse;

	const static float InfinityFloat;
	const static XMFLOAT2 InfinityVector2;
	const static XMFLOAT3 InfinityVector3;
	const static XMFLOAT4 InfinityVector4;

	static XMFLOAT2 const * InfinityVector2Ptr();
	static XMFLOAT3 const * InfinityVector3Ptr();
	static XMFLOAT4 const * InfinityVector4Ptr();

	const static XMFLOAT3 ForwardVector3;
	const static XMFLOAT4 ForwardVector4;

	const static XMFLOAT3 RightVector3;
	const static XMFLOAT4 RightVector4;

	const static XMFLOAT3 UpVector3;
	const static XMFLOAT4 UpVector4;

	const static XMFLOAT3 ZeroVector3;
	const static XMFLOAT4 ZeroVector4;

	static float RadiansToDegrees(float rad);
	static float DegreesToRadians(float deg);

	static XMFLOAT2 Normalise(XMFLOAT2 input);
	static XMFLOAT3 Normalise(XMFLOAT3 input);
	static XMFLOAT4 Normalise(XMFLOAT4 input);

	static XMFLOAT2 Add(XMFLOAT2 a, XMFLOAT2 b);
	static XMFLOAT2 Add(XMFLOAT2 a, float bX, float bY);
	static XMFLOAT3 Add(XMFLOAT3 a, XMFLOAT3 b);
	static XMFLOAT3 Add(XMFLOAT3 a, float bX, float bY, float bZ);
	static XMFLOAT4 Add(XMFLOAT4 a, XMFLOAT4 b);
	static XMFLOAT4 Add(XMFLOAT4 a, float bX, float bY, float bZ, float bW);

	static XMFLOAT2 Subtract(XMFLOAT2 a, XMFLOAT2 b);
	static XMFLOAT2 Subtract(XMFLOAT2 a, float bX, float bY);
	static XMFLOAT3 Subtract(XMFLOAT3 a, XMFLOAT3 b);
	static XMFLOAT3 Subtract(XMFLOAT3 a, float bX, float bY, float bZ);
	static XMFLOAT4 Subtract(XMFLOAT4 a, XMFLOAT4 b);
	static XMFLOAT4 Subtract(XMFLOAT4 a, float bX, float bY, float bZ, float bW);

	static XMFLOAT2 Multiply(XMFLOAT2 a, float b);
	static XMFLOAT3 Multiply(XMFLOAT3 a, float b);
	static XMFLOAT4 Multiply(XMFLOAT4 a, float b);

	static XMFLOAT2 Divide(XMFLOAT2 a, float b);
	static XMFLOAT3 Divide(XMFLOAT3 a, float b);
	static XMFLOAT4 Divide(XMFLOAT4 a, float b);

	static float Dot(XMFLOAT2 a, XMFLOAT2 b);
	static float Dot(XMFLOAT3 a, XMFLOAT3 b);
	static float Dot(XMFLOAT4 a, XMFLOAT4 b);

	static XMFLOAT3 Cross(XMFLOAT3 a, XMFLOAT3 b);

	static float AbsoluteDot(XMFLOAT2 a, XMFLOAT2 b);
	static float AbsoluteDot(XMFLOAT3 a, XMFLOAT3 b);
	static float AbsoluteDot(XMFLOAT4 a, XMFLOAT4 b);

	static bool Equals(XMFLOAT2 a, XMFLOAT2 b);
	static bool Equals(XMFLOAT3 a, XMFLOAT3 b);
	static bool Equals(XMFLOAT4 a, XMFLOAT4 b);

	static bool IsNormalised(XMFLOAT2 v);
	static bool IsNormalised(XMFLOAT3 v);
	static bool IsNormalised(XMFLOAT4 v);

	static XMFLOAT3 QuaternionToEuler(XMFLOAT4 quat, bool toDegrees = false);
	static XMFLOAT4 CombineQuaternions(XMFLOAT4 q1, XMFLOAT4 q2);
	static XMFLOAT4 AToBQuaternion(XMFLOAT3 a, XMFLOAT3 b, XMFLOAT3 up);

	static float Lerp(float p1, float p2, float t);
	static XMFLOAT2 Lerp(XMFLOAT2 p1, XMFLOAT2 p2, float t);
	static XMFLOAT3 Lerp(XMFLOAT3 p1, XMFLOAT3 p2, float t);
	static XMFLOAT4 Lerp(XMFLOAT4 p1, XMFLOAT4 p2, float t);

	static bool NearlyEqual(float a, float b, float threshold);

	static D3D11_CULL_MODE ToCullMode(string value);
	static D3D11_FILL_MODE ToFillMode(string value);
	static D3D11_COMPARISON_FUNC ToCompareFunc(string value);
	static D3D11_STENCIL_OP ToStencilOp(string value);
	static D3D11_DEPTH_WRITE_MASK ToDepthWriteMask(string value);
	static D3D11_BLEND_OP ToBlendOp(string value);
	static D3D11_BLEND ToBlend(string value);
	static UINT8 ToMask(string value);
};

#endif