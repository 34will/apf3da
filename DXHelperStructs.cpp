#include "DXHelperStructs.h"
#include <limits.h>

const unsigned int DXHelper::ShaderBoolFalse = 0;
const unsigned int DXHelper::ShaderBoolTrue = ~ShaderBoolFalse;

const float DXHelper::InfinityFloat = (numeric_limits<float>::has_infinity ? numeric_limits<float>::infinity() : FLT_MAX);
const XMFLOAT2 DXHelper::InfinityVector2 = XMFLOAT2(InfinityFloat, InfinityFloat);
const XMFLOAT3 DXHelper::InfinityVector3 = XMFLOAT3(InfinityFloat, InfinityFloat, InfinityFloat);
const XMFLOAT4 DXHelper::InfinityVector4 = XMFLOAT4(InfinityFloat, InfinityFloat, InfinityFloat, InfinityFloat);

const XMFLOAT3 DXHelper::ForwardVector3 = XMFLOAT3(0, 0, 1.0f);
const XMFLOAT4 DXHelper::ForwardVector4 = XMFLOAT4(0, 0, 1.0f, 0);

const XMFLOAT3 DXHelper::RightVector3 = XMFLOAT3(1.0f, 0, 0);
const XMFLOAT4 DXHelper::RightVector4 = XMFLOAT4(1.0f, 0, 0, 0);

const XMFLOAT3 DXHelper::UpVector3 = XMFLOAT3(0, 1.0f, 0);
const XMFLOAT4 DXHelper::UpVector4 = XMFLOAT4(0, 1.0f, 0, 0);

const XMFLOAT3 DXHelper::ZeroVector3 = XMFLOAT3(0, 0, 0);
const XMFLOAT4 DXHelper::ZeroVector4 = XMFLOAT4(0, 0, 0, 0);

const Parser<D3D11_CULL_MODE> DXHelper::cullMode = {
		{
			{ "D3D11_CULL_NONE", D3D11_CULL_NONE },
			{ "CULL_NONE", D3D11_CULL_NONE },
			{ "NONE", D3D11_CULL_NONE },
			{ "None", D3D11_CULL_NONE },
			{ "1", D3D11_CULL_NONE },
			{ "D3D11_CULL_FRONT", D3D11_CULL_FRONT },
			{ "CULL_FRONT", D3D11_CULL_FRONT },
			{ "FRONT", D3D11_CULL_FRONT },
			{ "Front", D3D11_CULL_FRONT },
			{ "2", D3D11_CULL_FRONT },
			{ "D3D11_CULL_BACK", D3D11_CULL_BACK },
			{ "CULL_BACK", D3D11_CULL_BACK },
			{ "BACK", D3D11_CULL_BACK },
			{ "Back", D3D11_CULL_BACK },
			{ "3", D3D11_CULL_BACK }
		}
};

const Parser<D3D11_FILL_MODE> DXHelper::fillMode = {
		{
			{ "D3D11_FILL_WIREFRAME", D3D11_FILL_WIREFRAME },
			{ "FILL_WIREFRAME", D3D11_FILL_WIREFRAME },
			{ "WIREFRAME", D3D11_FILL_WIREFRAME },
			{ "Wireframe", D3D11_FILL_WIREFRAME },
			{ "2", D3D11_FILL_WIREFRAME },
			{ "D3D11_FILL_SOLID", D3D11_FILL_SOLID },
			{ "FILL_SOLID", D3D11_FILL_SOLID },
			{ "SOLID", D3D11_FILL_SOLID },
			{ "Solid", D3D11_FILL_SOLID },
			{ "3", D3D11_FILL_SOLID }
		}
};

const Parser<D3D11_COMPARISON_FUNC> DXHelper::compareFunc = {
		{
			{ "D3D11_COMPARISON_NEVER", D3D11_COMPARISON_NEVER },
			{ "COMPARISON_NEVER", D3D11_COMPARISON_NEVER },
			{ "NEVER", D3D11_COMPARISON_NEVER },
			{ "Never", D3D11_COMPARISON_NEVER },
			{ "1", D3D11_COMPARISON_NEVER },
			{ "D3D11_COMPARISON_LESS", D3D11_COMPARISON_LESS },
			{ "COMPARISON_LESS", D3D11_COMPARISON_LESS },
			{ "LESS", D3D11_COMPARISON_LESS },
			{ "Less", D3D11_COMPARISON_LESS },
			{ "2", D3D11_COMPARISON_LESS },
			{ "D3D11_COMPARISON_EQUAL", D3D11_COMPARISON_EQUAL },
			{ "COMPARISON_EQUAL", D3D11_COMPARISON_EQUAL },
			{ "EQUAL", D3D11_COMPARISON_EQUAL },
			{ "Equal", D3D11_COMPARISON_EQUAL },
			{ "3", D3D11_COMPARISON_EQUAL },
			{ "D3D11_COMPARISON_LESS_EQUAL", D3D11_COMPARISON_LESS_EQUAL },
			{ "COMPARISON_LESS_EQUAL", D3D11_COMPARISON_LESS_EQUAL },
			{ "LESS_EQUAL", D3D11_COMPARISON_LESS_EQUAL },
			{ "Less_Equal", D3D11_COMPARISON_LESS_EQUAL },
			{ "4", D3D11_COMPARISON_LESS_EQUAL },
			{ "D3D11_COMPARISON_GREATER", D3D11_COMPARISON_GREATER },
			{ "COMPARISON_GREATER", D3D11_COMPARISON_GREATER },
			{ "GREATER", D3D11_COMPARISON_GREATER },
			{ "Greater", D3D11_COMPARISON_GREATER },
			{ "5", D3D11_COMPARISON_GREATER },
			{ "D3D11_COMPARISON_NOT_EQUAL", D3D11_COMPARISON_NOT_EQUAL },
			{ "COMPARISON_NOT_EQUAL", D3D11_COMPARISON_NOT_EQUAL },
			{ "NOT_EQUAL", D3D11_COMPARISON_NOT_EQUAL },
			{ "Not_Equal", D3D11_COMPARISON_NOT_EQUAL },
			{ "6", D3D11_COMPARISON_NOT_EQUAL },
			{ "D3D11_COMPARISON_GREATER_EQUAL", D3D11_COMPARISON_GREATER_EQUAL },
			{ "COMPARISON_GREATER_EQUAL", D3D11_COMPARISON_GREATER_EQUAL },
			{ "GREATER_EQUAL", D3D11_COMPARISON_GREATER_EQUAL },
			{ "Greater_Equal", D3D11_COMPARISON_GREATER_EQUAL },
			{ "7", D3D11_COMPARISON_GREATER_EQUAL },
			{ "D3D11_COMPARISON_ALWAYS", D3D11_COMPARISON_ALWAYS },
			{ "COMPARISON_ALWAYS", D3D11_COMPARISON_ALWAYS },
			{ "ALWAYS", D3D11_COMPARISON_ALWAYS },
			{ "Always", D3D11_COMPARISON_ALWAYS },
			{ "8", D3D11_COMPARISON_ALWAYS },
		}
};

const Parser<D3D11_STENCIL_OP> DXHelper::stencilOp = {
		{
			{ "D3D11_STENCIL_OP_KEEP", D3D11_STENCIL_OP_KEEP },
			{ "OP_KEEP", D3D11_STENCIL_OP_KEEP },
			{ "KEEP", D3D11_STENCIL_OP_KEEP },
			{ "Keep", D3D11_STENCIL_OP_KEEP },
			{ "1", D3D11_STENCIL_OP_KEEP },
			{ "D3D11_STENCIL_OP_ZERO", D3D11_STENCIL_OP_ZERO },
			{ "OP_ZERO", D3D11_STENCIL_OP_ZERO },
			{ "ZERO", D3D11_STENCIL_OP_ZERO },
			{ "Zero", D3D11_STENCIL_OP_ZERO },
			{ "0", D3D11_STENCIL_OP_ZERO },
			{ "2", D3D11_STENCIL_OP_ZERO },
			{ "D3D11_STENCIL_OP_REPLACE", D3D11_STENCIL_OP_REPLACE },
			{ "OP_REPLACE", D3D11_STENCIL_OP_REPLACE },
			{ "REPLACE", D3D11_STENCIL_OP_REPLACE },
			{ "Replace", D3D11_STENCIL_OP_REPLACE },
			{ "3", D3D11_STENCIL_OP_REPLACE },
			{ "D3D11_STENCIL_OP_INCR_SAT", D3D11_STENCIL_OP_INCR_SAT },
			{ "OP_INCR_SAT", D3D11_STENCIL_OP_INCR_SAT },
			{ "INCR_SAT", D3D11_STENCIL_OP_INCR_SAT },
			{ "Incr_Sat", D3D11_STENCIL_OP_INCR_SAT },
			{ "4", D3D11_STENCIL_OP_INCR_SAT },
			{ "D3D11_STENCIL_OP_DECR_SAT", D3D11_STENCIL_OP_DECR_SAT },
			{ "OP_DECR_SAT", D3D11_STENCIL_OP_DECR_SAT },
			{ "DECR_SAT", D3D11_STENCIL_OP_DECR_SAT },
			{ "Decr_Sat", D3D11_STENCIL_OP_DECR_SAT },
			{ "5", D3D11_STENCIL_OP_DECR_SAT },
			{ "D3D11_STENCIL_OP_INVERT", D3D11_STENCIL_OP_INVERT },
			{ "OP_INVERT", D3D11_STENCIL_OP_INVERT },
			{ "INVERT", D3D11_STENCIL_OP_INVERT },
			{ "Invert", D3D11_STENCIL_OP_INVERT },
			{ "6", D3D11_STENCIL_OP_INVERT },
			{ "D3D11_STENCIL_OP_INCR", D3D11_STENCIL_OP_INCR },
			{ "OP_INCR", D3D11_STENCIL_OP_INCR },
			{ "INCR", D3D11_STENCIL_OP_INCR },
			{ "Incr", D3D11_STENCIL_OP_INCR },
			{ "++", D3D11_STENCIL_OP_INCR },
			{ "7", D3D11_STENCIL_OP_INCR },
			{ "D3D11_STENCIL_OP_DECR", D3D11_STENCIL_OP_DECR },
			{ "OP_DECR", D3D11_STENCIL_OP_DECR },
			{ "DECR", D3D11_STENCIL_OP_DECR },
			{ "Decr", D3D11_STENCIL_OP_DECR },
			{ "--", D3D11_STENCIL_OP_DECR },
			{ "8", D3D11_STENCIL_OP_DECR }
		}
};

const Parser<D3D11_DEPTH_WRITE_MASK> DXHelper::depthWriteMask = {
		{
			{ "D3D11_DEPTH_WRITE_MASK_ZERO", D3D11_DEPTH_WRITE_MASK_ZERO },
			{ "WRITE_MASK_ZERO", D3D11_DEPTH_WRITE_MASK_ZERO },
			{ "ZERO", D3D11_DEPTH_WRITE_MASK_ZERO },
			{ "Zero", D3D11_DEPTH_WRITE_MASK_ZERO },
			{ "0", D3D11_DEPTH_WRITE_MASK_ZERO },
			{ "D3D11_DEPTH_WRITE_MASK_ALL", D3D11_DEPTH_WRITE_MASK_ALL },
			{ "WRITE_MASK_ALL", D3D11_DEPTH_WRITE_MASK_ALL },
			{ "ALL", D3D11_DEPTH_WRITE_MASK_ALL },
			{ "All", D3D11_DEPTH_WRITE_MASK_ALL },
			{ "1", D3D11_DEPTH_WRITE_MASK_ALL }
		}
};

const Parser<D3D11_BLEND_OP> DXHelper::blendOp = {
		{
			{ "D3D11_BLEND_OP_ADD", D3D11_BLEND_OP_ADD },
			{ "OP_ADD", D3D11_BLEND_OP_ADD },
			{ "ADD", D3D11_BLEND_OP_ADD },
			{ "Add", D3D11_BLEND_OP_ADD },
			{ "1", D3D11_BLEND_OP_ADD },
			{ "D3D11_BLEND_OP_SUBTRACT", D3D11_BLEND_OP_SUBTRACT },
			{ "OP_SUBTRACT", D3D11_BLEND_OP_SUBTRACT },
			{ "SUBTRACT", D3D11_BLEND_OP_SUBTRACT },
			{ "Subtract", D3D11_BLEND_OP_SUBTRACT },
			{ "2", D3D11_BLEND_OP_SUBTRACT },
			{ "D3D11_BLEND_OP_REV_SUBTRACT", D3D11_BLEND_OP_REV_SUBTRACT },
			{ "OP_REV_SUBTRACT", D3D11_BLEND_OP_REV_SUBTRACT },
			{ "REV_SUBTRACT", D3D11_BLEND_OP_REV_SUBTRACT },
			{ "Rev_Subtract", D3D11_BLEND_OP_REV_SUBTRACT },
			{ "3", D3D11_BLEND_OP_REV_SUBTRACT },
			{ "D3D11_BLEND_OP_MIN", D3D11_BLEND_OP_MIN },
			{ "OP_MIN", D3D11_BLEND_OP_MIN },
			{ "MIN", D3D11_BLEND_OP_MIN },
			{ "Min", D3D11_BLEND_OP_MIN },
			{ "4", D3D11_BLEND_OP_MIN },
			{ "D3D11_BLEND_OP_MAX", D3D11_BLEND_OP_MAX },
			{ "OP_MAX", D3D11_BLEND_OP_MAX },
			{ "MAX", D3D11_BLEND_OP_MAX },
			{ "Max", D3D11_BLEND_OP_MAX },
			{ "5", D3D11_BLEND_OP_MAX }
		}
};

const Parser<D3D11_BLEND> DXHelper::blend = {
		{
			{ "D3D11_BLEND_ZERO", D3D11_BLEND_ZERO },
			{ "ZERO", D3D11_BLEND_ZERO },
			{ "Zero", D3D11_BLEND_ZERO },
			{ "0", D3D11_BLEND_ZERO },
			{ "1", D3D11_BLEND_ZERO },
			{ "D3D11_BLEND_ONE", D3D11_BLEND_ONE },
			{ "ONE", D3D11_BLEND_ONE },
			{ "One", D3D11_BLEND_ONE },
			{ "1", D3D11_BLEND_ONE },
			{ "2", D3D11_BLEND_ONE },
			{ "D3D11_BLEND_SRC_COLOR", D3D11_BLEND_SRC_COLOR },
			{ "SRC_COLOR", D3D11_BLEND_SRC_COLOR },
			{ "Src_Color", D3D11_BLEND_SRC_COLOR },
			{ "Src_Colour", D3D11_BLEND_SRC_COLOR },
			{ "3", D3D11_BLEND_SRC_COLOR },
			{ "D3D11_BLEND_INV_SRC_COLOR", D3D11_BLEND_INV_SRC_COLOR },
			{ "INV_SRC_COLOR", D3D11_BLEND_INV_SRC_COLOR },
			{ "Inv_Src_Color", D3D11_BLEND_INV_SRC_COLOR },
			{ "Inv_Src_Colour", D3D11_BLEND_INV_SRC_COLOR },
			{ "4", D3D11_BLEND_INV_SRC_COLOR },
			{ "D3D11_BLEND_SRC_ALPHA", D3D11_BLEND_SRC_ALPHA },
			{ "SRC_ALPHA", D3D11_BLEND_SRC_ALPHA },
			{ "Src_Alpha", D3D11_BLEND_SRC_ALPHA },
			{ "5", D3D11_BLEND_SRC_ALPHA },
			{ "D3D11_BLEND_INV_SRC_ALPHA", D3D11_BLEND_INV_SRC_ALPHA },
			{ "INV_SRC_ALPHA", D3D11_BLEND_INV_SRC_ALPHA },
			{ "Inv_Src_Alpha", D3D11_BLEND_INV_SRC_ALPHA },
			{ "6", D3D11_BLEND_INV_SRC_ALPHA },
			{ "D3D11_BLEND_DEST_ALPHA", D3D11_BLEND_DEST_ALPHA },
			{ "DEST_ALPHA", D3D11_BLEND_DEST_ALPHA },
			{ "Dest_Alpha", D3D11_BLEND_DEST_ALPHA },
			{ "7", D3D11_BLEND_DEST_ALPHA },
			{ "D3D11_BLEND_INV_DEST_ALPHA", D3D11_BLEND_INV_DEST_ALPHA },
			{ "INV_DEST_ALPHA", D3D11_BLEND_INV_DEST_ALPHA },
			{ "Inv_Dest_Alpha", D3D11_BLEND_INV_DEST_ALPHA },
			{ "8", D3D11_BLEND_INV_DEST_ALPHA },
			{ "D3D11_BLEND_DEST_COLOR", D3D11_BLEND_DEST_COLOR },
			{ "DEST_COLOR", D3D11_BLEND_DEST_COLOR },
			{ "Dest_Color", D3D11_BLEND_DEST_COLOR },
			{ "Dest_Colour", D3D11_BLEND_DEST_COLOR },
			{ "9", D3D11_BLEND_DEST_COLOR },
			{ "D3D11_BLEND_INV_DEST_COLOR", D3D11_BLEND_INV_DEST_COLOR },
			{ "INV_DEST_COLOR", D3D11_BLEND_INV_DEST_COLOR },
			{ "Inv_Dest_Color", D3D11_BLEND_INV_DEST_COLOR },
			{ "Inv_Dest_Colour", D3D11_BLEND_INV_DEST_COLOR },
			{ "10", D3D11_BLEND_INV_DEST_COLOR },
			{ "D3D11_BLEND_SRC_ALPHA_SAT", D3D11_BLEND_SRC_ALPHA_SAT },
			{ "SRC_ALPHA_SAT", D3D11_BLEND_SRC_ALPHA_SAT },
			{ "Src_Alpha_Sat", D3D11_BLEND_SRC_ALPHA_SAT },
			{ "11", D3D11_BLEND_SRC_ALPHA_SAT },
			{ "D3D11_BLEND_BLEND_FACTOR", D3D11_BLEND_BLEND_FACTOR },
			{ "BLEND_FACTOR", D3D11_BLEND_BLEND_FACTOR },
			{ "Blend_Factor", D3D11_BLEND_BLEND_FACTOR },
			{ "14", D3D11_BLEND_BLEND_FACTOR },
			{ "D3D11_BLEND_INV_BLEND_FACTOR", D3D11_BLEND_INV_BLEND_FACTOR },
			{ "INV_BLEND_FACTOR", D3D11_BLEND_INV_BLEND_FACTOR },
			{ "Inv_Blend_Factor", D3D11_BLEND_INV_BLEND_FACTOR },
			{ "15", D3D11_BLEND_INV_BLEND_FACTOR },
			{ "D3D11_BLEND_SRC1_COLOR", D3D11_BLEND_SRC1_COLOR },
			{ "SRC1_COLOR", D3D11_BLEND_SRC1_COLOR },
			{ "Src1_Color", D3D11_BLEND_SRC1_COLOR },
			{ "Src1_Colour", D3D11_BLEND_SRC1_COLOR },
			{ "16", D3D11_BLEND_SRC1_COLOR },
			{ "D3D11_BLEND_INV_SRC1_COLOR", D3D11_BLEND_INV_SRC1_COLOR },
			{ "INV_SRC1_COLOR", D3D11_BLEND_INV_SRC1_COLOR },
			{ "Inv_Src1_Color", D3D11_BLEND_INV_SRC1_COLOR },
			{ "Inv_Src1_Colour", D3D11_BLEND_INV_SRC1_COLOR },
			{ "17", D3D11_BLEND_INV_SRC1_COLOR },
			{ "D3D11_BLEND_SRC1_ALPHA", D3D11_BLEND_SRC1_ALPHA },
			{ "SRC1_ALPHA", D3D11_BLEND_SRC1_ALPHA },
			{ "Src1_Alpha", D3D11_BLEND_SRC1_ALPHA },
			{ "18", D3D11_BLEND_SRC1_ALPHA },
			{ "D3D11_BLEND_INV_SRC1_ALPHA", D3D11_BLEND_INV_SRC1_ALPHA },
			{ "INV_SRC1_ALPHA", D3D11_BLEND_INV_SRC1_ALPHA },
			{ "Inv_Src1_Alpha", D3D11_BLEND_INV_SRC1_ALPHA },
			{ "19", D3D11_BLEND_INV_SRC1_ALPHA }
		}
};

const Parser<UINT8> DXHelper::mask = {
		{
			{ "D3D11_COLOR_WRITE_ENABLE_RED", 1 },
			{ "WRITE_ENABLE_RED", 1 },
			{ "Write_Enable_Red", 1 },
			{ "Red", 1 },
			{ "1", 1 },
			{ "D3D11_COLOR_WRITE_ENABLE_GREEN", 2 },
			{ "WRITE_ENABLE_GREEN", 2 },
			{ "Write_Enable_Green", 2 },
			{ "Green", 2 },
			{ "2", 2 },
			{ "D3D11_COLOR_WRITE_ENABLE_BLUE", 4 },
			{ "WRITE_ENABLE_BLUE", 4 },
			{ "Write_Enable_Blue", 4 },
			{ "Blue", 4 },
			{ "4", 4 },
			{ "D3D11_COLOR_WRITE_ENABLE_ALPHA", 8 },
			{ "WRITE_ENABLE_ALPHA", 8 },
			{ "Write_Enable_Alpha", 8 },
			{ "Alpha", 8 },
			{ "8", 8 },
			{ "D3D11_COLOR_WRITE_ENABLE_ALL", RGBAChannels },
			{ "WRITE_ENABLE_ALL", RGBAChannels },
			{ "Write_Enable_All", RGBAChannels },
			{ "All", RGBAChannels },
			{ "D3D11_DEFAULT_STENCIL_READ_MASK", 255 },
			{ "DEFAULT_STENCIL_READ_MASK", 255 },
			{ "Default_Stencil_Read_Mask", 255 },
			{ "0xff", 255 },
			{ "D3D11_DEFAULT_STENCIL_WRITE_MASK", 255 },
			{ "DEFAULT_STENCIL_WRITE_MASK", 255 },
			{ "Default_Stencil_Write_Mask", 255 }
		}
};

const UINT8 DXHelper::RGBAChannels = (((D3D11_COLOR_WRITE_ENABLE_RED | D3D11_COLOR_WRITE_ENABLE_GREEN) | D3D11_COLOR_WRITE_ENABLE_BLUE) | D3D11_COLOR_WRITE_ENABLE_ALPHA);

XMFLOAT2 const * DXHelper::InfinityVector2Ptr() {
	return &InfinityVector2;
}

XMFLOAT3 const * DXHelper::InfinityVector3Ptr() {
	return &InfinityVector3;
}

XMFLOAT4 const * DXHelper::InfinityVector4Ptr() {
	return &InfinityVector4;
}

float DXHelper::RadiansToDegrees(float rad) {
	return rad * 180 * XM_1DIVPI;
}

float DXHelper::DegreesToRadians(float deg) {
	return deg * (XM_PI / 180.0f);
}

XMFLOAT2 DXHelper::Normalise(XMFLOAT2 input) {
	float mag = sqrt((input.x * input.x) + (input.y * input.y));
	return XMFLOAT2(input.x / mag, input.y / mag);
}

XMFLOAT3 DXHelper::Normalise(XMFLOAT3 input) {
	float mag = sqrt((input.x * input.x) + (input.y * input.y) + (input.z * input.z));
	return XMFLOAT3(input.x / mag, input.y / mag, input.z / mag);
}

XMFLOAT4 DXHelper::Normalise(XMFLOAT4 input) {
	float mag = sqrt((input.x * input.x) + (input.y * input.y) + (input.z * input.z) + (input.w * input.w));
	return XMFLOAT4(input.x / mag, input.y / mag, input.z / mag, input.w / mag);
}

XMFLOAT2 DXHelper::Add(XMFLOAT2 a, XMFLOAT2 b) {
	return XMFLOAT2(a.x + b.x, a.y + b.y);
}

XMFLOAT2 DXHelper::Add(XMFLOAT2 a, float bX, float bY) {
	return XMFLOAT2(a.x + bX, a.y + bY);
}

XMFLOAT3 DXHelper::Add(XMFLOAT3 a, XMFLOAT3 b) {
	return XMFLOAT3(a.x + b.x, a.y + b.y, a.z + b.z);
}

XMFLOAT3 DXHelper::Add(XMFLOAT3 a, float bX, float bY, float bZ) {
	return XMFLOAT3(a.x + bX, a.y + bY, a.z + bZ);
}

XMFLOAT4 DXHelper::Add(XMFLOAT4 a, XMFLOAT4 b) {
	return XMFLOAT4(a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w);
}

XMFLOAT4 DXHelper::Add(XMFLOAT4 a, float bX, float bY, float bZ, float bW) {
	return XMFLOAT4(a.x + bX, a.y + bY, a.z + bZ, a.w + bW);
}

XMFLOAT2 DXHelper::Subtract(XMFLOAT2 a, XMFLOAT2 b) {
	return XMFLOAT2(a.x - b.x, a.y - b.y);
}

XMFLOAT2 DXHelper::Subtract(XMFLOAT2 a, float bX, float bY) {
	return XMFLOAT2(a.x - bX, a.y - bY);
}

XMFLOAT3 DXHelper::Subtract(XMFLOAT3 a, XMFLOAT3 b) {
	return XMFLOAT3(a.x - b.x, a.y - b.y, a.z - b.z);
}

XMFLOAT3 DXHelper::Subtract(XMFLOAT3 a, float bX, float bY, float bZ) {
	return XMFLOAT3(a.x - bX, a.y - bY, a.z - bZ);
}

XMFLOAT4 DXHelper::Subtract(XMFLOAT4 a, XMFLOAT4 b) {
	return XMFLOAT4(a.x - b.x, a.y - b.y, a.z - b.z, a.w - b.w);
}

XMFLOAT4 DXHelper::Subtract(XMFLOAT4 a, float bX, float bY, float bZ, float bW) {
	return XMFLOAT4(a.x - bX, a.y - bY, a.z - bZ, a.w - bW);
}

XMFLOAT2 DXHelper::Multiply(XMFLOAT2 a, float b) {
	return XMFLOAT2(a.x * b, a.y * b);
}

XMFLOAT3 DXHelper::Multiply(XMFLOAT3 a, float b) {
	return XMFLOAT3(a.x * b, a.y * b, a.z * b);
}

XMFLOAT4 DXHelper::Multiply(XMFLOAT4 a, float b) {
	return XMFLOAT4(a.x * b, a.y * b, a.z * b, a.w * b);
}

XMFLOAT2 DXHelper::Divide(XMFLOAT2 a, float b) {
	return XMFLOAT2(a.x / b, a.y / b);
}

XMFLOAT3 DXHelper::Divide(XMFLOAT3 a, float b) {
	return XMFLOAT3(a.x / b, a.y / b, a.z / b);
}

XMFLOAT4 DXHelper::Divide(XMFLOAT4 a, float b) {
	return XMFLOAT4(a.x / b, a.y / b, a.z / b, a.w / b);
}

float DXHelper::Dot(XMFLOAT2 a, XMFLOAT2 b) {
	return (a.x * b.x) + (a.y * b.y);
}

float DXHelper::Dot(XMFLOAT3 a, XMFLOAT3 b) {
	return (a.x * b.x) + (a.y * b.y) + (a.z * b.z);
}

float DXHelper::Dot(XMFLOAT4 a, XMFLOAT4 b) {
	return (a.x * b.x) + (a.y * b.y) + (a.z * b.z) + (a.w * b.w);
}

XMFLOAT3 DXHelper::Cross(XMFLOAT3 a, XMFLOAT3 b) {
	return { a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x };
}

float DXHelper::AbsoluteDot(XMFLOAT2 a, XMFLOAT2 b) {
	return abs(a.x * b.x) + abs(a.y * b.y);
}

float DXHelper::AbsoluteDot(XMFLOAT3 a, XMFLOAT3 b) {
	return abs(a.x * b.x) + abs(a.y * b.y) + abs(a.z * b.z);
}

float DXHelper::AbsoluteDot(XMFLOAT4 a, XMFLOAT4 b) {
	return abs(a.x * b.x) + abs(a.y * b.y) + abs(a.z * b.z) + abs(a.w * b.w);
}

bool DXHelper::Equals(XMFLOAT2 a, XMFLOAT2 b) {
	return a.x == b.x && a.y == b.y;
}

bool DXHelper::Equals(XMFLOAT3 a, XMFLOAT3 b) {
	return a.x == b.x && a.y == b.y && a.z == b.z;
}

bool DXHelper::Equals(XMFLOAT4 a, XMFLOAT4 b) {
	return a.x == b.x && a.y == b.y && a.z == b.z && a.w == b.w;
}

bool DXHelper::IsNormalised(XMFLOAT2 v) {
	return (((v.x * v.x) + (v.y * v.y)) == 1.0f);
}

bool DXHelper::IsNormalised(XMFLOAT3 v) {
	return (((v.x * v.x) + (v.y * v.y) + (v.z * v.z)) == 1.0f);
}

bool DXHelper::IsNormalised(XMFLOAT4 v) {
	return (((v.x * v.x) + (v.y * v.y) + (v.z * v.z) + (v.w * v.w)) == 1.0f);
}

XMFLOAT3 DXHelper::QuaternionToEuler(XMFLOAT4 quat, bool toDegrees) {
	XMFLOAT3 euler = XMFLOAT3(0, 0, 0);
	float sqw = quat.w * quat.w, sqx = quat.x * quat.x, sqy = quat.y * quat.y, sqz = quat.z * quat.z;
	float unit = sqx + sqy + sqz + sqw; // if normalised is one, otherwise is correction factor
	float test = (quat.x * quat.y) + (quat.z * quat.w);

	if(test > (0.499f * unit)) { // singularity at north pole
		euler.x = 0;
		euler.y = 2.0f * atan2(quat.x, quat.w);
		euler.z = XM_PIDIV2;
	} else if(test < (-0.499f * unit)) { // singularity at south pole
		euler.x = 0;
		euler.y = -2.0f * atan2(quat.x, quat.w);
		euler.z = -XM_PIDIV2;
	} else {
		euler.x = atan2(2.0f * quat.x * quat.w - 2.0f * quat.y * quat.z, -sqx + sqy - sqz + sqw);
		euler.y = atan2(2.0f * quat.y * quat.w - 2.0f * quat.x * quat.z, sqx - sqy - sqz + sqw);
		euler.z = asin(2.0f * test / unit);
	}

	return (toDegrees ? XMFLOAT3(RadiansToDegrees(euler.x), RadiansToDegrees(euler.y), RadiansToDegrees(euler.z)) : euler);
}

XMFLOAT4 DXHelper::CombineQuaternions(XMFLOAT4 lhs, XMFLOAT4 rhs) {
	return XMFLOAT4(lhs.w * rhs.x + lhs.x * rhs.w + lhs.y * rhs.z - lhs.z * rhs.y, lhs.w * rhs.y + lhs.y * rhs.w + lhs.z * rhs.x - lhs.x * rhs.z, lhs.w * rhs.z + lhs.z * rhs.w + lhs.x * rhs.y - lhs.y * rhs.x, lhs.w * rhs.w - lhs.x * rhs.x - lhs.y * rhs.y - lhs.z * rhs.z);
}

float DXHelper::Lerp(float p1, float p2, float t) {
	return ((1.0f - t) * p1) + (t * p2);
}

XMFLOAT2 DXHelper::Lerp(XMFLOAT2 p1, XMFLOAT2 p2, float t) {
	return XMFLOAT2(((1.0f - t) * p1.x) + (t * p2.x), ((1.0f - t) * p1.y) + (t * p2.y));
}

XMFLOAT3 DXHelper::Lerp(XMFLOAT3 p1, XMFLOAT3 p2, float t) {
	return XMFLOAT3(((1.0f - t) * p1.x) + (t * p2.x), ((1.0f - t) * p1.y) + (t * p2.y), ((1.0f - t) * p1.z) + (t * p2.z));
}

XMFLOAT4 DXHelper::Lerp(XMFLOAT4 p1, XMFLOAT4 p2, float t) {
	return XMFLOAT4(((1.0f - t) * p1.x) + (t * p2.x), ((1.0f - t) * p1.y) + (t * p2.y), ((1.0f - t) * p1.z) + (t * p2.z), ((1.0f - t) * p1.w) + (t * p2.w));
}

bool DXHelper::NearlyEqual(float a, float b, float threshold) {
	float diff = abs(b - a);

	return (diff <= threshold);
}

XMFLOAT4 DXHelper::AToBQuaternion(XMFLOAT3 a, XMFLOAT3 b, XMFLOAT3 up) {
	XMFLOAT3 calc;
	XMFLOAT4 ret = XMFLOAT4(0, 0, 0, 1);
	XMVECTOR outA = XMLoadFloat3(&a), outB = XMLoadFloat3(&b), outUp = XMLoadFloat3(&up), outCalc;

	if(!IsNormalised(a))
		outA = XMVector3Normalize(outA);
	if(!IsNormalised(b))
		outB = XMVector3Normalize(outB);
	if(!IsNormalised(up))
		outUp = XMVector3Normalize(outUp);

	outCalc = XMVector3Dot(outA, outB);
	XMStoreFloat3(&calc, outCalc);
	float dot = calc.x;

	if(NearlyEqual(dot, -1.0f, 0.000001f)) {
		outCalc = XMQuaternionRotationAxis(outUp, XM_PI);
		XMStoreFloat4(&ret, outCalc);
	} else if(!NearlyEqual(dot, 1.0f, 0.000001f)) {
		outCalc = XMVector3Cross(outA, outB);
		outCalc = XMVector3Normalize(outCalc);
		outCalc = XMQuaternionRotationAxis(outCalc, acos(dot));
		XMStoreFloat4(&ret, outCalc);
	}

	return ret;
}

D3D11_CULL_MODE DXHelper::ToCullMode(string value) {
	D3D11_CULL_MODE ret = D3D11_CULL_NONE;
	if(!cullMode.Parse(value, ret)) {
		string temp = "Could not parse " + value + ". Defaulting to CULL_NONE.";
		OutputDebugStringA(temp.c_str());
	}
	return ret;
}

D3D11_FILL_MODE DXHelper::ToFillMode(string value) {
	D3D11_FILL_MODE ret = D3D11_FILL_SOLID;
	if(!fillMode.Parse(value, ret)) {
		string temp = "Could not parse " + value + ". Defaulting to FILL_SOLID.";
		OutputDebugStringA(temp.c_str());
	}
	return ret;
}

D3D11_COMPARISON_FUNC DXHelper::ToCompareFunc(string value) {
	D3D11_COMPARISON_FUNC ret = D3D11_COMPARISON_ALWAYS;
	if(!compareFunc.Parse(value, ret)) {
		string temp = "Could not parse " + value + ". Defaulting to COMPARISON_ALWAYS.";
		OutputDebugStringA(temp.c_str());
	}
	return ret;
}

D3D11_STENCIL_OP DXHelper::ToStencilOp(string value) {
	D3D11_STENCIL_OP ret = D3D11_STENCIL_OP_KEEP;
	if(!stencilOp.Parse(value, ret)) {
		string temp = "Could not parse " + value + ". Defaulting to KEEP.";
		OutputDebugStringA(temp.c_str());
	}
	return ret;
}

D3D11_DEPTH_WRITE_MASK DXHelper::ToDepthWriteMask(string value) {
	D3D11_DEPTH_WRITE_MASK ret = D3D11_DEPTH_WRITE_MASK_ALL;
	if(!depthWriteMask.Parse(value, ret)) {
		string temp = "Could not parse " + value + ". Defaulting to MASK_ALL.";
		OutputDebugStringA(temp.c_str());
	}
	return ret;
}

D3D11_BLEND_OP DXHelper::ToBlendOp(string value) {
	D3D11_BLEND_OP ret = D3D11_BLEND_OP_ADD;
	if(!blendOp.Parse(value, ret)) {
		string temp = "Could not parse " + value + ". Defaulting to ADD.";
		OutputDebugStringA(temp.c_str());
	}
	return ret;
}

D3D11_BLEND DXHelper::ToBlend(string value) {
	D3D11_BLEND ret = D3D11_BLEND_ONE;
	if(!blend.Parse(value, ret)) {
		string temp = "Could not parse " + value + ". Defaulting to ONE.";
		OutputDebugStringA(temp.c_str());
	}
	return ret;
}

UINT8 DXHelper::ToMask(string value) {
	UINT8 ret = 255;
	if(!mask.Parse(value, ret)) {
		string temp = "Could not parse " + value + ". Defaulting to 255.";
		OutputDebugStringA(temp.c_str());
	}
	return ret;
}