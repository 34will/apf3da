#include "MeshGeneration.h"
#include "DirectX11App.h"

#define OneThird 0.33333333f
#define TwoThirds 0.66666666f

const Parser<MeshGenerationType> MeshGeneration::mgtParser = {
		{
			{ "None", MeshGenerationType::None },
			{ "Plane", MeshGenerationType::Plane },
			{ "Cube", MeshGenerationType::Cube },
			{ "Sphere", MeshGenerationType::Sphere },
			{ "CubeSphere", MeshGenerationType::CubeSphere }
		}
};

const Parser<CubeMapTextureType> MeshGeneration::cmttParser = {
		{
			{ "None", CubeMapTextureType::None },
			{ "OneTexture", CubeMapTextureType::OneTexture },
			{ "ThreeByTwo", CubeMapTextureType::ThreeByTwo },
			{ "TNet", CubeMapTextureType::TNet },
			{ "CrossNet", CubeMapTextureType::CrossNet }
		}
};

bool MeshGeneration::GeneratePlane(Mesh * mesh, Material * const materials, const unsigned int & numMaterials, float width, float depth, unsigned int numRows, unsigned int numCols, XMFLOAT3 offset) {
	if(width <= 0.0f || depth <= 0.0f || numRows == 0 || numCols == 0)
		return false;

	float dX = width / numCols, dZ = depth / numRows, halfWidth = (0.5f * width), halfDepth = (0.5f * depth);
	unsigned int m = (numCols + 1), n = (numRows + 1), numVerts = (m * n), numInds = (numCols * numRows * 6), k = 0;

	Vertex * vertices = new Vertex[numVerts];
	unsigned int * indices = new unsigned int[numInds];

	for(unsigned int j = 0; j < n; j++) {
		for(unsigned int i = 0; i < m; i++) {
			vertices[k] = Vertex({ offset.x + ((j * dX) - halfWidth), offset.y, offset.z + ((i * dZ) - halfDepth) }, { 0, 1.0f, 0 }, { ((float)i / (float)numCols), ((float)j / (float)numRows) });
			k++;
		}
	}

	k = 0;
	for(unsigned int j = 0; j < numRows; j++) {
		for(unsigned int i = 0; i < numCols; i++) {
			indices[k] = (i * m) + j;
			indices[k + 1] = (i * m) + j + 1;
			indices[k + 2] = ((i + 1) * m) + j;
			indices[k + 3] = ((i + 1) * m) + j;
			indices[k + 4] = (i * m) + j + 1;
			indices[k + 5] = ((i + 1) * m) + j + 1;

			k += 6;
		}
	}

	mesh->Create(vertices, numVerts, indices, numInds, materials, numMaterials);

	delete[] vertices;
	delete[] indices;

	return !FAILED(mesh->BuildBuffers(DirectX11App::Current()->GetDevice()));
}

/*bool MeshGeneration::GenerateCube(Mesh * mesh, Material * const materials, const unsigned int & numMaterials, float diameter, unsigned int numberOfSubdivisions, XMFLOAT3 offset) {
	if(numberOfSubdivisions == 0 || diameter <= 0.0f)
	return false;

	float delta = diameter / numberOfSubdivisions, radius = (0.5f * diameter);
	unsigned int mm = numberOfSubdivisions - 2, m = numberOfSubdivisions - 1, p = numberOfSubdivisions + 1;
	unsigned int numVertsPerFace = p * p, numVertsFrontBack = p * m, numVertsLeftRight = m * m, numVerts = (m * 12) + (m * m * 6) + 8, numInds = (numberOfSubdivisions * numberOfSubdivisions * 36), k = 0;
	unsigned int bottomOffset = numVertsPerFace, frontOffset = numVertsPerFace * 2, backOffset = frontOffset + numVertsFrontBack, leftOffset = backOffset + numVertsFrontBack, rightOffset = leftOffset + numVertsLeftRight;

	Vertex * vertices = new Vertex[numVerts];
	unsigned int * indices = new unsigned int[numInds];

	for(unsigned int j = 0; j < p; j++) {
	for(unsigned int i = 0; i < p; i++) {
	XMFLOAT2 texCoord = { ((float)i / (float)numberOfSubdivisions), ((float)j / (float)numberOfSubdivisions) };
	if(i == 0) {
	texCoord.x = 0.75f + (texCoord.y * 0.25f);
	texCoord.y = 0.375f;
	} else if(i == 1) {
	if(j == 1) {
	texCoord.x = 0.83333f;
	texCoord.y = 0.3125f;
	} else if(j == 2) {
	texCoord.x = 0.91666f;
	texCoord.y = 0.3125f;
	}
	} else if(i == 2) {
	if(j == 1) {
	texCoord.y = 0.3125f;
	texCoord.x = 0.43333f;
	} else if(j == 2) {
	texCoord.y = 0.3125f;
	texCoord.x = 0.31666f;
	}
	} else if(i == numberOfSubdivisions) {
	texCoord.x = 0.5f - (texCoord.y * 0.25f);
	texCoord.y = 0.375f;
	}

	vertices[k] = Vertex({ offset.x + ((j * delta) - radius), offset.y + radius, offset.z + ((i * delta) - radius) }, { 0, 1.0f, 0 }, texCoord);
	k++;
	}
	}

	for(unsigned int j = 0; j < p; j++) {
	for(unsigned int i = 0; i < p; i++) {
	vertices[k] = Vertex({ offset.x + (radius - (j * delta)), offset.y - radius, offset.z + ((i * delta) - radius) }, { 0, -1.0f, 0 }, { 1.0f - ((float)i / (float)numberOfSubdivisions), ((float)j / (float)numberOfSubdivisions) });
	k++;
	}
	}

	for(unsigned int j = 0; j < m; j++) {
	for(unsigned int i = 0; i < p; i++) {
	vertices[k] = Vertex({ offset.x + radius, offset.y + (radius - ((j + 1) * delta)), offset.z + ((i * delta) - radius) }, { 1.0f, 0, 0 }, { ((float)i / (float)numberOfSubdivisions) * 0.25f, 0.375f + (((float)(j + 1) / (float)numberOfSubdivisions) * 0.25f) });
	k++;
	}
	}

	for(unsigned int j = 0; j < m; j++) {
	for(unsigned int i = 0; i < p; i++) {
	vertices[k] = Vertex({ offset.x - radius, offset.y + (((j + 1) * delta) - radius), offset.z + ((i * delta) - radius) }, { -1.0f, 0, 0 }, { 0.5f + (((float)i / (float)numberOfSubdivisions) * 0.25f), 0.375f + (((float)(j + 1) / (float)numberOfSubdivisions) * 0.25f) });
	k++;
	}
	}

	for(unsigned int j = 0; j < m; j++) {
	for(unsigned int i = 0; i < m; i++) {
	vertices[k] = Vertex({ offset.x + (((i + 1) * delta) - radius), offset.y + (((j + 1) * delta) - radius), offset.z + radius }, { 0, 0, 1.0f }, { 0.25f + (((float)(i + 1) / (float)numberOfSubdivisions) * 0.25f), 0.375f + (((float)(j + 1) / (float)numberOfSubdivisions) * 0.25f) });
	k++;
	}
	}

	for(unsigned int j = 0; j < m; j++) {
	for(unsigned int i = 0; i < m; i++) {
	vertices[k] = Vertex({ offset.x + (radius - ((i + 1) * delta)), offset.y + (((j + 1) * delta) - radius), offset.z - radius }, { 0, 0, -1.0f }, { 0.75f + (((float)(i + 1) / (float)numberOfSubdivisions) * 0.25f), 0.375f + (((float)(j + 1) / (float)numberOfSubdivisions) * 0.25f) });
	k++;
	}
	}

	// Front Face
	k = 0;
	for(unsigned int j = 0; j < numberOfSubdivisions; j++) {
	for(unsigned int i = 0; i < numberOfSubdivisions; i++) {
	indices[k] = (j * p) + i;
	indices[k + 1] = (j * p) + i + 1;

	indices[k + 2] = ((j + 1) * p) + i;
	indices[k + 5] = ((j + 1) * p) + i + 1;

	indices[k + 3] = indices[k + 2];
	indices[k + 4] = indices[k + 1];

	k += 6;
	}
	}

	// Bottom Face
	for(unsigned int j = 0; j < numberOfSubdivisions; j++) {
	for(unsigned int i = 0; i < numberOfSubdivisions; i++) {
	indices[k] = bottomOffset + ((j * p) + i);
	indices[k + 1] = bottomOffset + ((j * p) + i + 1);

	indices[k + 2] = bottomOffset + (((j + 1) * p) + i);
	indices[k + 5] = bottomOffset + (((j + 1) * p) + i + 1);

	indices[k + 3] = indices[k + 2];
	indices[k + 4] = indices[k + 1];

	k += 6;
	}
	}

	// Front Face
	for(unsigned int j = 0; j < numberOfSubdivisions; j++) {
	for(unsigned int i = 0; i < numberOfSubdivisions; i++) {
	indices[k] = (j == 0 ? bottomOffset - p + i : frontOffset + (((j - 1) * p) + i));
	indices[k + 1] = (j == 0 ? bottomOffset - p + i + 1 : frontOffset + (((j - 1) * p) + i + 1));

	indices[k + 2] = (j == m ? bottomOffset + i : frontOffset + ((j * p) + i));
	indices[k + 5] = (j == m ? bottomOffset + i + 1 : frontOffset + ((j * p) + i + 1));

	indices[k + 3] = indices[k + 2];
	indices[k + 4] = indices[k + 1];

	k += 6;
	}
	}

	// Back Face
	for(unsigned int j = 0; j < numberOfSubdivisions; j++) {
	for(unsigned int i = 0; i < numberOfSubdivisions; i++) {
	indices[k] = (j == 0 ? frontOffset - p + i : backOffset + (((j - 1) * p) + i));
	indices[k + 1] = (j == 0 ? frontOffset - p + i + 1 : backOffset + (((j - 1) * p) + i + 1));

	indices[k + 2] = (j == m ? i : backOffset + ((j * p) + i));
	indices[k + 5] = (j == m ? i + 1 : backOffset + ((j * p) + i + 1));

	indices[k + 3] = indices[k + 2];
	indices[k + 4] = indices[k + 1];

	k += 6;
	}
	}

	// Left Face
	for(unsigned int j = 0; j < numberOfSubdivisions; j++) {
	for(unsigned int i = 0; i < numberOfSubdivisions; i++) {
	if(j == 0) {
	indices[k] = frontOffset - (i * p) - 1;
	indices[k + 1] = frontOffset - ((i + 1) * p) - 1;

	if(i == 0)
	indices[k + 2] = backOffset + (1 * p) - 1;
	else
	indices[k + 2] = leftOffset + (i - 1);

	if(i == m)
	indices[k + 5] = backOffset - 1;
	else
	indices[k + 5] = leftOffset + i;
	} else if(j == m) {
	if(i == 0)
	indices[k] = backOffset + (j * p) - 1;
	else
	indices[k] = leftOffset + ((j - 1) * m) + (i - 1);

	if(i == m)
	indices[k + 1] = frontOffset + ((numberOfSubdivisions - j) * p) - 1;
	else
	indices[k + 1] = leftOffset + ((j - 1) * m) + i;

	indices[k + 2] = (i * p) + numberOfSubdivisions;
	indices[k + 5] = ((i + 1) * p) + numberOfSubdivisions;
	} else {
	if(i == 0)
	indices[k] = backOffset + (j * p) - 1;
	else
	indices[k] = leftOffset + ((j - 1) * m) + (i - 1);

	if(i == m)
	indices[k + 1] = frontOffset + ((numberOfSubdivisions - j) * p) - 1;
	else
	indices[k + 1] = leftOffset + ((j - 1) * m) + i;

	if(i == 0)
	indices[k + 2] = backOffset + ((j + 1) * p) - 1;
	else
	indices[k + 2] = leftOffset + (j * m) + (i - 1);

	if(i == m)
	indices[k + 5] = frontOffset + ((numberOfSubdivisions - j - 1) * p) - 1;
	else
	indices[k + 5] = leftOffset + (j * m) + i;
	}

	indices[k + 3] = indices[k + 2];
	indices[k + 4] = indices[k + 1];

	k += 6;
	}
	}

	// Right Face
	for(unsigned int j = 0; j < numberOfSubdivisions; j++) {
	for(unsigned int i = 0; i < numberOfSubdivisions; i++) {
	if(j == 0) {
	indices[k] = bottomOffset + (i * p);
	indices[k + 1] = bottomOffset + ((i + 1) * p);

	if(i == 0)
	indices[k + 2] = frontOffset + (mm * p);
	else
	indices[k + 2] = rightOffset + (i - 1);

	if(i == m)
	indices[k + 5] = backOffset;
	else
	indices[k + 5] = rightOffset + i;
	} else if(j == m) {
	if(i == 0)
	indices[k] = frontOffset;
	else
	indices[k] = rightOffset + ((j - 1) * m) + (i - 1);

	if(i == m)
	indices[k + 1] = backOffset + (mm * p);
	else
	indices[k + 1] = rightOffset + ((j - 1) * m) + i;

	indices[k + 2] = ((numberOfSubdivisions - i) * p);
	indices[k + 5] = ((numberOfSubdivisions - i - 1) * p);
	} else {
	if(i == 0)
	indices[k] = frontOffset + ((mm - (j - 1)) * p);
	else
	indices[k] = rightOffset + ((j - 1) * m) + (i - 1);

	if(i == m)
	indices[k + 1] = backOffset + ((j - 1) * p);
	else
	indices[k + 1] = rightOffset + ((j - 1) * m) + i;

	if(i == 0)
	indices[k + 2] = frontOffset + ((mm - j) * p);
	else
	indices[k + 2] = rightOffset + (j * m) + (i - 1);

	if(i == m)
	indices[k + 5] = backOffset + (j * p);
	else
	indices[k + 5] = rightOffset + (j * m) + i;
	}

	indices[k + 3] = indices[k + 2];
	indices[k + 4] = indices[k + 1];

	k += 6;
	}
	}

	mesh->Create(vertices, numVerts, indices, numInds, materials, numMaterials);

	delete[] vertices;
	delete[] indices;

	return !FAILED(mesh->BuildBuffers(DirectX11App::Current()->GetDevice()));
	}*/

XMFLOAT2 MeshGeneration::CubeMapTextureCoords(CubeMapTextureType type, unsigned int i, unsigned int j, unsigned int numSubdivisions, unsigned int face) {
	if(type == CubeMapTextureType::None || numSubdivisions == 0 || face > 6)
		return XMFLOAT2(0.0f, 0.0f);

	float sF = (float)numSubdivisions, iF = (float)i / sF, jF = (float)j / sF;

	if(type == CubeMapTextureType::ThreeByTwo)
		return XMFLOAT2(((face % 3) * OneThird) + (iF * OneThird), ((int)(face / 3) * OneThird) + (jF * OneThird));
	else if(type == CubeMapTextureType::TNet) {
		if(face == 0)
			return XMFLOAT2(iF * 0.25f, 0.125f + (jF * 0.25f));
		else if(face == 1)
			return XMFLOAT2(iF * 0.25f, 0.875f - (jF * 0.25f));
		else
			return XMFLOAT2(((face - 2) * 0.25f) + (iF * 0.25f), 0.375f + (jF * 0.25f));
	} else if(type == CubeMapTextureType::CrossNet) {
		if(face == 0)
			return XMFLOAT2(0.25f + (iF * 0.25f), 0.125f + (jF * 0.25f));
		else if(face == 1)
			return XMFLOAT2(0.25f + (iF * 0.25f), 0.875f - (jF * 0.25f));
		else
			return XMFLOAT2(((face - 2) * 0.25f) + (iF * 0.25f), 0.375f + (jF * 0.25f));
	} else
		return XMFLOAT2(iF, jF);
}

bool MeshGeneration::GenerateCube(Mesh * mesh, Material * const materials, const unsigned int & numMaterials, CubeMapTextureType textureType, float diameter, unsigned int numberOfSubdivisions, XMFLOAT3 offset) {
	if(numberOfSubdivisions == 0 || diameter <= 0.0f)
		return false;

	float delta = diameter / numberOfSubdivisions, radius = (0.5f * diameter);
	unsigned int p = numberOfSubdivisions + 1;
	unsigned int numVertsPerFace = p * p, numVerts = numVertsPerFace * 6, numInds = (numberOfSubdivisions * numberOfSubdivisions * 36), k = 0;

	Vertex * vertices = new Vertex[numVerts];
	unsigned int * indices = new unsigned int[numInds];

	for(unsigned int j = 0; j < p; j++) {
		for(unsigned int i = 0; i < p; i++) {
			vertices[k] = Vertex({ offset.x + ((j * delta) - radius), offset.y + radius, offset.z + ((i * delta) - radius) }, { 0, 1.0f, 0 }, CubeMapTextureCoords(textureType, i, j, numberOfSubdivisions, 0));
			vertices[numVertsPerFace + k] = Vertex({ offset.x + ((j * delta) - radius), offset.y - radius, offset.z + (radius - (i * delta)) }, { 0, -1.0f, 0 }, CubeMapTextureCoords(textureType, i, j, numberOfSubdivisions, 1));
			k++;
		}
	}
	k += numVertsPerFace;

	for(unsigned int j = 0; j < p; j++) {
		for(unsigned int i = 0; i < p; i++) {
			vertices[k] = Vertex({ offset.x + radius, offset.y + ((j * delta) - radius), offset.z + (radius - (i * delta)) }, { 1.0f, 0, 0 }, CubeMapTextureCoords(textureType, i, j, numberOfSubdivisions, 2));
			vertices[numVertsPerFace + k] = Vertex({ offset.x - radius, offset.y + ((j * delta) - radius), offset.z + ((i * delta) - radius) }, { -1.0f, 0, 0 }, CubeMapTextureCoords(textureType, i, j, numberOfSubdivisions, 3));
			k++;
		}
	}
	k += numVertsPerFace;

	for(unsigned int j = 0; j < p; j++) {
		for(unsigned int i = 0; i < p; i++) {
			vertices[k] = Vertex({ offset.x + ((i * delta) - radius), offset.y + ((j * delta) - radius), offset.z + radius }, { 0, 0, 1.0f }, CubeMapTextureCoords(textureType, i, j, numberOfSubdivisions, 4));
			vertices[numVertsPerFace + k] = Vertex({ offset.x + (radius - (i * delta)), offset.y + ((j * delta) - radius), offset.z - radius }, { 0, 0, -1.0f }, CubeMapTextureCoords(textureType, i, j, numberOfSubdivisions, 5));
			k++;
		}
	}
	k += numVertsPerFace;

	k = 0;
	for(unsigned int f = 0; f < 6; f++) {
		unsigned int faceOffset = f * numVertsPerFace;
		for(unsigned int j = 0; j < numberOfSubdivisions; j++) {
			for(unsigned int i = 0; i < numberOfSubdivisions; i++) {
				indices[k] = faceOffset + (j * p) + i;
				indices[k + 1] = faceOffset + (j * p) + i + 1;

				indices[k + 2] = faceOffset + ((j + 1) * p) + i;
				indices[k + 5] = faceOffset + ((j + 1) * p) + i + 1;

				indices[k + 3] = indices[k + 2];
				indices[k + 4] = indices[k + 1];

				k += 6;
			}
		}
	}

	mesh->Create(vertices, numVerts, indices, numInds, materials, numMaterials);

	delete[] vertices;
	delete[] indices;

	return !FAILED(mesh->BuildBuffers(DirectX11App::Current()->GetDevice()));
}

bool MeshGeneration::GenerateSphere(Mesh * mesh, Material * const materials, const unsigned int & numMaterials) {
	UnusedVariable(mesh);
	UnusedVariable(materials);
	UnusedVariable(numMaterials);
	return false;
}

bool MeshGeneration::GenerateCubeSphere(Mesh * mesh, Material * const materials, const unsigned int & numMaterials, CubeMapTextureType textureType, float diameter, unsigned int numberOfSubdivisions, XMFLOAT3 offset) {
	if(!GenerateCube(mesh, materials, numMaterials, textureType, diameter, numberOfSubdivisions, offset))
		return false;

	Vertex ** vertices = new Vertex *[mesh->GetNumVertices()];
	ZeroMemory((void *)vertices, sizeof(Vertex *) * mesh->GetNumVertices());

	if(!mesh->BeginModify(vertices, NULL))
		return false;

	float radius = diameter * 0.5f;
	XMVECTOR outPos;
	for(unsigned int i = 0; i < mesh->GetNumVertices(); i++) {
		outPos = XMLoadFloat3(&vertices[i]->Position);
		outPos = XMVector3Normalize(outPos);

		XMStoreFloat3(&vertices[i]->Normal, outPos);
		outPos *= radius;
		XMStoreFloat3(&vertices[i]->Position, outPos);
	}

	mesh->EndModify();
	mesh->CalculateAndAssignTangents();

	delete[] vertices;

	return true;
}

MeshGenerationType MeshGeneration::StringToGenType(const string & type) {
	MeshGenerationType outType = MeshGenerationType::None;
	mgtParser.Parse(type, outType);
	return outType;
}

CubeMapTextureType MeshGeneration::StringToCubeMapType(const string & type) {
	CubeMapTextureType outType = CubeMapTextureType::None;
	cmttParser.Parse(type, outType);
	return outType;
}