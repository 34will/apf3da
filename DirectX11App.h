#ifndef DIRECTX_11_APP_H
#define DIRECTX_11_APP_H

#define _USE_MATH_DEFINES

#define CameraMoveSpeed 20.0f
#define CameraRotateSpeed 150.0f

#include <map>
#include <windows.h>
#include <d3d11.h>
#include <DirectXMath.h>
#include <math.h>
#include "DXHelperStructs.h"

#include "Input.h"
#include "Mesh.h"
#include "CameraComponent.h"
#include "TransformComponent.h"
#include "RenderComponent.h"
#include "LightComponent.h"
#include "tinyxml2.h"
#include "Pass.h"
#include "Font.h"

using namespace DirectX;
using namespace tinyxml2;

class DirectX11App {
private:
	static DirectX11App * current;

	D3D_DRIVER_TYPE driverType;
	D3D_FEATURE_LEVEL featureLevel;
	ID3D11Device * dX11Device;
	ID3D11DeviceContext * dX11DeviceContext;
	IDXGISwapChain * dX11SwapChain;
	ID3D11RenderTargetView * dX11RenderTargetView;
	ID3D11InputLayout * dX11VertexLayout;
	ID3D11DepthStencilState * dX11DepthStencilState, *dX11DepthStencilDisabledState;
	ID3D11DepthStencilView * dX11DepthStencilView;
	ID3D11RasterizerState * dX11RasterState, *dX11WireframeState;
	ID3D11BlendState * dX11BlendState, *dX11BlendDisabledState, *dX11DeferredRenderingState;
	ID3D11Texture2D * dX11DepthStencilBuffer;

	ID3D11SamplerState * anisoSamplerState;
	CameraComponent * mainCamera;
	TransformComponent * moveTrans, *cameraControl, *cameraTrans;

	Pass dsPassOne, dsPassTwo;
	ID3D11DepthStencilView * deferredShadingDepthStencilView;
	ID3D11Texture2D * deferredShadingDepthStencilBuffer, *deferredShadingTextureArray[4];
	ID3D11RenderTargetView * deferredShadingRTVArray[4];
	ID3D11ShaderResourceView * deferredShadingSRVArray[4], * deferredShadingDepthSRV;
	ID3D11Buffer * deferredShadingFSQVertexBuffer, * deferredShadingFSQIndexBuffer;
	unsigned int numberOfRenderTargets, textureToRender, positionFrom;
	vector<LightComponent *> * lights;

	Pass pass;
	SceneGraph sg;

	Pass fontPass;
	Font f;
	Sentence * s1, *s2;

	unsigned long width, height;
	bool doDeferredRender, drawLines;

	float ClearColour[4];

	HRESULT initSwapChain(HWND hWnd);
	HRESULT initStates(XMLElement * dXParams);
	HRESULT initShaders();
	HRESULT initFont();
	HRESULT initConstantBuffers();

	HRESULT initDeferredShading();
	HRESULT initDeferredShaders();
	HRESULT initDeferredShadingRTs();
	HRESULT initDeferredShadingFSQ();

	void forwardRender();
	void deferredRender();
	void fontRender();

public:
	static DirectX11App * Current();

	DirectX11App();
	~DirectX11App();

	HRESULT InitDevice(HINSTANCE hInst, HWND hWnd, XMLElement * dXParams);
	HRESULT InitScene(XMLElement * sceneParams);

	void CleanupDevice();

	HRESULT ResizeWindow(HWND hWnd);

	void Update(float dt);
	void Render();

	unsigned long GetWidth() const;
	unsigned long GetHeight() const;
	float GetAspectRatio() const;

	ID3D11Device * GetDevice() const;
	ID3D11DeviceContext * GetDeviceContext() const;
};

#endif