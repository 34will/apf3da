#include "Input.h"

bool Input::Initialize(HINSTANCE hInst, HWND hWnd, unsigned int ScreenWidth, unsigned int ScreenHeight) {
	if(hInst == NULL || hWnd == NULL || ScreenWidth == 0 || ScreenHeight == 0)
		return false;

	Shutdown();

	screenWidth = ScreenWidth;
	screenHeight = ScreenHeight;

	HRESULT hr = DirectInput8Create(hInst, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&directInput, NULL);
	if(FAILED(hr))
		return false;

	hr = directInput->CreateDevice(GUID_SysKeyboard, &keyboard, NULL);
	if(FAILED(hr))
		return false;

	hr = keyboard->SetDataFormat(&c_dfDIKeyboard);
	if(FAILED(hr))
		return false;

	hr = keyboard->SetCooperativeLevel(hWnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);
	if(FAILED(hr))
		return false;

	hr = keyboard->Acquire();
	if(FAILED(hr))
		return false;

	hr = directInput->CreateDevice(GUID_SysMouse, &mouse, NULL);
	if(FAILED(hr))
		return false;

	hr = mouse->SetDataFormat(&c_dfDIMouse);
	if(FAILED(hr))
		return false;

	hr = mouse->SetCooperativeLevel(hWnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);
	if(FAILED(hr))
		return false;

	hr = mouse->Acquire();
	if(FAILED(hr))
		return false;

	return true;
}

void Input::Shutdown() {
	if(mouse) {
		mouse->Unacquire();
		mouse->Release();
		mouse = NULL;
	}

	if(keyboard) {
		keyboard->Unacquire();
		keyboard->Release();
		keyboard = NULL;
	}

	if(directInput) {
		directInput->Release();
		directInput = NULL;
	}

	screenWidth = 0;
	screenHeight = 0;
	posX = 0;
	posY = 0;
	deltaX = 0;
	deltaY = 0;

	for(int i = 0; i < NUMKEYS; i++)
		keyboardState[i] = 0;
}

bool Input::ReadKeyboard() {
	HRESULT hr = keyboard->GetDeviceState(sizeof(keyboardState), (LPVOID)&keyboardState);
	if(FAILED(hr)) {
		if((hr == DIERR_INPUTLOST) || (hr == DIERR_NOTACQUIRED)) {
			keyboard->Acquire();
			if(FAILED(hr))
				return false;
		} else
			return false;
	}

	return true;
}

bool Input::ReadMouse() {
	HRESULT hr = mouse->GetDeviceState(sizeof(DIMOUSESTATE), (LPVOID)&mouseState);
	if(FAILED(hr)) {
		if((hr == DIERR_INPUTLOST) || (hr == DIERR_NOTACQUIRED)) {
			hr = mouse->Acquire();
			if(FAILED(hr))
				return false;
		} else
			return false;
	}

	return true;
}

void Input::ProcessInput() {
	deltaX = mouseState.lX;
	deltaY = mouseState.lY;
	deltaZ = mouseState.lZ;

	posX += deltaX;
	posY += deltaY;

	if(posX > screenWidth)
		posX = screenWidth;
	if(posY > screenHeight)
		posY = screenHeight;
}

bool Input::Update() {
	bool result = ReadKeyboard();
	if(!result)
		return false;

	result = ReadMouse();
	if(!result)
		return false;

	ProcessInput();

	return true;
}

bool Input::IsKeyPressed(unsigned char key) {
	return ((keyboardState[key] & 0x80) == 0x80);
}

bool Input::IsMouseButtonPressed(char button) {
	return ((mouseState.rgbButtons[button] & 0x80) == 0x80);
}

IDirectInput8 * Input::directInput = NULL;
IDirectInputDevice8 * Input::keyboard = NULL;
IDirectInputDevice8 * Input::mouse = NULL;

unsigned char Input::keyboardState[NUMKEYS];
DIMOUSESTATE Input::mouseState;

unsigned int Input::screenWidth = 0;
unsigned int Input::screenHeight = 0;

unsigned long Input::posX = 0;
unsigned long Input::posY = 0;

long Input::deltaX = 0;
long Input::deltaY = 0;
long Input::deltaZ = 0;