#ifndef TIMER_H
#define TIMER_H

#include <windows.h>

struct TimeSpan {
	float seconds, milliseconds;
};

class Timer {
	LARGE_INTEGER frequency;
	LARGE_INTEGER start, end, prevTemp; 
	float elapsedTime;

public:
	static float SecondsBetween(const LARGE_INTEGER & a, const LARGE_INTEGER & b, const LARGE_INTEGER & frequency);
	static float MillisecondsBetween(const LARGE_INTEGER & a, const LARGE_INTEGER & b, const LARGE_INTEGER & frequency);

	Timer(bool startTimer = false);
	~Timer();

	void Reset();

	void Start();
	TimeSpan Stop();

	TimeSpan Update();
};

#endif