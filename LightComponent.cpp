#include "LightComponent.h"

RegisterComponentType(LightComponent);

LightComponent::LightComponent() : Component(), transform(NULL), diffuse(0, 0, 0), specular(0, 0, 0), lightType(0), lightRadius(0), lightConeAngleCosine(0), lightSpotDecay(0), enabled(true) { lights.push_back(this); }

LightComponent::~LightComponent() { }

bool LightComponent::Initialise(XMLElement * init) {
	if(init == NULL)
		return false;

	transform = sceneObject->GetComponent<TransformComponent>();

	float r = 0, g = 0, b = 0;
	bool en = true;

	XMLError err = init->QueryFloatAttribute("diffuseR", &r);
	if(err != XML_SUCCESS)
		return false;

	err = init->QueryFloatAttribute("diffuseG", &g);
	if(err != XML_SUCCESS)
		return false;

	err = init->QueryFloatAttribute("diffuseB", &b);
	if(err != XML_SUCCESS)
		return false;

	SetDiffuse(r, g, b);

	err = init->QueryFloatAttribute("specularR", &r);
	if(err != XML_SUCCESS)
		return false;

	err = init->QueryFloatAttribute("specularG", &g);
	if(err != XML_SUCCESS)
		return false;

	err = init->QueryFloatAttribute("specularB", &b);
	if(err != XML_SUCCESS)
		return false;

	SetSpecular(r, g, b);

	err = init->QueryBoolAttribute("enabled", &en);
	if(err == XML_SUCCESS)
		enabled = en;

	string lightType = init->Attribute("type");

	if(lightType == "Point") {
		this->lightType = 1;

		err = init->QueryFloatAttribute("radius", &r);
		if(err != XML_SUCCESS)
			return false;

		lightRadius = r;
	} else if(lightType == "Spot") {
		this->lightType = 2;

		err = init->QueryFloatAttribute("radius", &r);
		if(err != XML_SUCCESS)
			return false;

		err = init->QueryFloatAttribute("coneAngle", &g);
		if(err != XML_SUCCESS)
			return false;

		err = init->QueryFloatAttribute("spotDecay", &b);
		if(err != XML_SUCCESS)
			return false;

		lightRadius = r;
		lightConeAngleCosine = cos(DXHelper::DegreesToRadians(g));
		lightSpotDecay = b;
	}

	return true;
}

void LightComponent::Start() { }

void LightComponent::Shutdown() { }

string LightComponent::GetTypename() const {
	return "LightComponent";
}

void LightComponent::SetDiffuse(XMFLOAT3 diffuse) {
	this->diffuse = diffuse;
}

void LightComponent::SetDiffuse(float diffuseR, float diffuseG, float diffuseB) {
	diffuse = XMFLOAT3(diffuseR, diffuseG, diffuseB);
}

XMFLOAT3 LightComponent::GetDiffuse() const {
	return diffuse;
}

XMFLOAT3 * LightComponent::GetDiffusePtr() {
	return &diffuse;
}

void LightComponent::SetSpecular(XMFLOAT3 specular) {
	this->specular = specular;
}

void LightComponent::SetSpecular(float specularR, float specularG, float specularB) {
	specular = XMFLOAT3(specularR, specularG, specularB);
}

void LightComponent::SetEnabled(bool value) {
	enabled = value;
}

XMFLOAT3 LightComponent::GetSpecular() const {
	return specular;
}

XMFLOAT3 * LightComponent::GetSpecularPtr() {
	return &specular;
}

XMFLOAT3 LightComponent::GetPosition() {
	return transform->GetWorldPosition();
}

XMFLOAT3 * LightComponent::GetPositionPtr() {
	return transform->GetWorldPositionPtr();
}

XMFLOAT3 LightComponent::GetDirection() {
	return transform->GetForward();
}

XMFLOAT3 * LightComponent::GetDirectionPtr() {
	return transform->GetForwardPtr();
}

int LightComponent::GetLightType() {
	return lightType;
}
int * LightComponent::GetLightTypePtr() {
	return &lightType;
}

float LightComponent::GetLightRadius() {
	return lightRadius;
}

float * LightComponent::GetLightRadiusPtr() {
	return &lightRadius;
}

float LightComponent::GetLightConeAngleCosine() {
	return lightConeAngleCosine;
}

float * LightComponent::GetLightConeAngleCosinePtr() {
	return &lightConeAngleCosine;
}

float LightComponent::GetLightSpotDecay() {
	return lightSpotDecay;
}

float * LightComponent::GetLightSpotDecayPtr() {
	return &lightSpotDecay;
}

bool LightComponent::GetEnabled() {
	return enabled;
}

vector<LightComponent *> LightComponent::lights = vector<LightComponent *>();

vector<LightComponent *> * LightComponent::GetLights() {
	return &lights;
}