#ifndef INPUT_H
#define INPUT_H

#define DIRECTINPUT_VERSION 0x0800
#define NUMKEYS 256

#include <dinput.h>

class Input {
private:
	static IDirectInput8 * directInput;
	static IDirectInputDevice8 * keyboard;
	static IDirectInputDevice8 * mouse;

	static unsigned char keyboardState[NUMKEYS];
	static DIMOUSESTATE mouseState;

	static unsigned int screenWidth, screenHeight;
	static unsigned long posX, posY;
	static long deltaX, deltaY, deltaZ;

	static bool ReadKeyboard();
	static bool ReadMouse();
	static void ProcessInput();

public:
	static bool Initialize(HINSTANCE hInst, HWND hWnd, unsigned int ScreenWidth, unsigned int ScreenHeight);
	static void Shutdown();

	static bool Update();
	static bool IsKeyPressed(unsigned char key);
	static bool IsMouseButtonPressed(char button);

	static long MousePosX() { return posX; }
	static long MousePosY() { return posY; }
	static long MouseDeltaX() { return deltaX; }
	static long MouseDeltaY() { return deltaY; }
	static long MouseDWheel() { return deltaZ; }
};

#endif