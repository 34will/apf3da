#include "Shader.h"
#include <fstream>
#include <vector>

const Parser<tuple<ShaderVariableType, int>> CBufferStruct::typesAndSizes = {
		{
			{ "uniform bool", tuple<ShaderVariableType, int>(Bool, 4) },
			{ "float", tuple<ShaderVariableType, int>(Float, 4) },
			{ "float4", tuple<ShaderVariableType, int>(Float4, 16) },
			{ "float4x2", tuple<ShaderVariableType, int>(Float4x2, 32) },
			{ "float4x3", tuple<ShaderVariableType, int>(Float4x3, 48) },
			{ "float4x4", tuple<ShaderVariableType, int>(Float4x4, 64) },
			{ "float3", tuple<ShaderVariableType, int>(Float3, 12) },
			{ "float3x2", tuple<ShaderVariableType, int>(Float3x2, 24) },
			{ "float3x3", tuple<ShaderVariableType, int>(Float3x3, 36) },
			{ "float3x4", tuple<ShaderVariableType, int>(Float3x4, 48) },
			{ "float2", tuple<ShaderVariableType, int>(Float2, 8) },
			{ "float2x2", tuple<ShaderVariableType, int>(Float2x2, 16) },
			{ "float2x3", tuple<ShaderVariableType, int>(Float2x3, 24) },
			{ "float2x4", tuple<ShaderVariableType, int>(Float2x4, 32) },
			{ "int", tuple<ShaderVariableType, int>(Int, 4) },
			{ "int4", tuple<ShaderVariableType, int>(Int4, 16) },
			{ "int4x2", tuple<ShaderVariableType, int>(Int4x2, 32) },
			{ "int4x3", tuple<ShaderVariableType, int>(Int4x3, 48) },
			{ "int4x4", tuple<ShaderVariableType, int>(Int4x4, 64) },
			{ "int3", tuple<ShaderVariableType, int>(Int3, 12) },
			{ "int3x2", tuple<ShaderVariableType, int>(Int3x2, 24) },
			{ "int3x3", tuple<ShaderVariableType, int>(Int3x3, 36) },
			{ "int3x4", tuple<ShaderVariableType, int>(Int3x4, 48) },
			{ "int2", tuple<ShaderVariableType, int>(Int2, 8) },
			{ "int2x2", tuple<ShaderVariableType, int>(Int2x2, 16) },
			{ "int2x3", tuple<ShaderVariableType, int>(Int2x3, 24) },
			{ "int2x4", tuple<ShaderVariableType, int>(Int2x4, 32) },
			{ "uint", tuple<ShaderVariableType, int>(Uint, 4) },
			{ "uint4", tuple<ShaderVariableType, int>(Uint4, 16) },
			{ "uint4x2", tuple<ShaderVariableType, int>(Uint4x2, 32) },
			{ "uint4x3", tuple<ShaderVariableType, int>(Uint4x3, 48) },
			{ "uint4x4", tuple<ShaderVariableType, int>(Uint4x4, 64) },
			{ "uint3", tuple<ShaderVariableType, int>(Uint3, 12) },
			{ "uint3x2", tuple<ShaderVariableType, int>(Uint3x2, 24) },
			{ "uint3x3", tuple<ShaderVariableType, int>(Uint3x3, 36) },
			{ "uint3x4", tuple<ShaderVariableType, int>(Uint3x4, 48) },
			{ "uint2", tuple<ShaderVariableType, int>(Uint2, 8) },
			{ "uint2x2", tuple<ShaderVariableType, int>(Uint2x2, 16) },
			{ "uint2x3", tuple<ShaderVariableType, int>(Uint2x3, 24) },
			{ "uint2x4", tuple<ShaderVariableType, int>(Uint2x4, 32) }
		}
};

ID3D11Buffer * CBufferStruct::nullBuffer[1] = { NULL };

CBufferStruct::CBufferStruct() : name(""), bufferNum(0), totalSize(0), variablesOffset(), variables(NULL), modified(true), bufferBuilt(false) { }
CBufferStruct::CBufferStruct(string name, unsigned int bufferNum) : name(name), bufferNum(bufferNum), totalSize(0), variablesOffset(), variables(NULL), modified(true), bufferBuilt(false) { }

void CBufferStruct::addVariable(string name, string type, int arraySize) {
	if(!bufferBuilt) {
		tuple<ShaderVariableType, int> out;
		if(typesAndSizes.Parse(type, out)) {
			unsigned int typeSize = get<1>(out);
			variablesOffset[name] = tuple<unsigned int, unsigned int>(totalSize, typeSize * arraySize);
			totalSize += (typeSize * arraySize);
		}
	}
}

HRESULT CBufferStruct::buildBuffer(ID3D11Device * dX11Device) {
	variables = new char[totalSize];
	ZeroMemory(variables, totalSize);

	D3D11_BUFFER_DESC cbDesc;
	D3D11_SUBRESOURCE_DATA initData;

	ZeroMemory(&cbDesc, sizeof(cbDesc));
	ZeroMemory(&initData, sizeof(initData));

	cbDesc.ByteWidth = totalSize;
	cbDesc.Usage = D3D11_USAGE_DEFAULT;
	cbDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cbDesc.CPUAccessFlags = 0;
	cbDesc.MiscFlags = 0;
	cbDesc.StructureByteStride = 0;

	initData.pSysMem = variables;
	initData.SysMemPitch = 0;
	initData.SysMemSlicePitch = 0;

	HRESULT hr = dX11Device->CreateBuffer(&cbDesc, &initData, &buffer);

	bufferBuilt = !FAILED(hr);

	return hr;
}

void CBufferStruct::Update(ID3D11DeviceContext * dX11DeviceContext) {
	if(bufferBuilt && modified) {
		modified = false;
		dX11DeviceContext->UpdateSubresource(buffer, 0, NULL, variables, 0, 0);

		dataSet = true;
	}
}

bool CBufferStruct::setVariable(string name, const void * object) {
	if(bufferBuilt) {
		unordered_map<string, tuple<unsigned int, unsigned int>>::const_iterator found = variablesOffset.find(name);
		if(found == variablesOffset.end())
			return false;
		else {
			modified = true;
			memcpy(&variables[get<0>(found->second)], object, get<1>(found->second));
			return true;
		}
	} else
		return false;
}

void CBufferStruct::Shutdown() {
	if(variables != NULL) {
		delete variables;
		variables = NULL;
	}

	totalSize = 0;
	modified = false;
	bufferBuilt = false;
}

Shader::Shader(WCHAR * file, LPCSTR entryPoint, LPCSTR version) : file(file), entryPoint(entryPoint), version(version) { }

HRESULT Shader::GenerateConstantBuffers(ID3D11Device * dX11Device) {
	wstring filename = wstring(file);
	if(!filename.empty() && dX11Device != NULL) {
		vector<string> lines = vector<string>();
		string line = "";
		ifstream shaderFile(filename);

		if(shaderFile.is_open()) {
			bool inCBuffer = false;
			string id = "";

			while(shaderFile.good() && !shaderFile.eof()) {
				getline(shaderFile, line);

				if(line.length() >= 2 && line[0] != '/' && line[1] != '*' && line[1] != '/') {
					lines.push_back(line);

					if(!inCBuffer) {
						if(line.find("cbuffer") != string::npos) {
							smatch matches;
							if(regex_search(line, matches, constantBufferRegex)) {
								id = matches[1];
								constantBufferMap[id] = CBufferStruct(id, (unsigned int)stoul(matches[2]));
								inCBuffer = true;
							}
						} else if(line.find("Texture2D ") != string::npos) {
							smatch matches;
							if(regex_search(line, matches, textureRegex)) {
								textures.push_back(matches[1]);
								textureMap[matches[1]] = { (unsigned int)stoul(matches[2]), NULL };
							}
						}
					} else {
						smatch matches;
						if(regex_search(line, matches, variableRegex)) {
							string name = (matches[2].matched ? matches[2] : matches[4]);
							if(name.length() > 0 && name[0] != '_')
								variables.push_back(name);

							cbVariableMap[name] = id;
							constantBufferMap[id].addVariable(name, matches[1], (matches[3].matched ? stoi(matches[3]) : 1));
						} else if(line.find("};") != string::npos)
							inCBuffer = false;
					}
				}
			}
		} else
			return E_FAIL;

		for(auto& x : constantBufferMap) {
			HRESULT hr = x.second.buildBuffer(dX11Device);

			if(FAILED(hr))
				return hr;
		}

		return S_OK;
	} else
		return E_FAIL;
}

Shader::~Shader() {
	for(auto& x : constantBufferMap)
		x.second.Shutdown();
}

void Shader::Deactivate(ID3D11DeviceContext * dX11DeviceContext) {
	for(auto& x : constantBufferMap) {
		if(x.second.bufferBuilt)
			SetConstantBuffers(dX11DeviceContext, x.second.bufferNum, 1, CBufferStruct::nullBuffer);
	}
}

void Shader::ActivateMaterial(const Material * material) {
	if(material != NULL) {
		for(auto x : textures)
			SetTexture(x, material->GetTexturePtr(x));

		for(auto x : variables)
			SetVariable(x, material->GetVariable(x));
	}
}

void Shader::SetShaderData(ID3D11DeviceContext * dX11DeviceContext) {
	for(auto& x : constantBufferMap) {
		if(x.second.bufferBuilt) {
			x.second.Update(dX11DeviceContext);
			SetConstantBuffers(dX11DeviceContext, x.second.bufferNum, 1, &(x.second.buffer));
		}
	}

	for(auto& x : textureMap) {
		if(x.second.srvs != NULL)
			SetTextures(dX11DeviceContext, x.second.slot, 1, x.second.srvs);
	}
}

bool Shader::SetVariable(const string & variable, const void * object) {
	unordered_map<string, string>::iterator found = cbVariableMap.find(variable);
	return (found == cbVariableMap.end() ? false : constantBufferMap[found->second].setVariable(variable, object));
}

vector<string> Shader::GetVariableNames() const {
	return variables;
}

bool Shader::SetTexture(const string & name, ID3D11ShaderResourceView * const * object) {
	unordered_map<string, TextureSlot>::iterator found = textureMap.find(name);
	if(found == textureMap.end())
		return false;
	else {
		found->second.srvs = object;
		return true;
	}
}

vector<string> Shader::GetTextureNames() const {
	return textures;
}

HRESULT Shader::CompileShaderFromFile(LPCWSTR szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut) {
	HRESULT hr = S_OK;

	DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined( DEBUG ) || defined( _DEBUG )
	dwShaderFlags |= D3DCOMPILE_DEBUG;
#endif

	ID3DBlob* pErrorBlob;
	hr = D3DCompileFromFile(szFileName, NULL, NULL, szEntryPoint, szShaderModel, dwShaderFlags, 0, ppBlobOut, &pErrorBlob);

	if(FAILED(hr)) {
		if(pErrorBlob != NULL)
			OutputDebugStringA((char*)pErrorBlob->GetBufferPointer());
		if(pErrorBlob)
			pErrorBlob->Release();

		return hr;
	}

	if(pErrorBlob)
		pErrorBlob->Release();

	return S_OK;
}

const regex Shader::constantBufferRegex = regex("cbuffer (.*)\\s+:\\s*register\\(b(\\d*)\\)");
const regex Shader::textureRegex = regex("Texture2D (.*)\\s+:\\s*register\\(t(\\d*)\\)");
const regex Shader::variableRegex = regex("(float.{0,3}|unit|int|uniform bool)\\s+(?:(.*)\\[(\\d*)\\]|(.*));");