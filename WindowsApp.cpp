#include "WindowsApp.h"
#include <sstream>

LRESULT CALLBACK WindowsApp::StaticWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	WindowsApp * pParent;   

	if(message == WM_CREATE) {      
		pParent = (WindowsApp *)((LPCREATESTRUCT)lParam)->lpCreateParams;      
		SetWindowLongPtr(hWnd, GWL_USERDATA, (LONG_PTR)pParent);   
	} else {      
		pParent = (WindowsApp *)GetWindowLongPtr(hWnd, GWL_USERDATA);      
		if(!pParent) 
			return DefWindowProc(hWnd, message, wParam, lParam);   
	}   
	
	pParent->hWnd = hWnd;   
	
	return pParent->WndProc(message, wParam, lParam);
}

WindowsApp::WindowsApp(): hInst(NULL), hWnd(NULL) { }

WindowsApp::~WindowsApp() { }

HRESULT WindowsApp::InitWindow(HINSTANCE hInstance, int nCmdShow, tinyxml2::XMLElement * windowParams) {
	if(windowParams == NULL)
        return E_FAIL;

	WNDCLASSEX wcex;
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW|CS_VREDRAW;
	wcex.lpfnWndProc = StaticWndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, (LPCTSTR)IDI_TUTORIAL1);
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = L"WindowApp";
	wcex.hIconSm = LoadIcon(wcex.hInstance, (LPCTSTR)IDI_TUTORIAL1);

	if(!RegisterClassEx(&wcex))
		return E_FAIL;

    long width = 0, height = 0;
    string line = windowParams->FirstChildElement("width")->GetText();
    if(line.empty() || !(stringstream(line) >> width))
        return E_FAIL;

    line = windowParams->FirstChildElement("height")->GetText();
    if(line.empty() || !(stringstream(line) >> height))
        return E_FAIL;

    line = windowParams->FirstChildElement("title")->GetText();
    if(line.empty())
        return E_FAIL;

	hInst = hInstance;
	RECT rc = {0, 0, width, height};
	AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, FALSE);
	hWnd = CreateWindowA("WindowApp", line.c_str(), WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, rc.right - rc.left, rc.bottom - rc.top, NULL, NULL, hInstance, this);

	if(!hWnd)
		return E_FAIL;

	ShowWindow(hWnd, nCmdShow);
	SetForegroundWindow(hWnd);
	SetFocus(hWnd);

	return S_OK;
}

LRESULT WindowsApp::WndProc(UINT message, WPARAM wParam, LPARAM lParam) {
	PAINTSTRUCT ps;
	HDC hdc;

	switch(message) {
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		EndPaint(hWnd, &ps);
		break;
    case WM_SIZE:
        resize(hWnd);
        break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;
}