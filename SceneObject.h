#ifndef SCENEOBJECT_H
#define SCENEOBJECT_H

#include <unordered_map>
#include <vector>
#include <d3d11.h>
#include "tinyxml2.h"
#include "Pass.h"

using namespace std;
using namespace tinyxml2;

class Component;
class CameraComponent;

class SceneObject {
private:
	unordered_map<string, vector<Component *>> components;
	vector<Component *> componentsIterator;
	string name;

public:
	SceneObject(string name);
	~SceneObject();

	void AddComponent(Component * c);

	void Initialise(XMLNode * node);

	void Start();
	void Shutdown();

	void Update(float dt);
	void Draw(ID3D11DeviceContext * dX11DeviceContext, Pass * pass, CameraComponent * currentCam);

#pragma warning(push)
#pragma warning(disable : 4127)
	template<class T>
	T * GetComponent() {
		if(is_base_of<Component, T>::value == true) {
			string typeName = typeof<T>::name();
			unordered_map<string, vector<Component *>>::iterator found = components.find(typeName);

			return (found == components.end() ? NULL : static_cast<T *>(found->second[0]));
		} else
			return NULL;
	}

	template<class T>
	bool GetComponents(vector<T *> & out) {
		if(is_base_of<Component, T>::value == true) {
			string typeName = typeof<T>::name();
			unordered_map<string, vector<Component *>>::iterator found = components.find(typeName);

			if(found == components.end())
				return false;
			else {
				for(auto c : found->second)
					out.push_back(static_cast<T *>(c));

				return true;
			}
		} else
			return false;
	}
#pragma warning(pop)
};

#endif