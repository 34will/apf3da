#ifndef FONT_H
#define FONT_H

#include <unordered_map>
#include "Mesh.h"
#include "Material.h"
#include "Pass.h"

class DirectX11App;
class Sentence;

struct FontCharacter {
	char character;
	XMFLOAT2 texCoords[4];
};

struct FontVertex {
	XMFLOAT2 position, texCoord;
};

typedef unordered_map<char, FontCharacter> CharacterMap;

class Font {
	friend class DirectX11App;
	friend class Sentence;

private:
	static unsigned int screenWidth, screenHeight;

	unsigned int numChars, charWidth, charHeight, mapWidth, mapHeight;
	ID3D11ShaderResourceView * fontMap;
	CharacterMap cMap;

	void buildVertexArray(FontVertex * vertices, string sentence, XMFLOAT2 position, float spacing, float scale = 1.0f);
	void draw(ID3D11DeviceContext * dX11DeviceContext, Pass * pass, unsigned int indexCount);

public:
	Font();
	~Font();

	HRESULT Load(ID3D11Device * dX11Device, ID3D11DeviceContext * dX11DeviceContext, string fileName);
	void CleanUp();
};

class Sentence {
private:
	XMFLOAT2 position;
	XMFLOAT4 colour;
	string sentence;
	FontVertex * vertices;
	unsigned int * indicies, vertexCount;
	ID3D11Buffer * vertexBuffer, *indexBuffer;
	bool changed;
	Font & font;
	float scale, spacing;

public:
	Sentence(string sentence, XMFLOAT2 screenPosition, float scale, Font & font, float spacing = 0.0f);
	~Sentence();
	Sentence& operator=(const Sentence &rhs);

	void CleanUp();

	HRESULT Build(ID3D11Device * dX11Device);

	void SetSentence(string sentence);
	void SetPosition(XMFLOAT2 position);

	void Draw(ID3D11DeviceContext * dX11DeviceContext, Pass * pass);
};

#endif