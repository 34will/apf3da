#include "Mesh.h"
#include "DirectX11App.h"

Mesh::Mesh() : vertices(NULL), indices(NULL), numVerts(0), numInds(0), numSubObjs(0), vertexBuffer(NULL), indexBuffer(NULL), subObjects(NULL), materials(NULL), modifyingVertices(false), modifyingIndices(false) {
	bb.maxX = FLT_MIN;
	bb.maxY = FLT_MIN;
	bb.maxZ = FLT_MIN;
	bb.minX = FLT_MAX;
	bb.minY = FLT_MAX;
	bb.minZ = FLT_MAX;
}

HRESULT Mesh::BuildBuffers(ID3D11Device * dX11Device) {
	if(vertices == NULL || indices == NULL)
		return E_FAIL;

	if(vertexBuffer) {
		vertexBuffer->Release();
		vertexBuffer = NULL;
	}

	if(indexBuffer) {
		indexBuffer->Release();
		indexBuffer = NULL;
	}

	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.ByteWidth = sizeof(Vertex) * numVerts;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = vertices;
	HRESULT hr = dX11Device->CreateBuffer(&bd, &InitData, &vertexBuffer);

	if(FAILED(hr))
		return hr;

	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.ByteWidth = sizeof(unsigned int) * numInds;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = indices;
	hr = dX11Device->CreateBuffer(&bd, &InitData, &indexBuffer);

	if(FAILED(hr))
		return hr;

	return S_OK;
}

void Mesh::CleanUp() {
	if(subObjects) {
		delete[] subObjects;
		subObjects = NULL;
	}
	if(materials) {
		delete[] materials;
		materials = NULL;
	}
	if(vertexBuffer) {
		vertexBuffer->Release();
		vertexBuffer = NULL;
	}
	if(indexBuffer) {
		indexBuffer->Release();
		indexBuffer = NULL;
	}
	if(vertices) {
		delete[] vertices;
		vertices = NULL;
	}
	if(indices) {
		delete[] indices;
		indices = NULL;
	}
}

void Mesh::Draw(ID3D11DeviceContext * dX11DeviceContext, Pass * pass) {
	unsigned int stride = sizeof(Vertex), offset = 0;
	dX11DeviceContext->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
	dX11DeviceContext->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	for(unsigned int i = 0; i < numSubObjs; i++) {
		pass->GetPixelShader()->ActivateMaterial(subObjects[i].material);

		pass->PreDraw(dX11DeviceContext);
		dX11DeviceContext->DrawIndexed(subObjects[i].vertexCount, subObjects[i].vertexStart, 0);
	}
}

BoundingBox Mesh::GetBoundingBox() {
	return bb;
}

BoundingBox Mesh::GetAABoundingBox(XMFLOAT4X4 const & world) {
	XMFLOAT3 minPoint = XMFLOAT3(bb.minX, bb.minY, bb.minZ), maxPoint = XMFLOAT3(bb.maxX, bb.maxY, bb.maxZ), tempCentre = DXHelper::Multiply(DXHelper::Add(minPoint, maxPoint), 0.5f), tempExtents = DXHelper::Subtract(tempCentre, minPoint);
	XMFLOAT4 centre = XMFLOAT4(tempCentre.x, tempCentre.y, tempCentre.z, 1.0f), extents = XMFLOAT4(tempExtents.x, tempExtents.y, tempExtents.z, 0), newCentre;
	XMFLOAT4X4 worldT;

	XMMATRIX outWorld = XMLoadFloat4x4(&world), outTWorld = XMMatrixTranspose(outWorld);
	XMVECTOR outCentre = XMLoadFloat4(&centre), outNewCentre = XMVector3Transform(outCentre, outWorld);
	XMStoreFloat4(&newCentre, outNewCentre);
	XMStoreFloat4x4(&worldT, outTWorld);

	BoundingBox aabb;
	XMFLOAT3 newExtents = XMFLOAT3(DXHelper::AbsoluteDot(XMFLOAT4(worldT.m[0]), extents), DXHelper::AbsoluteDot(XMFLOAT4(worldT.m[1]), extents), DXHelper::AbsoluteDot(XMFLOAT4(worldT.m[2]), extents));
	aabb.minX = -newExtents.x;
	aabb.minY = -newExtents.y;
	aabb.minZ = -newExtents.z;
	aabb.maxX = newExtents.x;
	aabb.maxY = newExtents.y;
	aabb.maxZ = newExtents.z;

	return aabb;
}

Material const * const Mesh::GetMaterials() const {
	return materials;
}

unsigned int Mesh::GetNumMaterials() const {
	return numMats;
}

Vertex const * const Mesh::GetVertices() const {
	return vertices;
}

unsigned int Mesh::GetNumVertices() const {
	return numVerts;
}

unsigned int const * const Mesh::GetIndices() const {
	return indices;
}

unsigned int Mesh::GetNumIndices() const {
	return numInds;
}

void Mesh::Create(Vertex const * const vertices, unsigned int numVerts, unsigned int const * const indices, unsigned int numInds, Material const * const materials, unsigned int numMats) {
	if(vertices != NULL && numVerts != 0 && indices != NULL && numInds != 0 && materials != NULL && numMats != 0) {
		CleanUp();

		this->numVerts = numVerts;
		this->vertices = new Vertex[numVerts];
		memcpy(this->vertices, vertices, sizeof(Vertex) * numVerts);

		this->numInds = numInds;
		this->indices = new unsigned int[numInds];
		memcpy(this->indices, indices, sizeof(unsigned int) * numInds);

		this->numMats = numMats;
		this->materials = new Material[numMats];
		memcpy(this->materials, materials, sizeof(Material) * numMats);

		if(tangentCalc != NULL) {
			delete[] tangentCalc;
			tangentCalc = new XMFLOAT3[numVerts * 2];
		}

		CalculateAndAssignTangents();

		numSubObjs = 1;
		subObjects = new SubObject[1];
		subObjects[0].vertexCount = numInds;
		subObjects[0].vertexStart = 0;
		subObjects[0].matName = materials[0].GetName();
		subObjects[0].material = &(this->materials[0]);
	}
}

bool Mesh::BeginModify(Vertex * * const vertices, unsigned int * * const indices) {
	if(vertices != NULL || indices != NULL) {
		if(vertices != NULL) {
			for(unsigned int i = 0; i < numVerts; i++)
				vertices[i] = &this->vertices[i];

			modifyingVertices = true;
			ZeroMemory(&mappedVResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
			if(FAILED(DirectX11App::Current()->GetDeviceContext()->Map(vertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedVResource))) {
				modifyingVertices = false;
				return false;
			}
		}

		if(indices != NULL) {
			for(unsigned int i = 0; i < numInds; i++)
				indices[i] = &this->indices[i];

			modifyingIndices = true;
			ZeroMemory(&mappedIResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
			if(FAILED(DirectX11App::Current()->GetDeviceContext()->Map(indexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedIResource))) {
				modifyingIndices = false;
				return false;
			}
		}
	}
	return true;
}

void Mesh::EndModify() {
	if(modifyingVertices) {
		memcpy(mappedVResource.pData, vertices, sizeof(Vertex) * numVerts);
		DirectX11App::Current()->GetDeviceContext()->Unmap(vertexBuffer, 0);
		modifyingVertices = false;
	}

	if(modifyingIndices) {
		memcpy(mappedIResource.pData, indices, sizeof(unsigned int) * numInds);
		DirectX11App::Current()->GetDeviceContext()->Unmap(indexBuffer, 0);
		modifyingIndices = false;
	}
}

void Mesh::CalculateAndAssignNormals() {
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	ZeroMemory(&mappedResource, sizeof(D3D11_MAPPED_SUBRESOURCE));

	if(vertexBuffer != NULL) {
		if(FAILED(DirectX11App::Current()->GetDeviceContext()->Map(vertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource)))
			return;
	}

	for(unsigned int a = 0; a < numVerts; a++)
		vertices[a].Normal = XMFLOAT3(0.0f, 0.0f, 0.0f);

	for(unsigned int a = 0; a < numInds; a += 3) {
		unsigned int i1 = indices[a];
		unsigned int i2 = indices[a + 1];
		unsigned int i3 = indices[a + 2];

		XMFLOAT3 e1 = DXHelper::Subtract(vertices[i1].Position, vertices[i2].Position);
		XMFLOAT3 e2 = DXHelper::Subtract(vertices[i3].Position, vertices[i2].Position);
		XMFLOAT3 cross = DXHelper::Cross(e1, e2);

		vertices[i1].Normal = DXHelper::Add(vertices[i1].Normal, cross);
		vertices[i2].Normal = DXHelper::Add(vertices[i2].Normal, cross);
		vertices[i3].Normal = DXHelper::Add(vertices[i3].Normal, cross);
	}

	for(unsigned int a = 0; a < numVerts; a++) {
		XMVECTOR normal = XMLoadFloat3(&vertices[a].Normal);
		normal = XMVector3Normalize(normal);
		XMStoreFloat3(&vertices[a].Normal, normal);
	}

	if(vertexBuffer != NULL) {
		memcpy(mappedResource.pData, vertices, sizeof(Vertex) * numVerts);
		DirectX11App::Current()->GetDeviceContext()->Unmap(vertexBuffer, 0);
	}
}

void Mesh::CalculateAndAssignTangents() {
	if(numVerts == 0 || numInds == 0)
		return;

	if(tangentCalc == NULL) 
		tangentCalc = new XMFLOAT3[numVerts * 2];
	ZeroMemory(tangentCalc, numVerts * sizeof(XMFLOAT3) * 2);

	for(unsigned int a = 0; a < numInds; a += 3) {
		unsigned int i1 = indices[a];
		unsigned int i2 = indices[a + 1];
		unsigned int i3 = indices[a + 2];

		const XMFLOAT3 & v1 = vertices[i1].Position;
		const XMFLOAT3 & v2 = vertices[i2].Position;
		const XMFLOAT3 & v3 = vertices[i3].Position;

		const XMFLOAT2 & w1 = vertices[i1].TexCoord;
		const XMFLOAT2 & w2 = vertices[i2].TexCoord;
		const XMFLOAT2 & w3 = vertices[i3].TexCoord;

		float x1 = v2.x - v1.x;
		float x2 = v3.x - v1.x;
		float y1 = v2.y - v1.y;
		float y2 = v3.y - v1.y;
		float z1 = v2.z - v1.z;
		float z2 = v3.z - v1.z;

		float s1 = w2.x - w1.x;
		float s2 = w3.x - w1.x;
		float t1 = w2.y - w1.y;
		float t2 = w3.y - w1.y;

		float r = 1.0f / (s1 * t2 - s2 * t1);
		XMFLOAT3 sdir((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r, (t2 * z1 - t1 * z2) * r);
		XMFLOAT3 tdir((s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r, (s1 * z2 - s2 * z1) * r);

		tangentCalc[i1] = { tangentCalc[i1].x + sdir.x, tangentCalc[i1].y + sdir.y, tangentCalc[i1].z + sdir.z };
		tangentCalc[i2] = { tangentCalc[i2].x + sdir.x, tangentCalc[i2].y + sdir.y, tangentCalc[i2].z + sdir.z };
		tangentCalc[i3] = { tangentCalc[i3].x + sdir.x, tangentCalc[i3].y + sdir.y, tangentCalc[i3].z + sdir.z };
	}

	D3D11_MAPPED_SUBRESOURCE mappedResource;
	ZeroMemory(&mappedResource, sizeof(D3D11_MAPPED_SUBRESOURCE));

	if(vertexBuffer != NULL) {
		if(FAILED(DirectX11App::Current()->GetDeviceContext()->Map(vertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource)))
			return;
	}

	for(unsigned int a = 0; a < numVerts; a++) {
		XMVECTOR tOut = XMLoadFloat3(&tangentCalc[a]), nOut = XMLoadFloat3(&vertices[a].Normal), calc;
		calc = tOut - nOut * XMVector3Dot(nOut, tOut);
		calc = XMVector3Normalize(calc);

		XMStoreFloat3(&vertices[a].Tangent, calc);
	}

	if(vertexBuffer != NULL) {
		memcpy(mappedResource.pData, vertices, sizeof(Vertex) * numVerts);
		DirectX11App::Current()->GetDeviceContext()->Unmap(vertexBuffer, 0);
	}
}