#ifndef CAMERA_COMPONENT_H
#define CAMERA_COMPONENT_H

#include <DirectXMath.h>
#include "Component.h"
#include "TransformComponent.h"
#include "ComponentRegistrar.h"

using namespace DirectX;
using namespace tinyxml2;

class CameraComponent : public Component {
private:
	TransformComponent * transform;
	XMFLOAT4X4 view, projection, inverseProjection, inverseView, ortho, inverseViewProjection;
	XMFLOAT4 viewPlanes[6];
	XMFLOAT3 up;
	float fov, aspect, nearPlane, farPlane;

	CameraComponent();

	void updateView();
	void updateProjection();
	void updateViewProjectionInverse();

public:
	~CameraComponent();

	bool Initialise(XMLElement * init);

	void Start();
	void Shutdown();

	string GetTypename() const;

	XMFLOAT3 GetAt() const;
	XMFLOAT3 * GetAtPtr();
	XMFLOAT3 GetEye() const;
	XMFLOAT3 * GetEyePtr();
	XMFLOAT3 GetUp() const;
	XMFLOAT3 * GetUpPtr();

	XMFLOAT4X4 GetProjection();
	XMFLOAT4X4 * GetProjectionPtr();
	XMFLOAT4X4 GetInverseProjection();
	XMFLOAT4X4 * GetInverseProjectionPtr();
	XMFLOAT4X4 GetInverseView();
	XMFLOAT4X4 * GetInverseViewPtr();
	XMFLOAT4X4 GetInverseViewProjection();
	XMFLOAT4X4 * GetInverseViewProjectionPtr();
	XMFLOAT4X4 GetOrthographic();
	XMFLOAT4X4 * GetOrthographicPtr();
	XMFLOAT4X4 GetView();
	XMFLOAT4X4 * GetViewPtr();
	XMFLOAT4X4 GetWorld() const;
	XMFLOAT4X4 * GetWorldPtr();

	const XMFLOAT4 GetNearViewFrustumCorner(unsigned int const & index) const;
	const XMFLOAT4 * GetNearViewFrustumCornerPtr(unsigned int const & index) const;
	const XMFLOAT4 GetFarViewFrustumCorner(unsigned int const & index) const;
	const XMFLOAT4 * GetFarViewFrustumCornerPtr(unsigned int const & index) const;

	const XMFLOAT4 * GetNearViewFrustumCorners() const;
	const XMFLOAT4 * GetFarViewFrustumCorners() const;

	void SetUp(XMFLOAT3 up);
	void SetUp(float upX, float upY, float upZ);
	void SetProjection(float fov, float aspect, float nearPlane, float farPlane);

	void SetFieldOfView(float fov);
	void SetAspectRatio(float aspect);
	void SetNearPlane(float nearPlane);
	void SetFarPlane(float farPlane);

	bool PointInViewFrustrum(XMFLOAT3 p);
	bool PointInViewFrustrum(float x, float y, float z);

	bool SphereInViewFrustrum(XMFLOAT3 p, float radius);
	bool SphereInViewFrustrum(float x, float y, float z, float radius);

	bool BoundingBoxInViewFrustrum(XMFLOAT3 p, BoundingBox bb);

	SetupComponentType(CameraComponent);
};

DeclareType(CameraComponent);

#endif