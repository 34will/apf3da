Texture2D Diffuse	: register(t0);
Texture2D Normal	: register(t1);
Texture2D Specular	: register(t2);

SamplerState TextureSampler : register(s0);

cbuffer DSPSMaterialBuffer : register(b0) {
	float4 MaterialAmbient;
	float4 MaterialDiffuse;
	float4 MaterialSpecular;
	float MaterialSpecularExponent;
	uniform bool IsTextured;
	uniform bool HasAlpha;
	float _b0Buffer;
};

struct VSOutput {
	float4		Position	: SV_POSITION;
	float4		WVPosition	: WV_POSITION;
	float2		Texel		: TEXCOORD0;
	float3x3	TBN			: TBNMATRIX;
};

struct PSOutput {
	float4	Diffuse			: SV_Target0;
	float4	Normal			: SV_Target1;
	float4	Position		: SV_Target2;
	float4	DepthSpec		: SV_Target3;
};

PSOutput main(VSOutput input) {
	PSOutput output = (PSOutput)0;

	if(IsTextured)
		output.Diffuse = Diffuse.Sample(TextureSampler, input.Texel);
	else
		output.Diffuse = MaterialDiffuse;

	if(IsTextured) {
		float3 normSample = Normal.Sample(TextureSampler, input.Texel).xyz;
		normSample = (normSample * 2.0f) - 1.0f;

		float3 bumpNormal = normalize(mul(normSample, input.TBN));
		output.Normal = float4((bumpNormal + 1.0f) * 0.5f, 1.0f);
	} else
		output.Normal = float4((input.TBN[2] + 1.0f) * 0.5f, 1.0f);

	output.Position = (input.WVPosition + 1.0f) * 0.5f;

	float spec = 0.0f;
	
	if(IsTextured)
		spec = Specular.Sample(TextureSampler, input.Texel).r;
	else
		spec = MaterialSpecular.r;

	output.DepthSpec = float4(input.Position.z / input.Position.w, spec, MaterialSpecularExponent, 1.0f);

	return output;
}