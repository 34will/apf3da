#ifndef SCENE_GRAPH_H
#define SCENE_GRAPH_H

#include <d3d11.h>
#include <unordered_map>
#include <vector>
#include "Mesh.h"
#include "SceneObject.h"
#include "Pass.h"
#include "tinyxml2.h"

using namespace tinyxml2;

typedef unordered_map<string, int> StringIDLookup;
class CameraComponent;

class SceneGraph {
	friend class DirectX11App;

private:
	static SceneGraph * current;
	static void setCurrentScene(SceneGraph * s);

	unsigned int numObjects, numMeshes;
	StringIDLookup meshLookup, sObjectLookup;
	vector<Mesh *> meshes;
	vector<SceneObject *> sobjects;

	HRESULT loadSceneObject(int id, const string& name, XMLElement * sceneObjectElement);

public:
	static SceneGraph * CurrentScene();

	SceneGraph();
	~SceneGraph();

	HRESULT Initialise(XMLElement * sceneElement, ID3D11Device * dX11Device);
	void CleanUp();

	void Update(float dt);
	void Draw(ID3D11DeviceContext * dX11DeviceContext, Pass * pass, CameraComponent * currentCam);

	Mesh * GetMesh(const unsigned int & id);
	Mesh * GetMesh(const string & name);
	SceneObject * GetSceneObject(const unsigned int & id);
	SceneObject * GetSceneObject(const string & name);
};

#endif