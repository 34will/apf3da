#include "AnimationComponent.h"
#include "RenderComponent.h"

RegisterComponentType(AnimationComponent);

AnimationComponent::AnimationComponent() : Component(), mesh(NULL) {}

AnimationComponent::~AnimationComponent() {}

bool AnimationComponent::Initialise(XMLElement * init) {
	if(init == NULL)
		return false;

	RenderComponent * render = sceneObject->GetComponent<RenderComponent>();

	mesh = dynamic_cast<AnimatedMesh *>(render->mesh);

	if(mesh == NULL)
		return false;

	return true;
}

void AnimationComponent::Start() {
	mesh->Play();
}

void AnimationComponent::Shutdown() {
}

void AnimationComponent::Update(float dt) {
	if(mesh != NULL)
		mesh->Update(dt);
}

string AnimationComponent::GetTypename() const {
	return "AnimationComponent";
}