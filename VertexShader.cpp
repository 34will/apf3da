#include "VertexShader.h"

VertexShader::VertexShader(WCHAR * file, const D3D11_INPUT_ELEMENT_DESC * layout, UINT numElements, LPCSTR entryPoint, LPCSTR version) : Shader(file, entryPoint, version), layout(layout), numElements(numElements), shader(NULL), vertexLayout(NULL) { }

VertexShader::~VertexShader() {
	if(shader)
		shader->Release();
	if(vertexLayout)
		vertexLayout->Release();
}

HRESULT VertexShader::Initialise(ID3D11Device * dX11Device) {
	ID3DBlob * shaderBlob = NULL;
	HRESULT hr = Shader::CompileShaderFromFile(file, entryPoint, version, &shaderBlob);

	if(FAILED(hr)) {
		MessageBox(NULL, L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return hr;
	}

	hr = dX11Device->CreateVertexShader(shaderBlob->GetBufferPointer(), shaderBlob->GetBufferSize(), NULL, &shader);

	if(FAILED(hr)) {
		shaderBlob->Release();
		return hr;
	}

	hr = dX11Device->CreateInputLayout(layout, numElements, shaderBlob->GetBufferPointer(), shaderBlob->GetBufferSize(), &vertexLayout);
	shaderBlob->Release();

	if(FAILED(hr))
		return hr;

	hr = GenerateConstantBuffers(dX11Device);

	if(FAILED(hr))
		return hr;

	return hr;
}

void VertexShader::RegisterInputLayout(ID3D11DeviceContext * dX11DeviceContext) {
	dX11DeviceContext->IASetInputLayout(vertexLayout);
}

ID3D11VertexShader * VertexShader::getShaderHandle() { return shader; }

void VertexShader::Activate(ID3D11DeviceContext * dX11DeviceContext) {
	dX11DeviceContext->VSSetShader(shader, NULL, 0);
	dX11DeviceContext->IASetInputLayout(vertexLayout);
}

void VertexShader::SetConstantBuffers(ID3D11DeviceContext * dX11DeviceContext, const unsigned int & StartSlot, const unsigned int & NumBuffers, ID3D11Buffer * const * constantBuffers) const {
	dX11DeviceContext->VSSetConstantBuffers(StartSlot, NumBuffers, constantBuffers);
}

void VertexShader::SetTextures(ID3D11DeviceContext * dX11DeviceContext, const unsigned int & StartSlot, const unsigned int & NumBuffers, ID3D11ShaderResourceView * const * srvs) const {
	dX11DeviceContext->VSSetShaderResources(StartSlot, NumBuffers, srvs);
}

VertexShader& VertexShader::operator=(const VertexShader& other) {
	shader = other.shader;
	return *this;
}
