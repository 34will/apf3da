#ifndef COMPONENT_REGISTRAR_H
#define COMPONENT_REGISTRAR_H

#define SetupComponentType(ComponentType) static void RegisterComponent() { Register(#ComponentType, &CreateComponent<ComponentType>); } \
	friend class Component;

template<typename ComponentType>
class ComponentRegistrar {
public:
	ComponentRegistrar() {
		ComponentType::RegisterComponent();
	}

	virtual ~ComponentRegistrar() { }
};

#define RegisterComponentType(ComponentType) ComponentRegistrar<ComponentType> registrarOf_##ComponentType;

#endif