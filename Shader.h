#ifndef SHADER_H
#define SHADER_H

#include <d3d11.h>
#include <d3dcompiler.h>
#include <DirectXMath.h>
#include <regex>
#include <unordered_map>
#include <string>
#include "Includes.h"
#include "DXHelperStructs.h"
#include "Material.h"

using namespace std;
using namespace DirectX;

enum ShaderVariableType {
	VarNone = 0,
	Bool,
	Float,
	Float4,
	Float4x2,
	Float4x3,
	Float4x4,
	Float3,
	Float3x2,
	Float3x3,
	Float3x4,
	Float2,
	Float2x2,
	Float2x3,
	Float2x4,
	Int,
	Int4,
	Int4x2,
	Int4x3,
	Int4x4,
	Int3,
	Int3x2,
	Int3x3,
	Int3x4,
	Int2,
	Int2x2,
	Int2x3,
	Int2x4,
	Uint,
	Uint4,
	Uint4x2,
	Uint4x3,
	Uint4x4,
	Uint3,
	Uint3x2,
	Uint3x3,
	Uint3x4,
	Uint2,
	Uint2x2,
	Uint2x3,
	Uint2x4
};

struct CBufferStruct {
	friend class Shader;

private:
	const static Parser<tuple<ShaderVariableType, int>> typesAndSizes;
	static ID3D11Buffer * nullBuffer[1];

	string name;
	unsigned int bufferNum, totalSize;
	unordered_map<string, tuple<unsigned int, unsigned int>> variablesOffset;
	char * variables;
	bool modified, bufferBuilt, dataSet;
	ID3D11Buffer * buffer;

	void addVariable(string name, string type, int arraySize = 1);
	HRESULT buildBuffer(ID3D11Device * dX11Device);

	void Update(ID3D11DeviceContext * dX11DeviceContext);

	bool setVariable(string name, const void * object);
	void Shutdown();

public:
	CBufferStruct();
	CBufferStruct(string name, unsigned int bufferNum);
};

struct TextureSlot {
	unsigned int slot;
	ID3D11ShaderResourceView * const * srvs;
};

class Shader {
private:
	const static regex constantBufferRegex, textureRegex, variableRegex;

	unordered_map<string, CBufferStruct> constantBufferMap;
	unordered_map<string, TextureSlot> textureMap;
	unordered_map<string, string> cbVariableMap;
	vector<string> variables, textures;

protected:
	WCHAR * file;
	LPCSTR entryPoint, version;

	Shader(WCHAR * file, LPCSTR entryPoint, LPCSTR version);

	HRESULT GenerateConstantBuffers(ID3D11Device * dX11Device);

	virtual void SetConstantBuffers(ID3D11DeviceContext * dX11DeviceContext, const unsigned int & StartSlot, const unsigned int & NumBuffers, ID3D11Buffer * const * constantBuffers) const = 0;
	virtual void SetTextures(ID3D11DeviceContext * dX11DeviceContext, const unsigned int & StartSlot, const unsigned int & NumBuffers, ID3D11ShaderResourceView * const * srvs) const = 0;

public:
	static HRESULT CompileShaderFromFile(LPCWSTR szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob **ppBlobOut);

	virtual ~Shader();

	virtual HRESULT Initialise(ID3D11Device * dX11Device) = 0;
	virtual void Activate(ID3D11DeviceContext * dX11DeviceContext) = 0;
	virtual void Deactivate(ID3D11DeviceContext * dX11DeviceContext);

	void ActivateMaterial(const Material * material);
	void SetShaderData(ID3D11DeviceContext * dX11DeviceContext);

	bool SetVariable(const string & variable, const void * object);
	vector<string> GetVariableNames() const;

	bool SetTexture(const string & name, ID3D11ShaderResourceView * const * object);
	vector<string> GetTextureNames() const;

	Shader& operator=(const Shader& other);
};

#endif