Texture2D Diffuse	: register(t0);
Texture2D Normal	: register(t1);
Texture2D Position	: register(t2);
Texture2D DepthSpec : register(t3);
Texture2D Depth		: register(t4);

SamplerState TextureSampler : register(s0);

cbuffer DSFPSToRenderBuffer : register(b0) {
	uint ToRender;
	uint PosFrom;
	float2 _b0Buffer;
};

cbuffer DSFPSProjectionInverseBuffer : register(b1) {
	float4x4 ProjectionInverse;
	float4x4 ViewInverse;
	float4x4 ViewProjectionInverse;
	float4x4 CameraProjection;
};

cbuffer DSFPSCameraBuffer : register(b2) {
	float4 CameraPosition;
};

cbuffer DSFPSLightsBuffer : register(b3) {
	float4 LightPosition;
	float4 LightDirection;
	float4 LightDiffuse;
	float4 LightSpecular;
	int LightType;
	float LightRadius;
	float LightConeAngleCosine;
	float LightSpotDecay;
};

struct FinalVSOutput {
	float4	Position		: SV_POSITION;
	float2	Texel			: TEXCOORD0;
};

float4 Phong(float3 Position, float3 Normal, float3 Albedo, float SpecularIntensity, float SpecularPower) {
	if(LightType == 1) {
		float3 ToLight = LightPosition.xyz - Position.xyz;
		float Attenuation = saturate(1.0f - max(0.01f, length(ToLight)) / (LightRadius / 2));
		ToLight = normalize(ToLight);
		float3 Reflection = normalize(reflect(-ToLight, Normal));
		float3 Eye = normalize(CameraPosition.xyz - Position.xyz);
		float NL = dot(Normal, ToLight);

		float3 Diffuse = NL * LightDiffuse.xyz * Albedo;
		float Specular = SpecularIntensity * pow(saturate(dot(Reflection, Eye)), SpecularPower);

		return Attenuation * LightDiffuse * float4(Diffuse.rgb, Specular);
	} else
		return float4(1.0f, 0.0f, 1.0f, 1.0f);
}

float4 main(FinalVSOutput input) : SV_TARGET {
	float4 DiffuseFromTex = Diffuse.Sample(TextureSampler, input.Texel);
	float4 NormalFromTex = Normal.Sample(TextureSampler, input.Texel);
	float4 PositionFromTex = Position.Sample(TextureSampler, input.Texel);
	float4 DepthSpecFromTex = DepthSpec.Sample(TextureSampler, input.Texel);
	float4 DepthBuffer = Depth.Sample(TextureSampler, input.Texel);

	if(PositionFromTex.x == 0.0f && PositionFromTex.y == 0.0f && PositionFromTex.z == 0.0f)
		return float4(0.0f, 0.0f, 0.0f, 0.0f);

	float3 NonTextureNormal = (NormalFromTex.xyz * 2.0f) - 1.0f;
	float3 Normal = normalize(NonTextureNormal);

	if (ToRender == 1)
		return DiffuseFromTex;
	else if (ToRender == 2)
		return NormalFromTex;
	else if (ToRender == 3)
		return PositionFromTex;
	else if (ToRender == 4)
		return DepthBuffer;
	else if (ToRender == 5)
		return float4(DepthSpecFromTex.x, DepthSpecFromTex.x, DepthSpecFromTex.x, 1.0f);
	else if (ToRender == 6)
		return float4(DepthSpecFromTex.y, DepthSpecFromTex.y, DepthSpecFromTex.y, 1.0f);
	else if(ToRender == 7) {
		float4 Position = 1.0f;
		Position.x = (input.Texel.x * 2.0f) - 1.0f;
		Position.y = ((1.0f - input.Texel.y) * 2.0f) - 1.0f;
		Position.z = DepthSpecFromTex.x;

		Position = mul(Position, ViewProjectionInverse);
		Position /= Position.w;

		return Phong(Position, Normal, DiffuseFromTex.rgb, DepthSpecFromTex.y, 2.0f);
	} else {
		float3 NonTexturePosition = (PositionFromTex.xyz * 2.0f) - 1.0f;
		float3 Position = mul(float4(NonTexturePosition, 1.0f), transpose(ViewInverse)).xyz;

		/*float x = (input.Texel.x * 2.0f) - 1.0f;
		float y = ((1.0f - input.Texel.y) * 2.0f) - 1.0f;
		float4 projectedPos = float4(x, y, DepthBuffer.x, 1.0f);
		projectedPos = mul(projectedPos, transpose(ViewProjectionInverse));
		position = projectedPos.xyz / projectedPos.w;*/

		return Phong(Position, Normal, DiffuseFromTex.rgb, DepthSpecFromTex.y, 2.0f);
	}
}