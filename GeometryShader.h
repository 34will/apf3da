#ifndef GEOMETRYSHADER_H
#define GEOMETRYSHADER_H

#include "Shader.h"

class GeometryShader : public Shader {
private:
	ID3D11GeometryShader * shader;

protected:
	void SetConstantBuffers(ID3D11DeviceContext * dX11DeviceContext, const unsigned int & StartSlot, const unsigned int & NumBuffers, ID3D11Buffer * const * constantBuffers) const;
	void SetTextures(ID3D11DeviceContext * dX11DeviceContext, const unsigned int & StartSlot, const unsigned int & NumBuffers, ID3D11ShaderResourceView * const * srvs) const;

public:
	GeometryShader(WCHAR * file, LPCSTR entryPoint = "PS", LPCSTR version = "gs_4_0");
	~GeometryShader();

	HRESULT Initialise(ID3D11Device * dX11Device);

	ID3D11GeometryShader * getShaderHandle();

	void Activate(ID3D11DeviceContext * dX11DeviceContext);

	GeometryShader& operator=(const GeometryShader& other);
};

#endif
