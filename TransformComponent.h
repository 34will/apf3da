#ifndef TRANSFORM_COMPONENT_H
#define TRANSFORM_COMPONENT_H

#include <DirectXMath.h>
#include <vector>
#include "Component.h"
#include "Includes.h"
#include "ComponentRegistrar.h"

using namespace std;
using namespace DirectX;
using namespace tinyxml2;

class TransformComponent : public Component {
private:
	bool changed;
	TransformComponent * parent;
	vector<TransformComponent *> children;
	XMFLOAT4X4 local, world;
	XMFLOAT4 rotationQuaternion, worldRotationQuaternion;
	XMFLOAT3 position, worldPos, scale, worldScl, forward, right, localRotation;

	TransformComponent();

	bool doDecomp();
	void updateLocal();
	void updateWorld(XMFLOAT4X4 * parentMatrix);

public:
	~TransformComponent();

	bool Initialise(XMLElement * init);

	void Start();
	void Shutdown();

	void RotateAboutPoint(XMFLOAT3 point, float angle);

	void LocalMove(XMFLOAT3 offset);
	void LocalMove(float xOffset, float yOffset, float zOffset);

	void LocalTrack(float offset);
	void LocalStrafe(float offset);

	void LocalRotate(XMFLOAT3 offset, bool degrees = false);
	void LocalRotate(float xOffset, float yOffset, float zOffset, bool degrees = false);

	void LocalScale(XMFLOAT3 offset);
	void LocalScale(float xOffset, float yOffset, float zOffset);

	void RotateAboutGlobalUp(float angle);

	string GetTypename() const;

	TransformComponent * GetParent();
	void SetParent(TransformComponent * parent, bool maintainWorldPosition = false);

	XMFLOAT4X4 GetWorld() const;
	XMFLOAT4X4 * GetWorldPtr();

	XMFLOAT3 GetLocalPosition() const;
	XMFLOAT4 GetLocalRotationQuaternion() const;
	XMFLOAT3 GetLocalScale() const;

	XMFLOAT3 * GetLocalPositionPtr();
	XMFLOAT4 * GetLocalRotationQuaternionPtr();
	XMFLOAT3 * GetLocalScalePtr();

	XMFLOAT3 GetForward();
	XMFLOAT3 * GetForwardPtr();

	XMFLOAT3 GetRight();
	XMFLOAT3 * GetRightPtr();

	XMFLOAT3 GetWorldPosition();
	XMFLOAT4 GetWorldRotationQuaternion();
	XMFLOAT3 GetWorldScale();

	XMFLOAT3 * GetWorldPositionPtr();
	XMFLOAT4 * GetWorldRotationQuaternionPtr();
	XMFLOAT3 * GetWorldScalePtr();

	void SetLocal(XMFLOAT3 position, XMFLOAT4 rotationQuaternion, XMFLOAT3 scale);
	void SetLocal(float posX, float posY, float posZ, float rotX, float rotY, float rotZ, float sclX, float sclY, float sclZ, bool degrees = false);

	void SetLocalPosition(XMFLOAT3 position);
	void SetLocalPosition(float posX, float posY, float posZ);

	void SetLocalRotation(XMFLOAT4 rotationQuaternion);
	void SetLocalRotation(float rotX, float rotY, float rotZ, bool degrees = false);

	void SetLocalScale(XMFLOAT3 scale);
	void SetLocalScale(float sclX, float sclY, float sclZ);

	void SetForward(XMFLOAT3 scale);
	void SetForward(float sclX, float sclY, float sclZ);

	bool HasChanged() const;

	SetupComponentType(TransformComponent);
};

DeclareType(TransformComponent);

#endif