#include "ComponentFactory.h"

CreatorMap * ComponentFactory::creators = NULL;

void ComponentFactory::Register(const string& name, ComponentCreator creator) {
	if(creators == NULL)
		creators = new CreatorMap();

	creators->insert(pair<string, ComponentCreator>(name, creator));
}

Component * ComponentFactory::CreateComponent(const string& name) {
	CreatorMap::const_iterator found = creators->find(name);

	return (found == creators->cend() ? NULL : found->second());
}

void ComponentFactory::Cleanup() {
	if(creators == NULL) {
		delete creators;
		creators = NULL;
	}
}