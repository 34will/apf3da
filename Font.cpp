#include "Font.h"
#include <iostream>
#include <fstream>
#include "WICTextureLoader.h"
#include "DirectX11App.h"

Sentence::Sentence(string sentence, XMFLOAT2 screenPosition, float scale, Font & font, float spacing) : sentence(sentence), position(screenPosition), vertices(NULL), indicies(NULL), changed(true), font(font), colour(1.0f, 1.0f, 1.0f, 1.0f), vertexBuffer(NULL), indexBuffer(NULL), vertexCount(0), scale(scale), spacing(spacing) { }

Sentence::~Sentence() {
	CleanUp();
}

Sentence& Sentence::operator=(const Sentence &rhs) {
	this->sentence = rhs.sentence;
	this->position = rhs.position;
	this->font = rhs.font;
	this->scale = rhs.scale;
	this->spacing = rhs.spacing;
	this->colour = rhs.colour;
	this->changed = true;

	return *this;
}

HRESULT Sentence::Build(ID3D11Device * dX11Device) {
	if(changed) {
		int count = 0;
		for(unsigned int i = 0; i < sentence.length(); i++)
			count += (sentence[i] == '\n' || sentence[i] == ' ' ? 0 : 1);

		if(vertices != NULL) {
			delete[] vertices;
			vertices = NULL;
		}

		if(indicies != NULL) {
			delete[] indicies;
			indicies = NULL;
		}

		vertexCount = count * 6;
		vertices = new FontVertex[vertexCount];

		font.buildVertexArray(vertices, sentence, position, spacing, scale);
		changed = false;

		indicies = new unsigned int[vertexCount];
		for(unsigned int i = 0; i < vertexCount; i++)
			indicies[i] = i;

		if(vertexBuffer)
			vertexBuffer->Release();

		if(indexBuffer)
			indexBuffer->Release();

		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));
		bd.Usage = D3D11_USAGE_DYNAMIC;
		bd.ByteWidth = sizeof(FontVertex) * vertexCount;
		bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

		D3D11_SUBRESOURCE_DATA InitData;
		ZeroMemory(&InitData, sizeof(InitData));
		InitData.pSysMem = vertices;
		HRESULT hr = dX11Device->CreateBuffer(&bd, &InitData, &vertexBuffer);

		if(FAILED(hr))
			return hr;

		ZeroMemory(&bd, sizeof(bd));
		bd.Usage = D3D11_USAGE_IMMUTABLE;
		bd.ByteWidth = sizeof(unsigned int) * vertexCount;
		bd.BindFlags = D3D11_BIND_INDEX_BUFFER;

		ZeroMemory(&InitData, sizeof(InitData));
		InitData.pSysMem = indicies;
		hr = dX11Device->CreateBuffer(&bd, &InitData, &indexBuffer);

		if(FAILED(hr))
			return hr;

		return S_OK;
	}
	return S_OK;
}

void Sentence::CleanUp() {
	if(vertexBuffer)
		vertexBuffer->Release();
	if(indexBuffer)
		indexBuffer->Release();

	if(vertices != NULL) {
		delete[] vertices;
		vertices = NULL;
	}
	if(indicies != NULL) {
		delete[] indicies;
		indicies = NULL;
	}

	sentence = "";
	changed = true;
	position = XMFLOAT2(0, 0);
}

void Sentence::SetSentence(string sentence) {
	if(sentence != this->sentence) {
		changed = true;
		this->sentence = sentence;
	}
}

void Sentence::SetPosition(XMFLOAT2 position) {
	if(position.x != this->position.x && position.y != this->position.y) {
		changed = true;
		this->position = position;
	}
}

void Sentence::Draw(ID3D11DeviceContext * dX11DeviceContext, Pass * pass) {
	unsigned int stride = sizeof(FontVertex), offset = 0;
	dX11DeviceContext->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
	dX11DeviceContext->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);
	dX11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	pass->GetPixelShader()->SetVariable("FontColour", &colour);

	font.draw(dX11DeviceContext, pass, vertexCount);
}

Font::Font() : numChars(0), charWidth(0), charHeight(0), mapWidth(0), mapHeight(0), fontMap(NULL) { }

Font::~Font() {
	CleanUp();
}

HRESULT Font::Load(ID3D11Device * dX11Device, ID3D11DeviceContext * dX11DeviceContext, string fileName) {
	if(!fileName.empty() && (fileName.find(".txt") != string::npos || fileName.find(".TXT") != string::npos) && dX11Device != NULL) {
		size_t found = fileName.find_last_of('/');
		string path = fileName.substr(0, found + 1);

		string line = "", mapFileName = "";
		ifstream objFile(fileName);
		bool charMode = false;
		int charCount = 0;
		char * tempCharacters = NULL;

		if(objFile.is_open()) {
			while(!objFile.eof()) {
				getline(objFile, line);

				if(charMode) {
					tempCharacters[charCount] = line[0];
					charCount++;
				} else {
					if(line.find("map ") != string::npos)
						mapFileName = path + line.substr(4);
					else if(line[0] == 'h') {
						if(sscanf_s(line.c_str(), "h %u", &charHeight) != 1)
							return E_FAIL;
					} else if(line[0] == 'w') {
						if(sscanf_s(line.c_str(), "w %u", &charWidth) != 1)
							return E_FAIL;
					} else if(line[0] == 'm') {
						if(line[1] == 'h') {
							if(sscanf_s(line.c_str(), "mh %u", &mapHeight) != 1)
								return E_FAIL;
						} else if(line[1] == 'w') {
							if(sscanf_s(line.c_str(), "mw %u", &mapWidth) != 1)
								return E_FAIL;
						}
					} else if(line[0] == 'c') {
						if(sscanf_s(line.c_str(), "c %u", &numChars) != 1)
							return E_FAIL;
						else {
							tempCharacters = new char[numChars];
							cMap = CharacterMap();
							cMap.reserve(numChars);
						}
					} else if(line[0] == 'd' && line[1] == ':')
						charMode = true;
				}
			}
		} else
			return E_FAIL;

		wstring stemp = wstring(mapFileName.begin(), mapFileName.end());
		HRESULT hr = WIC::CreateWICTextureFromFile(dX11Device, dX11DeviceContext, stemp.c_str(), NULL, &fontMap);

		if(FAILED(hr))
			return hr;

		unsigned int tempCharCount = 0;
		int numX = (int)((float)mapWidth / (float)charWidth);
		int numY = (int)((float)mapHeight / (float)charHeight);

		float xWidth = 1.0f / (float)numX;
		float yWidth = 1.0f / (float)numY;

		for(int y = 0; y < numY; y++) {
			if(tempCharCount > numChars)
				break;

			for(int x = 0; x < numX; x++) {
				if(tempCharCount > numChars)
					break;

				cMap[tempCharacters[tempCharCount]] = FontCharacter();
				cMap[tempCharacters[tempCharCount]].character = tempCharacters[tempCharCount];
				cMap[tempCharacters[tempCharCount]].texCoords[0] = XMFLOAT2((float)x * xWidth, (float)y * yWidth);
				cMap[tempCharacters[tempCharCount]].texCoords[1] = XMFLOAT2((float)(x + 1) * xWidth, (float)y * yWidth);
				cMap[tempCharacters[tempCharCount]].texCoords[2] = XMFLOAT2((float)(x + 1) * xWidth, (float)(y + 1) * yWidth);
				cMap[tempCharacters[tempCharCount]].texCoords[3] = XMFLOAT2((float)x * xWidth, (float)(y + 1) * yWidth);

				tempCharCount++;
			}
		}

		delete[] tempCharacters;

		return S_OK;
	} else
		return E_FAIL;
}

void Font::CleanUp() {
	if(fontMap)
		fontMap->Release();
}

void Font::buildVertexArray(FontVertex * vertices, string sentence, XMFLOAT2 position, float spacing, float scale) {
	int numLetters = sentence.length(), index = 0;

	float scaledCharWidth = charWidth * scale, scaledCharHeight = charHeight * scale;
	unsigned long height = DirectX11App::Current()->GetHeight();
	position.y = height - position.y;

	for(int i = 0; i < numLetters; i++) {
		if(sentence[i] == '\n')
			position.y += scaledCharHeight;
		else if(sentence[i] == ' ')
			position.x += scaledCharWidth + spacing;
		else {
			FontCharacter ch = cMap[sentence[i]];

			vertices[index].position = XMFLOAT2(position.x, position.y);
			vertices[index].texCoord = ch.texCoords[0];
			vertices[index + 1].position = XMFLOAT2(position.x + scaledCharWidth, position.y - scaledCharHeight);
			vertices[index + 1].texCoord = ch.texCoords[2];
			vertices[index + 2].position = XMFLOAT2(position.x, position.y - scaledCharHeight);
			vertices[index + 2].texCoord = ch.texCoords[3];

			vertices[index + 3].position = XMFLOAT2(position.x, position.y);
			vertices[index + 3].texCoord = ch.texCoords[0];
			vertices[index + 4].position = XMFLOAT2(position.x + scaledCharWidth, position.y);
			vertices[index + 4].texCoord = ch.texCoords[1];
			vertices[index + 5].position = XMFLOAT2(position.x + scaledCharWidth, position.y - scaledCharHeight);
			vertices[index + 5].texCoord = ch.texCoords[2];

			index += 6;
			position.x += scaledCharWidth + spacing;
		}
	}
}

void Font::draw(ID3D11DeviceContext * dX11DeviceContext, Pass * pass, unsigned int indexCount) {
	pass->GetPixelShader()->SetTexture("FontTexture", &fontMap);

	pass->PreDraw(dX11DeviceContext);
	dX11DeviceContext->DrawIndexed(indexCount, 0, 0);
}