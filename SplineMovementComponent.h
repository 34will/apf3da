#ifndef SPLINE_MOVEMENT_COMPONENT_H
#define SPLINE_MOVEMENT_COMPONENT_H

#include <DirectXMath.h>
#include <vector>
#include "Component.h"
#include "TransformComponent.h"
#include "ComponentRegistrar.h"

using namespace std;
using namespace DirectX;
using namespace tinyxml2;

struct SplineSegement {
	XMFLOAT3 p1, p2, p3, p4;

	XMFLOAT3 GeneratePoint(float u);
};

struct LineSegment {
	XMFLOAT3 p1, p2, direction;
	float length;

	void Calculate(XMFLOAT3 p1, XMFLOAT3 p2);
};

class SplineMovementComponent : public Component {
private:
	TransformComponent * transform;
	vector<SplineSegement> segments;
	unsigned numSteps, currentPoint;
	float t, speed, scaledSpeed;
	bool yRot;
	XMFLOAT3 oldPos, oldDir;
	vector<LineSegment> curveSegments;

	SplineMovementComponent();

public:
	~SplineMovementComponent();

	bool Initialise(XMLElement * init);

	void Start();
	void Shutdown();

	void Update(float dt);

	void AddSegment(XMFLOAT3 p1, XMFLOAT3 p2, XMFLOAT3 p3, XMFLOAT3 p4);
	void AddSegment(SplineSegement s);

	void ComputePath();

	string GetTypename() const;

	SetupComponentType(SplineMovementComponent);
};

DeclareType(SplineMovementComponent);

#endif