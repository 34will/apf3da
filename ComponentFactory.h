#ifndef COMPONENT_FACTORY_H
#define COMPONENT_FACTORY_H

#include <string>
#include <unordered_map>
#include "SceneObject.h"
#include "SceneGraph.h"

using namespace std;

class Component;

typedef Component * (*ComponentCreator)();
typedef unordered_map<string, ComponentCreator> CreatorMap;

class ComponentFactory {
	friend class Component;
	friend class SceneGraph;

private:
	static CreatorMap * creators;

	static void Register(const string& name, ComponentCreator creator);
	static Component * CreateComponent(const string& name);

public:
	static void Cleanup();
};

#endif