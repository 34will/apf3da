#include "SceneGraph.h"
#include "ComponentFactory.h"
#include "Component.h"
#include "MeshLoader.h"

SceneGraph::SceneGraph() : numObjects(0), numMeshes(0), meshLookup(), sObjectLookup(), meshes(), sobjects() { }

SceneGraph::~SceneGraph() {
	CleanUp();
}

HRESULT SceneGraph::loadSceneObject(int id, const string& name, XMLElement * sceneObjectElement) {
	if(sceneObjectElement == NULL)
		return E_FAIL;

	SceneObject * so = new SceneObject(name);

	XMLElement * component = sceneObjectElement->FirstChildElement();
	while(component != NULL) {
		string cType = component->Value();
		Component * c = (cType.size() == 0 ? NULL : ComponentFactory::CreateComponent(cType));

		if(c == NULL)
			return E_FAIL;
		else {
			so->AddComponent(c);

			if(!c->Initialise(component))
				return E_FAIL;

			component = component->NextSiblingElement();
		}
	}

	sObjectLookup[name] = id;
	sobjects[id] = so;

	return S_OK;
}

HRESULT SceneGraph::Initialise(XMLElement * sceneElement, ID3D11Device * dX11Device) {
	if(sceneElement == NULL)
		return E_FAIL;

	XMLElement * meshBankElement = sceneElement->FirstChildElement("MeshBank");

	unsigned int currentMesh = 0;
	if(meshBankElement->QueryUnsignedAttribute("count", &currentMesh) != XML_SUCCESS)
		return E_FAIL;

	numMeshes = currentMesh;
	meshes = vector<Mesh *>(numMeshes);
	meshLookup = StringIDLookup(numMeshes);

	XMLElement * meshParams = meshBankElement->FirstChildElement("Mesh");

	currentMesh = 0;
	HRESULT hr = S_OK;

	while(meshParams != NULL && currentMesh < numMeshes) {
		string meshType = meshParams->Attribute("type");
		if(meshType == "OBJ" || meshType == "MD5") {
			string filename = meshParams->Attribute("src"), name = meshParams->Attribute("name"), scale = meshParams->Attribute("scale");
			meshes[currentMesh] = NULL;
			hr = MeshLoader::LoadMesh(meshes[currentMesh], filename, stof(scale.c_str()), meshParams);
			meshLookup[name] = currentMesh;
			if(FAILED(hr)) {
				string output = "Loading " + name + " failed.\n";
				OutputDebugStringA(output.c_str());
				return hr;
			}
		}

		currentMesh++;
		meshParams = meshParams->NextSiblingElement("Mesh");
	}

	for(auto x : meshes) {
		hr = x->BuildBuffers(dX11Device);

		if(FAILED(hr))
			return hr;
	}

	XMLElement * sceneGraphElement = sceneElement->FirstChildElement("SceneGraph");

	if(sceneGraphElement == NULL)
		return E_FAIL;

	unsigned int currentObject = 0;
	if(sceneGraphElement->QueryUnsignedAttribute("count", &currentObject) != XML_SUCCESS)
		return E_FAIL;

	numObjects = currentObject;
	sobjects = vector<SceneObject *>(numObjects);
	sObjectLookup = StringIDLookup(numObjects);

	XMLElement * sceneObjectParams = sceneGraphElement->FirstChildElement("SceneObject");

	currentObject = 0;
	hr = S_OK;

	while(sceneObjectParams != NULL && currentObject < numObjects) {
		string name = sceneObjectParams->Attribute("name");
		hr = loadSceneObject(currentObject, name, sceneObjectParams);

		if(FAILED(hr)) {
			string output = "Creating " + name + " failed.\n";
			OutputDebugStringA(output.c_str());
			return hr;
		}

		currentObject++;
		sceneObjectParams = sceneObjectParams->NextSiblingElement("SceneObject");
	}

	for(auto x : sobjects)
		x->Start();

	return S_OK;
}

void SceneGraph::CleanUp() {
	for(unsigned int i = 0; i < sobjects.size(); i++) {
		if(sobjects[i] != NULL) {
			sobjects[i]->Shutdown();
			delete sobjects[i];
			sobjects[i] = NULL;
		}
	}

	sobjects.clear();

	for(unsigned int i = 0; i < meshes.size(); i++) {
		meshes[i]->CleanUp();
		delete meshes[i];
		meshes[i] = NULL;
	}

	meshes.clear();
}

void SceneGraph::Update(float dt) {
	for(auto x : sobjects)
		x->Update(dt);
}

void SceneGraph::Draw(ID3D11DeviceContext * dX11DeviceContext, Pass * pass, CameraComponent * currentCam) {
	for(auto x : sobjects)
		x->Draw(dX11DeviceContext, pass, currentCam);
}

Mesh * SceneGraph::GetMesh(const unsigned int & id) {
	return (id >= 0 && id < meshes.size() ? meshes[id] : NULL);
}

Mesh * SceneGraph::GetMesh(const string & name) {
	StringIDLookup::const_iterator found = meshLookup.find(name);
	return (found != meshLookup.cend() ? meshes[found->second] : NULL);
}

SceneObject * SceneGraph::GetSceneObject(const unsigned int & id) {
	return (id >= 0 && id < sobjects.size() ? sobjects[id] : NULL);
}

SceneObject * SceneGraph::GetSceneObject(const string & name) {
	StringIDLookup::const_iterator found = sObjectLookup.find(name);
	return (found != sObjectLookup.cend() ? sobjects[found->second] : NULL);
}

SceneGraph * SceneGraph::current = NULL;

void SceneGraph::setCurrentScene(SceneGraph * s) {
	current = s;
}

SceneGraph * SceneGraph::CurrentScene() {
	return current;
}