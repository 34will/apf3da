#include "DirectX11App.h"
#include <iostream>
#include <string>
#include "PixelShader.h"
#include "VertexShader.h"
#include "GeometryShader.h"
#include "SceneObject.h"
#include "Includes.h"

using namespace std;

DirectX11App::DirectX11App() : driverType(D3D_DRIVER_TYPE_NULL), featureLevel(D3D_FEATURE_LEVEL_11_0), dX11Device(NULL), dX11DeviceContext(NULL), dX11SwapChain(NULL), dX11RenderTargetView(NULL), dX11VertexLayout(NULL), pass(), mainCamera(NULL), moveTrans(NULL), doDeferredRender(false), textureToRender(1), positionFrom(0) {
	current = this;
}

DirectX11App::~DirectX11App() {}

HRESULT DirectX11App::initSwapChain(HWND hWnd) {
	RECT rc;
	GetClientRect(hWnd, &rc);
	width = rc.right - rc.left;
	height = rc.bottom - rc.top;

	DXGI_SWAP_CHAIN_DESC sd;

	ZeroMemory(&sd, sizeof(sd));
	sd.BufferCount = 1;
	sd.BufferDesc.Width = width;
	sd.BufferDesc.Height = height;
	sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	sd.BufferDesc.RefreshRate.Numerator = 60;
	sd.BufferDesc.RefreshRate.Denominator = 1;
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sd.OutputWindow = hWnd;
	sd.SampleDesc.Count = 1;
	sd.SampleDesc.Quality = 0;
	sd.Windowed = true;

	D3D_DRIVER_TYPE driverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE,
	};
	UINT numDriverTypes = ARRAYSIZE(driverTypes);

	D3D_FEATURE_LEVEL featureLevels[] =
	{
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
	};
	UINT numFeatureLevels = ARRAYSIZE(featureLevels);
	HRESULT hr;

	for(UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++) {
		driverType = driverTypes[driverTypeIndex];
		hr = D3D11CreateDeviceAndSwapChain(NULL, driverType, NULL, 0, featureLevels, numFeatureLevels, D3D11_SDK_VERSION, &sd, &dX11SwapChain, &dX11Device, &featureLevel, &dX11DeviceContext);
		if(SUCCEEDED(hr))
			break;
	}

	if(FAILED(hr))
		return hr;

	ID3D11Texture2D * backBuffer = NULL;
	hr = dX11SwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&backBuffer);
	if(FAILED(hr))
		return hr;

	hr = dX11Device->CreateRenderTargetView(backBuffer, NULL, &dX11RenderTargetView);
	backBuffer->Release();
	if(FAILED(hr))
		return hr;

	D3D11_VIEWPORT vp;
	vp.Width = (FLOAT)width;
	vp.Height = (FLOAT)height;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	dX11DeviceContext->RSSetViewports(1, &vp);

	return S_OK;
}

HRESULT DirectX11App::initStates(XMLElement * dXParams) {
	HRESULT hr;
	D3D11_TEXTURE2D_DESC depthBufferDesc;
	D3D11_DEPTH_STENCIL_DESC depthStencilDesc, noDepthStencilDesc;
	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	D3D11_RASTERIZER_DESC rasterDesc;
	D3D11_BLEND_DESC blendDesc;

	string line = "";
	bool bValue = false;

	ZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));
	XMLElement * dStencil = dXParams->FirstChildElement("DepthStencil");
	if(dStencil->FirstChildElement("DepthEnable")->QueryBoolText(&bValue) != XML_SUCCESS) return E_FAIL;
	else depthStencilDesc.DepthEnable = bValue;
	line = dStencil->FirstChildElement("DepthWriteMask")->GetText();
	if(!line.empty()) depthStencilDesc.DepthWriteMask = DXHelper::ToDepthWriteMask(line);
	else return E_FAIL;
	line = dStencil->FirstChildElement("DepthFunc")->GetText();
	if(!line.empty()) depthStencilDesc.DepthFunc = DXHelper::ToCompareFunc(line);
	else return E_FAIL;
	if(dStencil->FirstChildElement("StencilEnable")->QueryBoolText(&bValue) != XML_SUCCESS) return E_FAIL;
	else depthStencilDesc.StencilEnable = bValue;
	line = dStencil->FirstChildElement("StencilReadMask")->GetText();
	if(!line.empty()) depthStencilDesc.StencilReadMask = DXHelper::ToMask(line);
	else return E_FAIL;
	line = dStencil->FirstChildElement("StencilReadMask")->GetText();
	if(!line.empty()) depthStencilDesc.StencilWriteMask = DXHelper::ToMask(line);
	else return E_FAIL;
	XMLElement * fFace = dStencil->FirstChildElement("FrontFace");
	line = fFace->FirstChildElement("StencilDepthFailOp")->GetText();
	if(!line.empty()) depthStencilDesc.FrontFace.StencilDepthFailOp = DXHelper::ToStencilOp(line);
	else return E_FAIL;
	line = fFace->FirstChildElement("StencilFailOp")->GetText();
	if(!line.empty()) depthStencilDesc.FrontFace.StencilFailOp = DXHelper::ToStencilOp(line);
	else return E_FAIL;
	line = fFace->FirstChildElement("StencilPassOp")->GetText();
	if(!line.empty()) depthStencilDesc.FrontFace.StencilPassOp = DXHelper::ToStencilOp(line);
	else return E_FAIL;
	line = fFace->FirstChildElement("StencilFunc")->GetText();
	if(!line.empty()) depthStencilDesc.FrontFace.StencilFunc = DXHelper::ToCompareFunc(line);
	else return E_FAIL;
	XMLElement * bFace = dStencil->FirstChildElement("BackFace");
	line = bFace->FirstChildElement("StencilDepthFailOp")->GetText();
	if(!line.empty()) depthStencilDesc.BackFace.StencilDepthFailOp = DXHelper::ToStencilOp(line);
	else return E_FAIL;
	line = bFace->FirstChildElement("StencilFailOp")->GetText();
	if(!line.empty()) depthStencilDesc.BackFace.StencilFailOp = DXHelper::ToStencilOp(line);
	else return E_FAIL;
	line = bFace->FirstChildElement("StencilPassOp")->GetText();
	if(!line.empty()) depthStencilDesc.BackFace.StencilPassOp = DXHelper::ToStencilOp(line);
	else return E_FAIL;
	line = bFace->FirstChildElement("StencilFunc")->GetText();
	if(!line.empty()) depthStencilDesc.BackFace.StencilFunc = DXHelper::ToCompareFunc(line);
	else return E_FAIL;

	ZeroMemory(&rasterDesc, sizeof(rasterDesc));
	XMLElement * rState = dXParams->FirstChildElement("RasterState");
	if(rState->FirstChildElement("AntialiasedLineEnable")->QueryBoolText(&bValue) != XML_SUCCESS) return E_FAIL;
	else rasterDesc.AntialiasedLineEnable = bValue;
	line = rState->FirstChildElement("CullMode")->GetText();
	if(!line.empty()) rasterDesc.CullMode = DXHelper::ToCullMode(line);
	else return E_FAIL;
	if(rState->FirstChildElement("DepthBias")->QueryBoolText(&bValue) != XML_SUCCESS) return E_FAIL;
	else rasterDesc.DepthBias = bValue;
	if(rState->FirstChildElement("DepthBiasClamp")->QueryBoolText(&bValue) != XML_SUCCESS) return E_FAIL;
	else rasterDesc.DepthBiasClamp = bValue;
	if(rState->FirstChildElement("DepthClipEnable")->QueryBoolText(&bValue) != XML_SUCCESS) return E_FAIL;
	else rasterDesc.DepthClipEnable = bValue;
	line = rState->FirstChildElement("FillMode")->GetText();
	if(!line.empty()) rasterDesc.FillMode = DXHelper::ToFillMode(line);
	else return E_FAIL;
	if(rState->FirstChildElement("FrontCounterClockwise")->QueryBoolText(&bValue) != XML_SUCCESS) return E_FAIL;
	else rasterDesc.FrontCounterClockwise = bValue;
	if(rState->FirstChildElement("MultisampleEnable")->QueryBoolText(&bValue) != XML_SUCCESS) return E_FAIL;
	else rasterDesc.MultisampleEnable = bValue;
	if(rState->FirstChildElement("ScissorEnable")->QueryBoolText(&bValue) != XML_SUCCESS) return E_FAIL;
	else rasterDesc.ScissorEnable = bValue;
	if(rState->FirstChildElement("SlopeScaledDepthBias")->QueryBoolText(&bValue) != XML_SUCCESS) return E_FAIL;
	else rasterDesc.SlopeScaledDepthBias = bValue;

	ZeroMemory(&blendDesc, sizeof(blendDesc));
	XMLElement * bState = dXParams->FirstChildElement("BlendState");
	if(bState->FirstChildElement("AlphaToCoverageEnable")->QueryBoolText(&bValue) != XML_SUCCESS) return E_FAIL;
	else blendDesc.AlphaToCoverageEnable = bValue;
	if(bState->FirstChildElement("IndependentBlendEnable")->QueryBoolText(&bValue) != XML_SUCCESS) return E_FAIL;
	else blendDesc.IndependentBlendEnable = bValue;
	XMLElement * rTOneState = bState->FirstChildElement("RenderTargetOne");
	if(rTOneState->FirstChildElement("BlendEnable")->QueryBoolText(&bValue) != XML_SUCCESS) return E_FAIL;
	else blendDesc.RenderTarget[0].BlendEnable = bValue;
	line = rTOneState->FirstChildElement("BlendOp")->GetText();
	if(!line.empty()) blendDesc.RenderTarget[0].BlendOp = DXHelper::ToBlendOp(line);
	else return E_FAIL;
	line = rTOneState->FirstChildElement("BlendOpAlpha")->GetText();
	if(!line.empty()) blendDesc.RenderTarget[0].BlendOpAlpha = DXHelper::ToBlendOp(line);
	else return E_FAIL;
	line = rTOneState->FirstChildElement("SrcBlend")->GetText();
	if(!line.empty()) blendDesc.RenderTarget[0].SrcBlend = DXHelper::ToBlend(line);
	else return E_FAIL;
	line = rTOneState->FirstChildElement("SrcBlendAlpha")->GetText();
	if(!line.empty()) blendDesc.RenderTarget[0].SrcBlendAlpha = DXHelper::ToBlend(line);
	else return E_FAIL;
	line = rTOneState->FirstChildElement("DestBlend")->GetText();
	if(!line.empty()) blendDesc.RenderTarget[0].DestBlend = DXHelper::ToBlend(line);
	else return E_FAIL;
	line = rTOneState->FirstChildElement("DestBlendAlpha")->GetText();
	if(!line.empty()) blendDesc.RenderTarget[0].DestBlendAlpha = DXHelper::ToBlend(line);
	else return E_FAIL;
	line = rTOneState->FirstChildElement("RenderTargetWriteMask")->GetText();
	if(!line.empty()) blendDesc.RenderTarget[0].RenderTargetWriteMask = DXHelper::ToMask(line);
	else return E_FAIL;

	ZeroMemory(&depthBufferDesc, sizeof(depthBufferDesc));
	depthBufferDesc.Width = width;
	depthBufferDesc.Height = height;
	depthBufferDesc.MipLevels = 1;
	depthBufferDesc.ArraySize = 1;
	depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthBufferDesc.SampleDesc.Count = 1;
	depthBufferDesc.SampleDesc.Quality = 0;
	depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthBufferDesc.CPUAccessFlags = 0;
	depthBufferDesc.MiscFlags = 0;

	hr = dX11Device->CreateTexture2D(&depthBufferDesc, NULL, &dX11DepthStencilBuffer);
	if(FAILED(hr))
		return hr;

	ZeroMemory(&noDepthStencilDesc, sizeof(noDepthStencilDesc));
	noDepthStencilDesc = depthStencilDesc;
	noDepthStencilDesc.DepthEnable = false;

	hr = dX11Device->CreateDepthStencilState(&depthStencilDesc, &dX11DepthStencilState);
	if(FAILED(hr))
		return hr;

	hr = dX11Device->CreateDepthStencilState(&noDepthStencilDesc, &dX11DepthStencilDisabledState);
	if(FAILED(hr))
		return hr;

	dX11DeviceContext->OMSetDepthStencilState(dX11DepthStencilState, 1);

	ZeroMemory(&depthStencilViewDesc, sizeof(depthStencilViewDesc));
	depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	depthStencilViewDesc.Texture2D.MipSlice = 0;

	hr = dX11Device->CreateDepthStencilView(dX11DepthStencilBuffer, &depthStencilViewDesc, &dX11DepthStencilView);
	if(FAILED(hr))
		return hr;

	dX11DeviceContext->OMSetRenderTargets(1, &dX11RenderTargetView, dX11DepthStencilView);

	hr = dX11Device->CreateRasterizerState(&rasterDesc, &dX11RasterState);
	if(FAILED(hr))
		return hr;

	ZeroMemory(&rasterDesc, sizeof(D3D11_RASTERIZER_DESC));
	rasterDesc.FillMode = D3D11_FILL_WIREFRAME;
	rasterDesc.CullMode = D3D11_CULL_NONE;

	hr = dX11Device->CreateRasterizerState(&rasterDesc, &dX11WireframeState);
	if(FAILED(hr))
		return hr;

	dX11DeviceContext->RSSetState(dX11RasterState);

	hr = dX11Device->CreateBlendState(&blendDesc, &dX11BlendState);
	if(FAILED(hr))
		return hr;

	blendDesc.RenderTarget[0].BlendEnable = false;
	hr = dX11Device->CreateBlendState(&blendDesc, &dX11BlendDisabledState);

	if(FAILED(hr))
		return hr;

	float blendFactor[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
	UINT sampleMask = 0xffffffff;
	dX11DeviceContext->OMSetBlendState(dX11BlendState, blendFactor, sampleMask);

	return S_OK;
}

HRESULT DirectX11App::initFont() {
	PixelShader * pShader = new PixelShader(L"FontPixelShader.hlsl", "main");
	HRESULT hr = pShader->Initialise(dX11Device);

	if(FAILED(hr))
		return hr;

	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 8, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	VertexShader * vShader = new VertexShader(L"FontVertexShader.hlsl", layout, _countof(layout), "main");
	hr = vShader->Initialise(dX11Device);

	if(FAILED(hr))
		return hr;

	vShader->RegisterInputLayout(dX11DeviceContext);

	fontPass.SetPixelShader(pShader);
	fontPass.SetVertexShader(vShader);

	f = Font();
	hr = f.Load(dX11Device, dX11DeviceContext, "Fonts/PixelFont.txt");

	if(FAILED(hr))
		return hr;

	s1 = new Sentence("Forward Render: Diffuse Texture", XMFLOAT2(5.0f, 5.0f), 0.5f, f, -5.0f);
	hr = s1->Build(dX11Device);

	if(FAILED(hr))
		return hr;

	s2 = new Sentence("View Frustrum: Main Camera", XMFLOAT2(5.0f, 25.0f), 0.5f, f, -5.0f);
	hr = s2->Build(dX11Device);

	if(FAILED(hr))
		return hr;

	return S_OK;
}

HRESULT DirectX11App::initShaders() {
	PixelShader * pShader = new PixelShader(L"PixelShader.hlsl", "main");
	HRESULT hr = pShader->Initialise(dX11Device);

	if(FAILED(hr))
		return hr;

	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 36, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	VertexShader * vShader = new VertexShader(L"VertexShader.hlsl", layout, _countof(layout), "main");
	hr = vShader->Initialise(dX11Device);

	if(FAILED(hr))
		return hr;

	vShader->RegisterInputLayout(dX11DeviceContext);
	dX11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	pass.SetPixelShader(pShader);
	pass.SetVertexShader(vShader);

	return S_OK;
}

HRESULT DirectX11App::initDeferredShading() {
	HRESULT hr = initDeferredShaders();

	if(FAILED(hr))
		return hr;

	hr = initDeferredShadingRTs();

	if(FAILED(hr))
		return hr;

	hr = initDeferredShadingFSQ();

	if(FAILED(hr))
		return hr;

	textureToRender = 0;

	return S_OK;
}

HRESULT DirectX11App::initDeferredShaders() {
	PixelShader * pShader = new PixelShader(L"DeferredShadingFinalPS.hlsl", "main");
	HRESULT hr = pShader->Initialise(dX11Device);

	if(FAILED(hr))
		return hr;

	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 1, DXGI_FORMAT_R32_UINT, 0, 20, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	VertexShader * vShader = new VertexShader(L"DeferredShadingFinalVS.hlsl", layout, _countof(layout), "main");
	hr = vShader->Initialise(dX11Device);

	if(FAILED(hr))
		return hr;

	dsPassTwo.SetPixelShader(pShader);
	dsPassTwo.SetVertexShader(vShader);

	pShader = NULL;
	vShader = NULL;

	pShader = new PixelShader(L"DeferredShadingPS.hlsl", "main");
	hr = pShader->Initialise(dX11Device);

	if(FAILED(hr))
		return hr;

	D3D11_INPUT_ELEMENT_DESC newLayout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 36, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	vShader = new VertexShader(L"DeferredShadingVS.hlsl", newLayout, _countof(newLayout), "main");
	hr = vShader->Initialise(dX11Device);

	if(FAILED(hr))
		return hr;

	dsPassOne.SetPixelShader(pShader);
	dsPassOne.SetVertexShader(vShader);

	D3D11_BLEND_DESC blendDesc;
	blendDesc.AlphaToCoverageEnable = false;
	blendDesc.IndependentBlendEnable = false;
	blendDesc.RenderTarget[0].BlendEnable = true;
	blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	hr = dX11Device->CreateBlendState(&blendDesc, &dX11DeferredRenderingState);

	if(FAILED(hr))
		return hr;

	return S_OK;
}

HRESULT DirectX11App::initDeferredShadingRTs() {
	numberOfRenderTargets = 4;
	D3D11_TEXTURE2D_DESC textureDesc, depthBufferDesc;
	HRESULT hr;
	D3D11_RENDER_TARGET_VIEW_DESC renderTargetViewDesc;
	D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc;
	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;

	ZeroMemory(&depthBufferDesc, sizeof(depthBufferDesc));
	depthBufferDesc.Width = width;
	depthBufferDesc.Height = height;
	depthBufferDesc.MipLevels = 1;
	depthBufferDesc.ArraySize = 1;
	depthBufferDesc.Format = DXGI_FORMAT_R24G8_TYPELESS;
	depthBufferDesc.SampleDesc.Count = 1;
	depthBufferDesc.SampleDesc.Quality = 0;
	depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	depthBufferDesc.CPUAccessFlags = 0;
	depthBufferDesc.MiscFlags = 0;

	hr = dX11Device->CreateTexture2D(&depthBufferDesc, NULL, &deferredShadingDepthStencilBuffer);

	if(FAILED(hr))
		return hr;

	ZeroMemory(&depthStencilViewDesc, sizeof(depthStencilViewDesc));
	depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	depthStencilViewDesc.Texture2D.MipSlice = 0;

	hr = dX11Device->CreateDepthStencilView(deferredShadingDepthStencilBuffer, &depthStencilViewDesc, &deferredShadingDepthStencilView);

	if(FAILED(hr))
		return hr;

	ZeroMemory(&textureDesc, sizeof(textureDesc));
	textureDesc.Width = width;
	textureDesc.Height = height;
	textureDesc.MipLevels = 1;
	textureDesc.ArraySize = 1;
	textureDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = 0;

	for(unsigned int i = 0; i < numberOfRenderTargets; i++) {
		hr = dX11Device->CreateTexture2D(&textureDesc, NULL, &deferredShadingTextureArray[i]);

		if(FAILED(hr))
			return hr;
	}

	ZeroMemory(&renderTargetViewDesc, sizeof(renderTargetViewDesc));
	renderTargetViewDesc.Format = textureDesc.Format;
	renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	renderTargetViewDesc.Texture2D.MipSlice = 0;

	for(unsigned int i = 0; i < numberOfRenderTargets; i++) {
		hr = dX11Device->CreateRenderTargetView(deferredShadingTextureArray[i], &renderTargetViewDesc, &deferredShadingRTVArray[i]);

		if(FAILED(hr))
			return hr;
	}

	ZeroMemory(&shaderResourceViewDesc, sizeof(shaderResourceViewDesc));
	shaderResourceViewDesc.Format = textureDesc.Format;
	shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	shaderResourceViewDesc.Texture2D.MostDetailedMip = 0;
	shaderResourceViewDesc.Texture2D.MipLevels = 1;

	for(unsigned int i = 0; i < numberOfRenderTargets; i++) {
		hr = dX11Device->CreateShaderResourceView(deferredShadingTextureArray[i], &shaderResourceViewDesc, &deferredShadingSRVArray[i]);

		if(FAILED(hr))
			return hr;
	}

	ZeroMemory(&shaderResourceViewDesc, sizeof(shaderResourceViewDesc));
	shaderResourceViewDesc.Format = DXGI_FORMAT_R24_UNORM_X8_TYPELESS;
	shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	shaderResourceViewDesc.Texture2D.MostDetailedMip = 0;
	shaderResourceViewDesc.Texture2D.MipLevels = 1;

	hr = dX11Device->CreateShaderResourceView(deferredShadingDepthStencilBuffer, &shaderResourceViewDesc, &deferredShadingDepthSRV);

	if(FAILED(hr))
		return hr;

	return S_OK;
}

HRESULT DirectX11App::initDeferredShadingFSQ() {
	FSQVertex v[4];

	v[0].Position = XMFLOAT3(-1.0f, 1.0f, 0);
	v[0].TexCoord = XMFLOAT2(0, 0);
	v[0].Index = 0;

	v[1].Position = XMFLOAT3(1.0f, 1.0f, 0);
	v[1].TexCoord = XMFLOAT2(1, 0);
	v[1].Index = 1;

	v[2].Position = XMFLOAT3(1.0f, -1.0f, 0);
	v[2].TexCoord = XMFLOAT2(1, 1);
	v[2].Index = 2;

	v[3].Position = XMFLOAT3(-1.0f, -1.0f, 0);
	v[3].TexCoord = XMFLOAT2(0, 1);
	v[3].Index = 3;

	UINT i[6] = { 0, 1, 3, 2, 3, 1 };

	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(FSQVertex) * 4;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = v;

	HRESULT hr = dX11Device->CreateBuffer(&bd, &InitData, &deferredShadingFSQVertexBuffer);

	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(UINT) * 6;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;

	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = i;

	hr = dX11Device->CreateBuffer(&bd, &InitData, &deferredShadingFSQIndexBuffer);

	if(FAILED(hr))
		return hr;

	return S_OK;
}

HRESULT DirectX11App::InitDevice(HINSTANCE hInst, HWND hWnd, XMLElement * dXParams) {
	HRESULT hr;

	if(dXParams == NULL)
		return E_FAIL;

	hr = initSwapChain(hWnd);

	if(FAILED(hr))
		return hr;

	hr = initStates(dXParams);

	if(FAILED(hr))
		return hr;

	hr = initDeferredShading();

	if(FAILED(hr))
		return hr;

	hr = initShaders();

	if(FAILED(hr))
		return hr;

	hr = initFont();

	if(FAILED(hr))
		return hr;

	if(!Input::Initialize(hInst, hWnd, width, height))
		return E_FAIL;

	return S_OK;
}

HRESULT DirectX11App::InitScene(XMLElement * sceneParams) {
	if(sceneParams == NULL)
		return E_FAIL;

	if(sceneParams->QueryFloatAttribute("clearR", &ClearColour[0]) != XML_SUCCESS)
		return E_FAIL;
	if(sceneParams->QueryFloatAttribute("clearG", &ClearColour[1]) != XML_SUCCESS)
		return E_FAIL;
	if(sceneParams->QueryFloatAttribute("clearB", &ClearColour[2]) != XML_SUCCESS)
		return E_FAIL;
	ClearColour[4] = 0.0f;

	SceneGraph::setCurrentScene(&sg);

	HRESULT hr = sg.Initialise(sceneParams, dX11Device);

	if(FAILED(hr))
		return hr;

	SceneObject * camera = sg.GetSceneObject("MainCamera");

	if(camera == NULL)
		return E_FAIL;

	mainCamera = camera->GetComponent<CameraComponent>();

	if(mainCamera == NULL)
		return E_FAIL;

	cameraTrans = camera->GetComponent<TransformComponent>();

	if(cameraTrans == NULL)
		return E_FAIL;

	SceneObject * ship = sg.GetSceneObject("CameraController");

	moveTrans = ship->GetComponent<TransformComponent>();

	if(moveTrans == NULL)
		return E_FAIL;

	SceneObject * mcc = sg.GetSceneObject("MainCameraControl");

	cameraControl = mcc->GetComponent<TransformComponent>();

	if(cameraControl == NULL)
		return E_FAIL;

	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory(&sampDesc, sizeof(sampDesc));
	sampDesc.Filter = D3D11_FILTER_ANISOTROPIC;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampDesc.MinLOD = 0;
	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;

	hr = dX11Device->CreateSamplerState(&sampDesc, &anisoSamplerState);

	if(FAILED(hr))
		return hr;

	lights = LightComponent::GetLights();

	return S_OK;
}

void DirectX11App::CleanupDevice() {
	sg.CleanUp();

	if(s1 != NULL) {
		delete s1;
		s1 = NULL;
	}
	f.CleanUp();
	fontPass.Shutdown();

	pass.Shutdown();

	dsPassOne.Shutdown();
	dsPassTwo.Shutdown();

	if(deferredShadingDepthStencilView)
		deferredShadingDepthStencilView->Release();
	if(deferredShadingDepthStencilBuffer)
		deferredShadingDepthStencilBuffer->Release();
	for(unsigned int i = 0; i < numberOfRenderTargets; i++) {
		if(deferredShadingSRVArray[i] != NULL)
			deferredShadingSRVArray[i]->Release();

		if(deferredShadingRTVArray[i] != NULL)
			deferredShadingRTVArray[i]->Release();

		if(deferredShadingTextureArray[i] != NULL)
			deferredShadingTextureArray[i]->Release();
	}
	if(deferredShadingDepthSRV != NULL)
		deferredShadingDepthSRV->Release();

	if(deferredShadingFSQVertexBuffer)
		deferredShadingFSQVertexBuffer->Release();
	if(deferredShadingFSQIndexBuffer)
		deferredShadingFSQIndexBuffer->Release();

	if(dX11DeviceContext)
		dX11DeviceContext->ClearState();

	if(dX11DepthStencilState)
		dX11DepthStencilState->Release();
	if(dX11DepthStencilView)
		dX11DepthStencilView->Release();
	if(dX11RasterState)
		dX11RasterState->Release();
	if(dX11DepthStencilBuffer)
		dX11DepthStencilBuffer->Release();
	if(dX11VertexLayout)
		dX11VertexLayout->Release();
	if(dX11RenderTargetView)
		dX11RenderTargetView->Release();
	if(dX11SwapChain)
		dX11SwapChain->Release();
	if(dX11DeviceContext)
		dX11DeviceContext->Release();
	if(dX11Device)
		dX11Device->Release();

	ComponentFactory::Cleanup();
}

HRESULT DirectX11App::ResizeWindow(HWND hWnd) {
	if(dX11SwapChain != NULL) {
		RECT rc;
		GetClientRect(hWnd, &rc);
		UINT width = rc.right - rc.left;
		UINT height = rc.bottom - rc.top;

		D3D11_VIEWPORT vp;
		vp.Width = (float)width;
		vp.Height = (float)height;
		vp.MinDepth = 0.0f;
		vp.MaxDepth = 1.0f;
		vp.TopLeftX = 0;
		vp.TopLeftY = 0;
		dX11DeviceContext->RSSetViewports(1, &vp);

		dX11DeviceContext->OMSetRenderTargets(0, 0, 0);

		if(dX11DepthStencilView)
			dX11DepthStencilView->Release();
		if(dX11DepthStencilBuffer)
			dX11DepthStencilBuffer->Release();
		if(dX11RenderTargetView)
			dX11RenderTargetView->Release();

		HRESULT hr = dX11SwapChain->ResizeBuffers(0, 0, 0, DXGI_FORMAT_UNKNOWN, 0);

		if(FAILED(hr))
			return hr;

		ID3D11Texture2D* pBuffer;
		hr = dX11SwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&pBuffer);

		if(FAILED(hr))
			return hr;

		hr = dX11Device->CreateRenderTargetView(pBuffer, NULL, &dX11RenderTargetView);

		if(FAILED(hr))
			return hr;

		pBuffer->Release();

		D3D11_TEXTURE2D_DESC depthBufferDesc;
		ZeroMemory(&depthBufferDesc, sizeof(depthBufferDesc));
		depthBufferDesc.Width = width;
		depthBufferDesc.Height = height;
		depthBufferDesc.MipLevels = 1;
		depthBufferDesc.ArraySize = 1;
		depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
		depthBufferDesc.SampleDesc.Count = 1;
		depthBufferDesc.SampleDesc.Quality = 0;
		depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
		depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
		depthBufferDesc.CPUAccessFlags = 0;
		depthBufferDesc.MiscFlags = 0;

		hr = dX11Device->CreateTexture2D(&depthBufferDesc, NULL, &dX11DepthStencilBuffer);
		if(FAILED(hr))
			return hr;

		dX11DeviceContext->OMSetDepthStencilState(dX11DepthStencilState, 1);

		D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
		ZeroMemory(&depthStencilViewDesc, sizeof(depthStencilViewDesc));
		depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
		depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
		depthStencilViewDesc.Texture2D.MipSlice = 0;

		hr = dX11Device->CreateDepthStencilView(dX11DepthStencilBuffer, &depthStencilViewDesc, &dX11DepthStencilView);
		if(FAILED(hr))
			return hr;

		dX11DeviceContext->OMSetRenderTargets(1, &dX11RenderTargetView, dX11DepthStencilView);

		mainCamera->SetAspectRatio(GetAspectRatio());
	}
	return S_OK;
}

void DirectX11App::Update(float dt) {
	Input::Update();

	if(Input::IsKeyPressed(DIK_ESCAPE))
		PostQuitMessage(0);

	if(Input::IsKeyPressed(DIK_A) || Input::IsKeyPressed(DIK_LEFT))
		moveTrans->LocalRotate(0.0f, -CameraRotateSpeed * dt, 0.0f, true);
	if(Input::IsKeyPressed(DIK_D) || Input::IsKeyPressed(DIK_RIGHT))
		moveTrans->LocalRotate(0.0f, CameraRotateSpeed * dt, 0.0f, true);
	if(Input::IsKeyPressed(DIK_S) || Input::IsKeyPressed(DIK_DOWN))
		moveTrans->LocalTrack(-CameraMoveSpeed * dt);
	if(Input::IsKeyPressed(DIK_W) || Input::IsKeyPressed(DIK_UP))
		moveTrans->LocalTrack(CameraMoveSpeed * dt);
	if(Input::IsKeyPressed(DIK_Q))
		moveTrans->LocalStrafe(-CameraMoveSpeed * dt);
	if(Input::IsKeyPressed(DIK_E))
		moveTrans->LocalStrafe(CameraMoveSpeed * dt);
	if(Input::IsKeyPressed(DIK_R))
		cameraControl->SetLocalRotation(45.0f, 0, 0, true);

	if(Input::IsKeyPressed(DIK_L))
		drawLines = true;
	if(Input::IsKeyPressed(DIK_K))
		drawLines = false;

	if(Input::IsKeyPressed(DIK_0)) {
		if(textureToRender != 0) {
			delete s1;
			s1 = new Sentence("Deferred Render: Fully Lit", XMFLOAT2(5.0f, 5.0f), 0.5f, f, -5.0f);
			s1->Build(dX11Device);

			textureToRender = 0;
		}
	}
	if(Input::IsKeyPressed(DIK_1)) {
		if(textureToRender != 1) {
			delete s1;
			s1 = new Sentence("Deferred Render: Diffuse Texture", XMFLOAT2(5.0f, 5.0f), 0.5f, f, -5.0f);
			s1->Build(dX11Device);

			textureToRender = 1;
		}
	}
	if(Input::IsKeyPressed(DIK_2)) {
		if(textureToRender != 2) {
			delete s1;
			s1 = new Sentence("Deferred Render: Normals", XMFLOAT2(5.0f, 5.0f), 0.5f, f, -5.0f);
			s1->Build(dX11Device);

			textureToRender = 2;
		}
	}
	if(Input::IsKeyPressed(DIK_3)) {
		if(textureToRender != 3) {
			delete s1;
			s1 = new Sentence("Deferred Render: View Position", XMFLOAT2(5.0f, 5.0f), 0.5f, f, -5.0f);
			s1->Build(dX11Device);

			textureToRender = 3;
		}
	}
	if(Input::IsKeyPressed(DIK_4)) {
		if(textureToRender != 4) {
			delete s1;
			s1 = new Sentence("Deferred Render: Depth Buffer", XMFLOAT2(5.0f, 5.0f), 0.5f, f, -5.0f);
			s1->Build(dX11Device);

			textureToRender = 4;
		}
	}
	if(Input::IsKeyPressed(DIK_5)) {
		if(textureToRender != 5) {
			delete s1;
			s1 = new Sentence("Deferred Render: Depth From Tex", XMFLOAT2(5.0f, 5.0f), 0.5f, f, -5.0f);
			s1->Build(dX11Device);

			textureToRender = 5;
		}
	}
	if(Input::IsKeyPressed(DIK_6)) {
		if(textureToRender != 6) {
			delete s1;
			s1 = new Sentence("Deferred Render: Specular", XMFLOAT2(5.0f, 5.0f), 0.5f, f, -5.0f);
			s1->Build(dX11Device);

			textureToRender = 6;
		}
	}
	if(Input::IsKeyPressed(DIK_7)) {
		if(textureToRender != 7) {
			delete s1;
			s1 = new Sentence("Deferred Render: Seven", XMFLOAT2(5.0f, 5.0f), 0.5f, f, -5.0f);
			s1->Build(dX11Device);

			textureToRender = 7;
		}
	}
	if(Input::IsKeyPressed(DIK_8)) {
		if(textureToRender != 8) {
			delete s1;
			s1 = new Sentence("Deferred Render: Eight", XMFLOAT2(5.0f, 5.0f), 0.5f, f, -5.0f);
			s1->Build(dX11Device);

			textureToRender = 8;
		}
	}
	if(Input::IsKeyPressed(DIK_9)) {
		if(textureToRender != 9) {
			delete s1;
			s1 = new Sentence("Deferred Render: Nine", XMFLOAT2(5.0f, 5.0f), 0.5f, f, -5.0f);
			s1->Build(dX11Device);

			textureToRender = 9;
		}
	}

	if(Input::IsKeyPressed(DIK_O)) {
		if(!doDeferredRender) {
			delete s1;
			s1 = new Sentence("Deferred Render: Fully Lit", XMFLOAT2(5.0f, 5.0f), 0.5f, f, -5.0f);
			s1->Build(dX11Device);

			doDeferredRender = true;
			textureToRender = 0;

			pass.Deactivate(dX11DeviceContext);
		}
	}
	if(Input::IsKeyPressed(DIK_P)) {
		if(doDeferredRender) {
			delete s1;
			s1 = new Sentence("Forward Render: Diffuse Texture", XMFLOAT2(5.0f, 5.0f), 0.5f, f, -5.0f);
			s1->Build(dX11Device);

			doDeferredRender = false;

			dsPassOne.Deactivate(dX11DeviceContext);
			dsPassTwo.Deactivate(dX11DeviceContext);
		}
	}
	if(Input::IsKeyPressed(DIK_T))
		positionFrom = 0;
	else if(Input::IsKeyPressed(DIK_G))
		positionFrom = 1;

	long mouseDX = Input::MouseDeltaX(), mouseDY = Input::MouseDeltaY(), wheelDelta = Input::MouseDWheel();;
	if(Input::IsMouseButtonPressed(0) && (mouseDX != 0 || mouseDY != 0)) {
		cameraControl->LocalRotate(0, mouseDX * 0.1f, 0, true);
		cameraControl->LocalRotate(mouseDY * 0.1f, 0, 0, true);
	}
	if(wheelDelta != 0)
		cameraTrans->LocalMove(0, 0, wheelDelta * 0.01f);

	sg.Update(dt);
}

void DirectX11App::Render() {
	if(drawLines)
		dX11DeviceContext->RSSetState(dX11WireframeState);
	else
		dX11DeviceContext->RSSetState(dX11RasterState);

	dX11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	if(doDeferredRender)
		deferredRender();
	else
		forwardRender();

	fontRender();

	dX11SwapChain->Present(0, 0);
}

void DirectX11App::forwardRender() {
	dX11DeviceContext->ClearRenderTargetView(dX11RenderTargetView, ClearColour);
	dX11DeviceContext->ClearDepthStencilView(dX11DepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);

	dX11DeviceContext->OMSetRenderTargets(1, &dX11RenderTargetView, dX11DepthStencilView);

	pass.Activate(dX11DeviceContext);

	dX11DeviceContext->PSSetSamplers(0, 1, &anisoSamplerState);

	bool ret = true;
	ret = pass.GetVertexShader()->SetVariable("View", mainCamera->GetViewPtr());
	ret = pass.GetVertexShader()->SetVariable("Projection", mainCamera->GetProjectionPtr());

	sg.Draw(dX11DeviceContext, &pass, mainCamera);
}

void DirectX11App::deferredRender() {
	bool ret = true;
	float blendFactor[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
	UINT sampleMask = 0xffffffff;

	for(unsigned int i = 0; i < numberOfRenderTargets; i++)
		dX11DeviceContext->ClearRenderTargetView(deferredShadingRTVArray[i], ClearColour);
	dX11DeviceContext->ClearDepthStencilView(deferredShadingDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);

	dX11DeviceContext->OMSetRenderTargets(numberOfRenderTargets, deferredShadingRTVArray, deferredShadingDepthStencilView);

	dX11DeviceContext->OMSetBlendState(dX11BlendDisabledState, blendFactor, sampleMask);

	dsPassOne.Activate(dX11DeviceContext);

	dX11DeviceContext->PSSetSamplers(0, 1, &anisoSamplerState);

	ret = dsPassOne.GetVertexShader()->SetVariable("View", mainCamera->GetViewPtr());
	ret = dsPassOne.GetVertexShader()->SetVariable("Projection", mainCamera->GetProjectionPtr());

	sg.Draw(dX11DeviceContext, &dsPassOne, mainCamera);

	dsPassOne.Deactivate(dX11DeviceContext);

	dX11DeviceContext->ClearRenderTargetView(dX11RenderTargetView, ClearColour);
	dX11DeviceContext->ClearDepthStencilView(dX11DepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
	dX11DeviceContext->RSSetState(dX11RasterState);
	dX11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	dX11DeviceContext->OMSetRenderTargets(1, &dX11RenderTargetView, dX11DepthStencilView);

	dsPassTwo.Activate(dX11DeviceContext);

	ret = dsPassTwo.GetVertexShader()->SetVariable("View", mainCamera->GetViewPtr());
	ret = dsPassTwo.GetVertexShader()->SetVariable("Projection", mainCamera->GetProjectionPtr());

	ret = dsPassTwo.GetPixelShader()->SetVariable("ProjectionInverse", mainCamera->GetInverseProjectionPtr());
	ret = dsPassTwo.GetPixelShader()->SetVariable("ViewInverse", mainCamera->GetInverseViewPtr());
	ret = dsPassTwo.GetPixelShader()->SetVariable("ViewProjectionInverse", mainCamera->GetInverseViewProjectionPtr());
	ret = dsPassTwo.GetPixelShader()->SetVariable("CameraProjection", mainCamera->GetProjectionPtr());
	ret = dsPassTwo.GetPixelShader()->SetVariable("ToRender", &textureToRender);
	ret = dsPassTwo.GetPixelShader()->SetVariable("PosFrom", &positionFrom);
	ret = dsPassTwo.GetPixelShader()->SetVariable("CameraPosition", mainCamera->GetEyePtr());
	dsPassTwo.GetPixelShader()->SetShaderData(dX11DeviceContext);

	dX11DeviceContext->PSSetShaderResources(0, numberOfRenderTargets, deferredShadingSRVArray);
	dX11DeviceContext->PSSetShaderResources(numberOfRenderTargets, 1, &deferredShadingDepthSRV);

	unsigned int stride = sizeof(FSQVertex), offset = 0;
	dX11DeviceContext->IASetVertexBuffers(0, 1, &deferredShadingFSQVertexBuffer, &stride, &offset);
	dX11DeviceContext->IASetIndexBuffer(deferredShadingFSQIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	dX11DeviceContext->OMSetDepthStencilState(dX11DepthStencilDisabledState, 1);
	dX11DeviceContext->OMSetBlendState(dX11DeferredRenderingState, blendFactor, sampleMask);

	for(LightComponent * x : *lights) {
		if(x->GetEnabled()) {
			int lightType = x->GetLightType();
			ret = dsPassTwo.GetPixelShader()->SetVariable("LightPosition", x->GetPositionPtr());
			ret = dsPassTwo.GetPixelShader()->SetVariable("LightDiffuse", x->GetDiffusePtr());
			ret = dsPassTwo.GetPixelShader()->SetVariable("LightSpecular", x->GetSpecularPtr());
			ret = dsPassTwo.GetPixelShader()->SetVariable("LightType", &lightType);
			ret = dsPassTwo.GetPixelShader()->SetVariable("LightDirection", x->GetDirectionPtr());

			if(lightType == 1)
				ret = dsPassTwo.GetPixelShader()->SetVariable("LightRadius", x->GetLightRadiusPtr());
			else if(lightType == 2) {
				ret = dsPassTwo.GetPixelShader()->SetVariable("LightRadius", x->GetLightRadiusPtr());
				ret = dsPassTwo.GetPixelShader()->SetVariable("LightConeAngleCosine", x->GetLightConeAngleCosinePtr());
				ret = dsPassTwo.GetPixelShader()->SetVariable("LightSpotDecay", x->GetLightSpotDecayPtr());
			}

			dsPassTwo.PreDraw(dX11DeviceContext);
			dX11DeviceContext->DrawIndexed(6, 0, 0);
		}
	}

	dX11DeviceContext->OMSetDepthStencilState(dX11DepthStencilState, 1);

	ID3D11ShaderResourceView * reset[1] = { NULL };
	for(unsigned int i = 0; i < numberOfRenderTargets; i++)
		dX11DeviceContext->PSSetShaderResources(i, 1, reset);
	dX11DeviceContext->PSSetShaderResources(numberOfRenderTargets, 1, reset);

	dsPassTwo.Deactivate(dX11DeviceContext);
}

void DirectX11App::fontRender() {
	float blendFactor[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
	UINT sampleMask = 0xffffffff;
	dX11DeviceContext->OMSetDepthStencilState(dX11DepthStencilDisabledState, 1);
	dX11DeviceContext->OMSetBlendState(dX11BlendState, blendFactor, sampleMask);
	dX11DeviceContext->RSSetState(dX11RasterState);
	dX11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	fontPass.Activate(dX11DeviceContext);

	dX11DeviceContext->PSSetSamplers(0, 1, &anisoSamplerState);

	bool ret = true;
	ret = fontPass.GetVertexShader()->SetVariable("Ortho", mainCamera->GetOrthographicPtr());
	fontPass.GetVertexShader()->SetShaderData(dX11DeviceContext);

	s1->Draw(dX11DeviceContext, &fontPass);
	s2->Draw(dX11DeviceContext, &fontPass);

	fontPass.Deactivate(dX11DeviceContext);

	dX11DeviceContext->OMSetBlendState(dX11BlendDisabledState, blendFactor, sampleMask);
	dX11DeviceContext->OMSetDepthStencilState(dX11DepthStencilState, 1);
}

unsigned long DirectX11App::GetWidth() const {
	return width;
}

unsigned long DirectX11App::GetHeight() const {
	return height;
}

float DirectX11App::GetAspectRatio() const {
	return (float)width / (float)height;
}

ID3D11Device * DirectX11App::GetDevice() const {
	return dX11Device;
}

ID3D11DeviceContext * DirectX11App::GetDeviceContext() const {
	return dX11DeviceContext;
}

DirectX11App * DirectX11App::current = NULL;

DirectX11App * DirectX11App::Current() {
	return current;
}