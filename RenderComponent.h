#ifndef RENDER_COMPONENT_H
#define RENDER_COMPONENT_H

#include "Component.h"
#include "TransformComponent.h"
#include "Includes.h"
#include "Mesh.h"
#include "ComponentRegistrar.h"

using namespace tinyxml2;

class RenderComponent : public Component {
private:
	TransformComponent * transform;

	RenderComponent();

public:
	Mesh * mesh;
	~RenderComponent();

	bool Initialise(XMLElement * init);

	void Start();
	void Shutdown();
	void Draw(ID3D11DeviceContext * dX11DeviceContext, Pass * pass, CameraComponent * currentCam);

	string GetTypename() const;

	SetupComponentType(RenderComponent);
};

DeclareType(RenderComponent);

#endif