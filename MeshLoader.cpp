#include "MeshLoader.h"
#include "Mesh.h"
#include "AnimatedMesh.h"
#include "Material.h"
#include "DXHelperStructs.h"
#include "DirectX11App.h"
#include <iostream>
#include <fstream>
#include <sstream>

HRESULT MeshLoader::LoadMD5Anim(AnimatedMesh *& mesh, string const & animName, string const & fileName, unsigned int animNum, float scale) {
	if(fileName.length() > 8 && animName != "" && scale > 0.0f) {
		size_t found = fileName.find_last_of('/');
		string path = fileName.substr(0, found + 1);

		ifstream md5AnimFile(fileName);
		if(md5AnimFile.is_open()) {
			string line = "";
			float ** frameChanges = NULL;
			Animation & anim = mesh->animations[animNum];
			Joint * baseJoints = NULL;
			unsigned int numAnimatedComponents = 0, currentFrame = 0, currentJoint = 0, currentFrameChange = 0, currentBB = 0;
			bool inJoints = false, inBounds = false, inBaseFrame = false, inFrame = false;

			anim.Name = animName;

			while(!md5AnimFile.bad() && !md5AnimFile.eof()) {
				md5AnimFile >> line;

				if(line == "MD5Version") {
					int md5Version = 0;
					md5AnimFile >> md5Version;

					if(md5Version != 10)
						return E_FAIL;
				} else if(line == "numFrames") {
					md5AnimFile >> anim.NumberOfFrames;

					anim.FrameJoints = new Joint *[anim.NumberOfFrames];
					frameChanges = new float *[anim.NumberOfFrames];
					anim.BoundingBoxes = new BoundingBox[anim.NumberOfFrames];

					getline(md5AnimFile, line);
				} else if(line == "numJoints") {
					md5AnimFile >> anim.NumberOfJoints;

					for(unsigned int i = 0; i < anim.NumberOfFrames; i++)
						anim.FrameJoints[i] = new Joint[anim.NumberOfJoints];

					baseJoints = new Joint[anim.NumberOfJoints];

					getline(md5AnimFile, line);
				} else if(line == "frameRate") {
					md5AnimFile >> anim.FrameRate;
					getline(md5AnimFile, line);
				} else if(line == "numAnimatedComponents") {
					md5AnimFile >> numAnimatedComponents;

					for(unsigned int i = 0; i < anim.NumberOfFrames; i++)
						frameChanges[i] = new float[numAnimatedComponents];

					getline(md5AnimFile, line);
				} else if(line == "hierarchy") {
					inJoints = true;
					getline(md5AnimFile, line);
				} else if(line == "bounds") {
					inBounds = true;
					getline(md5AnimFile, line);
				} else if(line == "baseframe") {
					inBaseFrame = true;
					getline(md5AnimFile, line);
				} else if(line == "frame") {
					inFrame = true;
					getline(md5AnimFile, line);
				} else if(line == "}") {
					inJoints = false;
					inBounds = false;
					inBaseFrame = false;

					if(inFrame) {
						for(unsigned int i = 0; i < anim.NumberOfJoints; i++) {
							int k = 0;

							Joint tempJoint;
							tempJoint.Name = baseJoints[i].Name;
							tempJoint.Flag = baseJoints[i].Flag;
							tempJoint.StartID = baseJoints[i].StartID;
							tempJoint.ParentID = baseJoints[i].ParentID;
							tempJoint.Position = XMFLOAT3(0.0f, 0.0f, 0.0f);
							tempJoint.Orientation = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);

							if(tempJoint.Flag & 1)
								tempJoint.Position.x = frameChanges[currentFrame][tempJoint.StartID + k] * scale;

							if(tempJoint.Flag & 2)
								tempJoint.Position.z = frameChanges[currentFrame][tempJoint.StartID + k + 1] * scale;

							if(tempJoint.Flag & 4)
								tempJoint.Position.y = frameChanges[currentFrame][tempJoint.StartID + k + 2] * scale;

							if(tempJoint.Flag & 8)
								tempJoint.Orientation.x = frameChanges[currentFrame][tempJoint.StartID + k + 3];

							if(tempJoint.Flag & 16)
								tempJoint.Orientation.z = frameChanges[currentFrame][tempJoint.StartID + k + 4];

							if(tempJoint.Flag & 32)
								tempJoint.Orientation.y = frameChanges[currentFrame][tempJoint.StartID + k + 5];

							k += 6;

							float t = 1.0f - (tempJoint.Orientation.x * tempJoint.Orientation.x) - (tempJoint.Orientation.y * tempJoint.Orientation.y) - (tempJoint.Orientation.z * tempJoint.Orientation.z);
							tempJoint.Orientation.w = (t < 0.0f) ? 0.0f : -sqrtf(t);

							if(tempJoint.ParentID >= 0) {
								Joint parentJoint = anim.FrameJoints[currentFrame][tempJoint.ParentID];

								XMVECTOR outParentOrient = XMLoadFloat4(&parentJoint.Orientation);
								XMVECTOR outTempPos = XMVectorSet(tempJoint.Position.x, tempJoint.Position.y, tempJoint.Position.z, 0.0f);
								XMVECTOR outParentOrientConj = XMQuaternionConjugate(outParentOrient);
								XMVECTOR outParentPos = XMLoadFloat3(&parentJoint.Position);
								XMVECTOR outRotatedPos = XMQuaternionMultiply(XMQuaternionMultiply(outParentOrient, outTempPos), outParentOrientConj);
								outRotatedPos += outParentPos;
								XMStoreFloat3(&anim.FrameJoints[currentFrame][i].Position, outRotatedPos);

								XMVECTOR outJointOrient = XMLoadFloat4(&tempJoint.Orientation);
								outJointOrient = XMQuaternionMultiply(outParentOrient, outJointOrient);
								outJointOrient = XMQuaternionNormalize(outJointOrient);
								XMStoreFloat4(&anim.FrameJoints[currentFrame][i].Orientation, outJointOrient);
							} else {
								anim.FrameJoints[currentFrame][i].Position = tempJoint.Position;
								anim.FrameJoints[currentFrame][i].Orientation = tempJoint.Orientation;
							}
						}

						inFrame = false;
						currentFrame++;
					}

					currentJoint = 0;
					currentBB = 0;
					currentFrameChange = 0;
				} else if(inJoints) {
					if(line[line.size() - 1] != '"') {
						bool jointNameFound = false;
						while(!jointNameFound) {
							char checkChar = (char)md5AnimFile.get();

							if(checkChar == '"')
								jointNameFound = true;

							line += checkChar;
						}
					}

					string name = line.substr(1, line.length() - 2);

					int parentID = 0;
					unsigned int flag = 0, startID = 0;
					md5AnimFile >> parentID;
					md5AnimFile >> flag;
					md5AnimFile >> startID;

					bool jointMatchFound = false;
					for(unsigned int i = 0; i < mesh->numJoints; i++) {
						if(mesh->joints[i].Name == name) {
							if(mesh->joints[i].ParentID == parentID) {
								jointMatchFound = true;
								baseJoints[currentJoint].Name = name;
								baseJoints[currentJoint].ParentID = parentID;
								baseJoints[currentJoint].Flag = flag;
								baseJoints[currentJoint].StartID = startID;
							}

							break;
						}
					}

					if(!jointMatchFound)
						return E_FAIL;

					currentJoint++;
					getline(md5AnimFile, line);
				} else if(inBounds) {
					md5AnimFile >> anim.BoundingBoxes[currentBB].minX >> anim.BoundingBoxes[currentBB].minZ >> anim.BoundingBoxes[currentBB].minY;
					md5AnimFile >> line >> line;
					md5AnimFile >> anim.BoundingBoxes[currentBB].maxX >> anim.BoundingBoxes[currentBB].maxZ >> anim.BoundingBoxes[currentBB].maxY;
					md5AnimFile >> line;

					anim.BoundingBoxes[currentBB].minX *= scale;
					anim.BoundingBoxes[currentBB].minY *= scale;
					anim.BoundingBoxes[currentBB].minZ *= scale;

					anim.BoundingBoxes[currentBB].maxX *= scale;
					anim.BoundingBoxes[currentBB].maxY *= scale;
					anim.BoundingBoxes[currentBB].maxZ *= scale;

					currentBB++;
				} else if(inBaseFrame) {
					md5AnimFile >> baseJoints[currentJoint].Position.x >> baseJoints[currentJoint].Position.z >> baseJoints[currentJoint].Position.y;
					md5AnimFile >> line >> line;
					md5AnimFile >> baseJoints[currentJoint].Orientation.x >> baseJoints[currentJoint].Orientation.z >> baseJoints[currentJoint].Orientation.y;
					md5AnimFile >> line;

					baseJoints[currentJoint].Position = DXHelper::Multiply(baseJoints[currentJoint].Position, scale);

					currentJoint++;
					getline(md5AnimFile, line);
				} else if(inFrame) {
					frameChanges[currentFrame][currentFrameChange] = stof(line.c_str());
					currentFrameChange++;
				} else
					getline(md5AnimFile, line);
			}

			for(unsigned int i = 0; i < anim.NumberOfFrames; i++)
				delete[] frameChanges[i];

			delete[] frameChanges;

			delete[] baseJoints;

			anim.AnimationFrameLength = 1.0f / anim.FrameRate;
			anim.AnimationLength = anim.NumberOfFrames * anim.AnimationFrameLength;

			return S_OK;
		} else
			return E_FAIL;
	} else
		return E_FAIL;
}

HRESULT MeshLoader::LoadMD5Mesh(Mesh *& outMesh, string const & fileName, float scale, XMLElement * meshConfig) {
	size_t found = fileName.find_last_of('/');
	string path = fileName.substr(0, found + 1);

	ifstream md5File(fileName);

	if(md5File.is_open() && meshConfig->FirstChild() != NULL) {
		unsigned int numAnims = 0;
		if(meshConfig->QueryUnsignedAttribute("numAnimations", &numAnims) != XML_SUCCESS)
			return E_FAIL;

		outMesh = new AnimatedMesh();
		AnimatedMesh * animMesh = (AnimatedMesh *)outMesh;

		string line = "", ** textures = NULL;
		unsigned int numJoints = 0, currentJoint = 0, currentVert = 0, totalNumVerts = 0, currentTri = 0, totalNumTris = 0, currentWeight = 0, totalNumWeights = 0, numMeshes = 0, currentMesh = 0;
		Joint * joints = NULL;
		Weight ** weights = NULL;
		MD5Vertex ** md5Vertices = NULL;
		Vertex ** vertices = NULL;
		unsigned int ** indices = NULL, *numVerts = NULL, *numTris = NULL, *numWeights = NULL;
		bool inJoints = false, inMesh = false, *diffuse = NULL, *normal = NULL, *spec = NULL;

		while(md5File.good() && !md5File.eof()) {
			md5File >> line;

			if(line == "MD5Version") {
				int md5Version = 0;
				md5File >> md5Version;

				if(md5Version != 10)
					return E_FAIL;
			} else if(line == "numJoints") {
				md5File >> numJoints;

				joints = new Joint[numJoints];

				getline(md5File, line);
			} else if(line == "numMeshes") {
				md5File >> numMeshes;

				weights = new Weight *[numMeshes];
				md5Vertices = new MD5Vertex *[numMeshes];
				vertices = new Vertex *[numMeshes];
				indices = new unsigned int *[numMeshes];
				textures = new string *[numMeshes];
				numVerts = new unsigned int[numMeshes];
				numTris = new unsigned int[numMeshes];
				numWeights = new unsigned int[numMeshes];
				currentMesh = 0;

				for(unsigned int i = 0; i < numMeshes; i++)
					textures[i] = new string[3];

				diffuse = new bool[numMeshes];
				normal = new bool[numMeshes]; 
				spec = new bool[numMeshes];

				ZeroMemory(diffuse, sizeof(bool) * numMeshes);
				ZeroMemory(normal, sizeof(bool) * numMeshes);
				ZeroMemory(spec, sizeof(bool) * numMeshes);

				getline(md5File, line);
			} else if(line == "joints") {
				inJoints = true;
				getline(md5File, line);
			} else if(line == "mesh") {
				inMesh = true;
				getline(md5File, line);
			} else if(line == "shader") {
				string file = "";
				md5File >> file;
				textures[currentMesh][0] = path + file.substr(1, file.length() - 2);
				diffuse[currentMesh] = true;
				getline(md5File, line);
			} else if(line == "shader_normal") {
				string file = "";
				md5File >> file;
				textures[currentMesh][1] = path + file.substr(1, file.length() - 2);
				normal[currentMesh] = true;
				getline(md5File, line);
			} else if(line == "shader_specular") {
				string file = "";
				md5File >> file;
				textures[currentMesh][2] = path + file.substr(1, file.length() - 2);
				spec[currentMesh] = true;
				getline(md5File, line);
			} else if(line == "numverts") {
				md5File >> numVerts[currentMesh];

				md5Vertices[currentMesh] = new MD5Vertex[numVerts[currentMesh]];
				vertices[currentMesh] = new Vertex[numVerts[currentMesh]];
				totalNumVerts += numVerts[currentMesh];

				getline(md5File, line);
			} else if(line == "vert") {
				float u, v;
				md5File >> line >> line >> u >> v >> line >> md5Vertices[currentMesh][currentVert].WeightStartID >> md5Vertices[currentMesh][currentVert].WeightCount;
				vertices[currentMesh][currentVert].TexCoord = XMFLOAT2(u, v);

				getline(md5File, line);
				currentVert++;
			} else if(line == "numtris") {
				md5File >> numTris[currentMesh];
				numTris[currentMesh] *= 3;

				indices[currentMesh] = new unsigned int[numTris[currentMesh]];
				totalNumTris += numTris[currentMesh];

				getline(md5File, line);
			} else if(line == "tri") {
				md5File >> line >> indices[currentMesh][currentTri] >> indices[currentMesh][currentTri + 1] >> indices[currentMesh][currentTri + 2];

				getline(md5File, line);
				currentTri += 3;
			} else if(line == "numweights") {
				md5File >> numWeights[currentMesh];

				weights[currentMesh] = new Weight[numWeights[currentMesh]];
				totalNumWeights += numWeights[currentMesh];

				getline(md5File, line);
			} else if(line == "weight") {
				float x, y, z;
				md5File >> line >> weights[currentMesh][currentWeight].JointID >> weights[currentMesh][currentWeight].Influence >> line >> x >> z >> y;
				weights[currentMesh][currentWeight].LocalPosition = DXHelper::Multiply(XMFLOAT3(x, y, z), scale);

				getline(md5File, line);
				currentWeight++;
			} else if(line == "}") {
				inJoints = false;
				if(inMesh) {
					for(unsigned int i = 0; i < numVerts[currentMesh]; i++) {
						MD5Vertex tempVert = md5Vertices[currentMesh][i];
						XMVECTOR outVertPos = XMVectorZero();

						for(unsigned int j = 0; j < tempVert.WeightCount; j++) {
							Weight tempWeight = weights[currentMesh][tempVert.WeightStartID + j];
							Joint tempJoint = joints[tempWeight.JointID];

							XMVECTOR outJointPos = XMLoadFloat3(&tempJoint.Position);
							XMVECTOR outJointOrient = XMLoadFloat4(&tempJoint.Orientation);
							XMVECTOR outWeightPos = XMVectorSet(tempWeight.LocalPosition.x, tempWeight.LocalPosition.y, tempWeight.LocalPosition.z, 0.0f);
							XMVECTOR outOrientConj = XMQuaternionConjugate(outJointOrient);

							XMVECTOR outRotPoint = XMQuaternionMultiply(XMQuaternionMultiply(outJointOrient, outWeightPos), outOrientConj);
							outVertPos += (outJointPos + outRotPoint) * tempWeight.Influence;
						}

						XMStoreFloat3(&vertices[currentMesh][i].Position, outVertPos);
					}

					XMFLOAT3 * faceNormals = new XMFLOAT3[numTris[currentMesh] / 3];
					for(unsigned int i = 0, normalI = 0; i < numTris[currentMesh]; i += 3, normalI++) {
						XMFLOAT3 edge1 = DXHelper::Subtract(vertices[currentMesh][indices[currentMesh][i]].Position, vertices[currentMesh][indices[currentMesh][i + 2]].Position);
						XMFLOAT3 edge2 = DXHelper::Subtract(vertices[currentMesh][indices[currentMesh][i + 2]].Position, vertices[currentMesh][indices[currentMesh][i + 1]].Position);

						faceNormals[normalI] = DXHelper::Cross(edge1, edge2);
					}

					for(unsigned int i = 0; i < numVerts[currentMesh]; ++i) {
						XMFLOAT3 normalSum = { 0.0f, 0.0f, 0.0f };
						int facesUsing = 0;

						for(unsigned int j = 0, normalI = 0; j < numTris[currentMesh]; j += 3, normalI++) {
							if(indices[currentMesh][j] == i || indices[currentMesh][j + 1] == i || indices[currentMesh][j + 2] == i) {
								normalSum = DXHelper::Add(normalSum, faceNormals[normalI]);
								facesUsing++;
							}
						}

						normalSum = DXHelper::Divide(normalSum, (float)facesUsing);

						XMVECTOR outNormal = XMLoadFloat3(&normalSum);
						outNormal = XMVector3Normalize(outNormal);
						XMStoreFloat3(&normalSum, outNormal);

						vertices[currentMesh][i].Normal = XMFLOAT3(-normalSum.x, -normalSum.y, -normalSum.z);

						for(unsigned int j = 0; j < md5Vertices[currentMesh][i].WeightCount; j++) {
							Joint tempJoint = joints[weights[currentMesh][md5Vertices[currentMesh][i].WeightStartID + j].JointID];
							XMVECTOR jointOrientation = XMLoadFloat4(&tempJoint.Orientation);
							XMVECTOR normal = XMQuaternionMultiply(XMQuaternionMultiply(XMQuaternionInverse(jointOrientation), outNormal), jointOrientation);
							normal = XMVector3Normalize(normal);

							XMStoreFloat3(&weights[currentMesh][md5Vertices[currentMesh][i].WeightStartID + j].Normal, normal);
						}
					}

					inMesh = false;
					currentMesh++;
					currentVert = 0;
					currentTri = 0;
					currentWeight = 0;
				}
			} else if(inJoints) {
				if(line[line.size() - 1] != '"') {
					bool jointNameFound = false;
					while(!jointNameFound) {
						char checkChar = (char)md5File.get();

						if(checkChar == '"')
							jointNameFound = true;

						line += checkChar;
					}
				}

				joints[currentJoint].Name = line.substr(1, line.length() - 2);

				float x, y, z;
				joints[currentJoint].ID = currentJoint;
				md5File >> joints[currentJoint].ParentID >> line >> x >> z >> y;
				joints[currentJoint].Position = DXHelper::Multiply(XMFLOAT3(x, y, z), scale);

				md5File >> line >> line >> x >> z >> y;
				float t = 1.0f - (x * x) - (y * y) - (z * z);
				joints[currentJoint].Orientation = XMFLOAT4(x, y, z, (t < 0.0f) ? 0.0f : -sqrtf(t));

				getline(md5File, line);

				currentJoint++;
			} else
				getline(md5File, line);
		}

		animMesh->numSubObjs = numMeshes;
		animMesh->subObjects = new SubObject[numMeshes];

		animMesh->numMats = numMeshes;
		animMesh->materials = new Material[numMeshes];
		bool loadFail = false;

		for(unsigned int i = 0; i < numMeshes; i++) {
			animMesh->materials[i] = Material();
			animMesh->materials[i].SetName(textures[i][0]);

			if(diffuse[i] && !animMesh->materials[i].LoadTexture("Diffuse", textures[i][0])) {
				loadFail = true;
				break;
			}

			if(normal[i] && !animMesh->materials[i].LoadTexture("Normal", textures[i][1])) {
				loadFail = true;
				break;
			}

			if(spec[i] && !animMesh->materials[i].LoadTexture("Specular", textures[i][2])) {
				loadFail = true;
				break;
			}
		}

		if(!loadFail) {
			animMesh->vertices = new Vertex[totalNumVerts];
			animMesh->md5Vertices = new MD5Vertex[totalNumVerts];
			animMesh->numVerts = totalNumVerts;
			animMesh->indices = new unsigned int[totalNumTris];
			animMesh->numInds = totalNumTris;
			animMesh->weights = new Weight[totalNumWeights];
			animMesh->numWeights = totalNumWeights;

			animMesh->joints = new Joint[numJoints];
			animMesh->numJoints = numJoints;
			memcpy(animMesh->joints, joints, sizeof(Joint) * numJoints);

			unsigned int offsetV = 0, offsetI = 0, offsetW = 0;
			for(unsigned int i = 0; i < numMeshes; i++) {
				animMesh->subObjects[i] = SubObject(animMesh->materials[i].GetName(), offsetI, numTris[i]);
				animMesh->subObjects[i].material = &(animMesh->materials[i]);

				memcpy(animMesh->vertices + offsetV, vertices[i], sizeof(Vertex) * numVerts[i]);

				memcpy(animMesh->weights + offsetW, weights[i], sizeof(Weight) * numWeights[i]);

				for(unsigned int j = 0; j < numVerts[i]; j++) {
					animMesh->md5Vertices[offsetV + j].WeightStartID = (md5Vertices[i][j].WeightStartID + offsetW);
					animMesh->md5Vertices[offsetV + j].WeightCount = md5Vertices[i][j].WeightCount;
				}

				for(unsigned int j = 0; j < numTris[i]; j++)
					animMesh->indices[offsetI + j] = (indices[i][j] + offsetV);

				offsetV += numVerts[i];
				offsetI += numTris[i];
				offsetW += numWeights[i];
			}

			animMesh->CalculateAndAssignTangents();

			animMesh->numAnimations = numAnims;
			animMesh->animations = new Animation[numAnims];

			XMLElement * animConfig = meshConfig->FirstChildElement("Animation");
			unsigned int i = 0;
			while(animConfig != NULL && i < numAnims) {
				string filename = animConfig->Attribute("src"), name = animConfig->Attribute("name");

				HRESULT hr = LoadMD5Anim(animMesh, name, path + filename, i, scale);
				if(FAILED(hr)) {
					loadFail = true;
					break;
				}

				animConfig = animConfig->NextSiblingElement("Animation");
			}

			if(!loadFail)
				animMesh->PrepareAnimations();
		}

		if(loadFail) {
			delete animMesh;
			animMesh = NULL;
		}

		delete[] joints;
		delete[] numVerts;
		delete[] numTris;
		delete[] numWeights;

		for(unsigned int i = 0; i < numMeshes; i++) {
			delete[] textures[i];
			delete[] weights[i];
			delete[] md5Vertices[i];
			delete[] vertices[i];
			delete[] indices[i];
		}

		delete[] textures;
		delete[] weights;
		delete[] md5Vertices;
		delete[] vertices;
		delete[] indices;
		delete[] diffuse;
		delete[] normal;
		delete[] spec;

		return loadFail ? E_FAIL : S_OK;
	} else
		return E_FAIL;
}

HRESULT MeshLoader::LoadFromMTL(Mesh * m, string const & path, string const & fileName) {
	if(m != NULL && !fileName.empty() && (fileName.find(".mtl") != string::npos || fileName.find(".MTL") != string::npos)) {
		string line = "";
		ifstream matFile(path + fileName);
		unsigned int numMaterials = 0, *numTextures = NULL;
		if(matFile.is_open()) {
			while(matFile.good() && !matFile.eof()) {
				getline(matFile, line);
				if(line.length() > 0 && line[0] != '#' && line.find("newmtl ") != string::npos)
					numMaterials++;
			}

			numTextures = new unsigned int[numMaterials];
			for(unsigned int i = 0; i < numMaterials; i++)
				numTextures[i] = 0;

			matFile.close();
			matFile.open(path + fileName);

			int currMat = -1;
			bool foundBump = false, foundNorm = false, foundSpec = false;
			while(matFile.good() && !matFile.eof()) {
				getline(matFile, line);
				if(line.length() > 0 && line[0] != '#') {
					if(line.find("newmtl ") != string::npos)
						currMat++;
					else if(line.find("map_Ka ") != string::npos)
						numTextures[currMat]++;
					else if(line.find("map_Kd ") != string::npos)
						numTextures[currMat]++;
					else if(line.find("map_bump ") != string::npos && !foundBump) {
						numTextures[currMat]++;
						foundBump = true;
					} else if(line.find("bump ") != string::npos && !foundBump) {
						numTextures[currMat]++;
						foundBump = true;
					} else if(line.find("map_normal ") != string::npos && !foundNorm) {
						numTextures[currMat]++;
						foundNorm = true;
					} else if(line.find("normal ") != string::npos && !foundNorm) {
						numTextures[currMat]++;
						foundNorm = true;
					} else if(line.find("map_spec ") != string::npos && !foundSpec) {
						numTextures[currMat]++;
						foundSpec = true;
					} else if(line.find("spec ") != string::npos && !foundSpec) {
						numTextures[currMat]++;
						foundSpec = true;
					} else if(line.find("map_d ") != string::npos)
						numTextures[currMat]++;
				}
			}

			m->materials = new Material[numMaterials];
			currMat = -1;
			int currTex = 0;
			string ** textureFilesPerMaterial = new string*[numMaterials], ** textureNamesPerMaterial = new string*[numMaterials];

			matFile.close();
			matFile.open(path + fileName);

			while(matFile.good() && !matFile.eof()) {
				getline(matFile, line);
				if(line.length() > 0 && line[0] != '#') {
					if(line.find("newmtl ") != string::npos) {
						currMat++;
						currTex = 0;
						textureFilesPerMaterial[currMat] = new string[numTextures[currMat]];
						textureNamesPerMaterial[currMat] = new string[numTextures[currMat]];

						m->materials[currMat] = Material();
						m->materials[currMat].SetName(line.substr(7));
					} else if(line.find("Ns ") != string::npos) {
						float amount = 0.0f;
						if(stringstream(line.substr(line.find("Ns ") + 3)) >> amount) {
							m->materials[currMat].SetSpecularExponent(amount);
						} else return E_FAIL;
					} else if(line.find("Ka ") != string::npos && line.find("map") == string::npos) {
						size_t base = line.find("Ka ");
						float amountOne = 0.0f, amountTwo = 0.0f, amountThree = 0.0f;
						if(stringstream(line.substr(base + 3, 6)) >> amountOne && stringstream(line.substr(base + 10, 6)) >> amountTwo && stringstream(line.substr(base + 17, 6)) >> amountThree) {
							m->materials[currMat].SetAmbient(amountOne, amountTwo, amountThree);
						} else return E_FAIL;
					} else if(line.find("Kd ") != string::npos && line.find("map") == string::npos) {
						size_t base = line.find("Kd ");
						float amountOne = 0.0f, amountTwo = 0.0f, amountThree = 0.0f;
						if(stringstream(line.substr(base + 3, 6)) >> amountOne && stringstream(line.substr(base + 10, 6)) >> amountTwo && stringstream(line.substr(base + 17, 6)) >> amountThree) {
							m->materials[currMat].SetDiffuse(amountOne, amountTwo, amountThree);
						} else return E_FAIL;
					} else if(line.find("Ks ") != string::npos && line.find("map") == string::npos) {
						size_t base = line.find("Ks ");
						float amountOne = 0.0f, amountTwo = 0.0f, amountThree = 0.0f;
						if(stringstream(line.substr(base + 3, 6)) >> amountOne && stringstream(line.substr(base + 10, 6)) >> amountTwo && stringstream(line.substr(base + 17, 6)) >> amountThree) {
							m->materials[currMat].SetSpecular(amountOne, amountTwo, amountThree);
						} else return E_FAIL;
					} else if(line.find("map_Ka ") != string::npos) {
						textureFilesPerMaterial[currMat][currTex] = path + line.substr(line.find("map_Ka ") + 7);
						textureNamesPerMaterial[currMat][currTex] = "Ambient";
						currTex++;
					} else if(line.find("map_Kd ") != string::npos) {
						textureFilesPerMaterial[currMat][currTex] = path + line.substr(line.find("map_Kd ") + 7);
						textureNamesPerMaterial[currMat][currTex] = "Diffuse";
						currTex++;
					} else if(line.find("map_bump ") != string::npos) {
						textureFilesPerMaterial[currMat][currTex] = path + line.substr(line.find("map_bump ") + 9);
						textureNamesPerMaterial[currMat][currTex] = "Bump";
						currTex++;
					} else if(line.find("bump ") != string::npos) {
						textureFilesPerMaterial[currMat][currTex] = path + line.substr(line.find("bump ") + 5);
						textureNamesPerMaterial[currMat][currTex] = "Bump";
						currTex++;
					} else if(line.find("map_normal ") != string::npos) {
						textureFilesPerMaterial[currMat][currTex] = path + line.substr(line.find("map_normal ") + 11);
						textureNamesPerMaterial[currMat][currTex] = "Normal";
						currTex++;
					} else if(line.find("normal ") != string::npos) {
						textureFilesPerMaterial[currMat][currTex] = path + line.substr(line.find("normal ") + 7);
						textureNamesPerMaterial[currMat][currTex] = "Normal";
						currTex++;
					} else if(line.find("map_spec ") != string::npos) {
						textureFilesPerMaterial[currMat][currTex] = path + line.substr(line.find("map_spec ") + 9);
						textureNamesPerMaterial[currMat][currTex] = "Specular";
						currTex++;
					} else if(line.find("spec ") != string::npos) {
						textureFilesPerMaterial[currMat][currTex] = path + line.substr(line.find("spec ") + 5);
						textureNamesPerMaterial[currMat][currTex] = "Specular";
						currTex++;
					} else if(line.find("map_d ") != string::npos) {
						textureFilesPerMaterial[currMat][currTex] = path + line.substr(line.find("map_d ") + 6);
						textureNamesPerMaterial[currMat][currTex] = "Alpha";
						currTex++;
					}
				}
			}
			matFile.close();

			m->numMats = numMaterials;

			for(unsigned int i = 0; i < numMaterials; i++) {
				if(!m->materials[i].LoadTextures(textureNamesPerMaterial[i], textureFilesPerMaterial[i], numTextures[i]))
					return false;
			}
			return S_OK;
		} else
			return E_FAIL;
	} else
		return E_FAIL;
}

HRESULT MeshLoader::LoadOBJMesh(Mesh *& outMesh, string const & fileName, float scale) {
	if(!fileName.empty() && (fileName.find(".obj") != string::npos || fileName.find(".OBJ") != string::npos)) {
		outMesh = new Mesh();

		size_t found = fileName.find_last_of('/');
		string path = fileName.substr(0, found + 1);

		string line = "", objectName = "", matlibFileName = "";
		ifstream objFile(fileName);
		unsigned int numVertices = 0, numVertexNormals = 0, numTextureCoords = 0, numTriangles = 0, numSubObjects = 0;

		if(objFile.is_open()) {
			while(objFile.good() && !objFile.eof()) {
				getline(objFile, line);
				if(line.length() > 0) {
					if(line[0] == '#') {
						if(line.find(" object ") != string::npos)
							objectName = line.substr(9);
					} else if(line[0] == 'v') {
						if(line[1] == 'n')
							numVertexNormals++;
						else if(line[1] == 't')
							numTextureCoords++;
						else if(line[1] == ' ')
							numVertices++;
					} else if(line[0] == 'f')
						numTriangles++;
					else if(line.find("usemtl ") != string::npos)
						numSubObjects++;
					else if(line.find("mtllib ") != string::npos)
						matlibFileName = line.substr(7);
				}
			}

			HRESULT hr = LoadFromMTL(outMesh, path, matlibFileName);
			if(FAILED(hr))
				return hr;

			XMFLOAT3 * tempVertices = new XMFLOAT3[numVertices];
			XMFLOAT3 * tempNormals = new XMFLOAT3[numVertexNormals];
			XMFLOAT2 * tempTexCoords = new XMFLOAT2[numTextureCoords];

			outMesh->vertices = new Vertex[numTriangles * 3];
			outMesh->indices = new unsigned int[numTriangles * 3];
			outMesh->subObjects = new SubObject[numSubObjects];

			string prevMtlName = "", strGarbage = "";
			unsigned int vertexCount = 0, normCount = 0, texCount = 0, polygonIndex = 0, polygonCount = 0, prevPolygonCount = 0;
			int subObjIndex = -1;

			objFile.close();
			objFile.open(fileName);

			char line[256];
			ZeroMemory(&line, 256);
			objFile.getline(line, 256);

			while(!objFile.eof()) {
				if(line[0] == 'v') {
					if(line[1] == ' ') {
						float x = 0, y = 0, z = 0;
						if(sscanf_s(line, "v %f %f %f", &x, &y, &z) == 3) {
							tempVertices[vertexCount].x = x * scale;
							tempVertices[vertexCount].y = y * scale;
							tempVertices[vertexCount].z = z * scale;

							if(tempVertices[vertexCount].x > outMesh->bb.maxX)
								outMesh->bb.maxX = tempVertices[vertexCount].x;
							else if(tempVertices[vertexCount].x < outMesh->bb.minX)
								outMesh->bb.minX = tempVertices[vertexCount].x;

							if(tempVertices[vertexCount].y > outMesh->bb.maxY)
								outMesh->bb.maxY = tempVertices[vertexCount].y;
							else if(tempVertices[vertexCount].y < outMesh->bb.minY)
								outMesh->bb.minY = tempVertices[vertexCount].y;

							if(tempVertices[vertexCount].z > outMesh->bb.maxZ)
								outMesh->bb.maxZ = tempVertices[vertexCount].z;
							else if(tempVertices[vertexCount].z < outMesh->bb.minZ)
								outMesh->bb.minZ = tempVertices[vertexCount].z;

							vertexCount++;
						}
					} else if(line[1] == 'n') {
						float x = 0, y = 0, z = 0;
						if(sscanf_s(line, "vn %f %f %f", &x, &y, &z) == 3) {
							tempNormals[normCount].x = x;
							tempNormals[normCount].y = y;
							tempNormals[normCount].z = z;
							normCount++;
						}
					} else if(line[1] == 't') {
						float x = 0, y = 0;
						if(sscanf_s(line, "vt %f %f", &x, &y) == 2) {
							tempTexCoords[texCount].x = x;
							tempTexCoords[texCount].y = 1.0f - y;
							texCount++;
						}
					}
				} else if(line[0] == 'f') {
					if(line[1] == ' ') {
						unsigned int v1 = 0, v2 = 0, v3 = 0, n1 = 0, n2 = 0, n3 = 0, t1 = 0, t2 = 0, t3 = 0;
						int spr = sscanf_s(line, "f %d/%d/%d %d/%d/%d %d/%d/%d", &v1, &t1, &n1, &v2, &t2, &n2, &v3, &t3, &n3);
						if(spr == 9) {
							outMesh->vertices[polygonIndex] = Vertex(tempVertices[v1 - 1], tempNormals[n1 - 1], tempTexCoords[t1 - 1]);
							outMesh->vertices[polygonIndex + 1] = Vertex(tempVertices[v2 - 1], tempNormals[n2 - 1], tempTexCoords[t2 - 1]);
							outMesh->vertices[polygonIndex + 2] = Vertex(tempVertices[v3 - 1], tempNormals[n3 - 1], tempTexCoords[t3 - 1]);
							polygonIndex += 3;
							polygonCount += 3;
						}
					}
				} else if(line[0] == 'u') {
					string lineStr = line;
					if(lineStr.find("semtl ") != string::npos) {
						if(subObjIndex >= 0) {
							outMesh->subObjects[subObjIndex] = SubObject(prevMtlName, prevPolygonCount, polygonCount);
							prevPolygonCount = polygonCount;
							polygonCount = 0;
						}
						prevMtlName = lineStr.substr(7);
						subObjIndex++;
					}
				}

				ZeroMemory(&line, 256);
				objFile.getline(line, 256);
			}

			outMesh->subObjects[subObjIndex] = SubObject(prevMtlName, prevPolygonCount, polygonCount);
			outMesh->numSubObjs = numSubObjects;

			for(unsigned int i = 0; i < outMesh->numSubObjs; i++) {
				for(unsigned int j = 0; j < outMesh->numMats; j++) {
					if(outMesh->subObjects[i].matName == outMesh->materials[j].GetName())
						outMesh->subObjects[i].material = &(outMesh->materials[j]);
				}
			}

			objFile.close();

			outMesh->numVerts = outMesh->numInds = polygonIndex;
			for(unsigned int i = 0; i < polygonIndex; i++)
				outMesh->indices[i] = i;

			outMesh->CalculateAndAssignTangents();

			delete[] tempVertices;
			delete[] tempNormals;
			delete[] tempTexCoords;

			return S_OK;
		} else
			return E_FAIL;
	} else
		return E_FAIL;
}

HRESULT MeshLoader::LoadMesh(Mesh *& outMesh, string const & fileName, float scale, XMLElement * meshConfig) {
	if(fileName.length() < 5 || scale <= 0.0f)
		return E_FAIL;

	size_t lastPeriod = fileName.find_last_of('.');

	if(lastPeriod == string::npos || lastPeriod == fileName.length() - 1)
		return E_FAIL;

	string filetype = fileName.substr(lastPeriod + 1);

	if(filetype == "obj" || filetype == "OBJ")
		return LoadOBJMesh(outMesh, fileName, scale);
	else if(filetype == "md5" || filetype == "MD5" || filetype == "md5mesh" || filetype == "MD5MESH" || filetype == "md5anim" || filetype == "MD5ANIM")
		return LoadMD5Mesh(outMesh, fileName, scale, meshConfig);
	else
		return E_FAIL;
}