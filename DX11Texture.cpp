#include "DX11Texture.h"
#include "DirectX11App.h"
#include "WICTextureLoader.h"

DX11Texture::DX11Texture() : Texture(""), srv(NULL), resource(NULL), width(UINT_MAX), height(UINT_MAX), pixels(NULL), CPUAccess(false) { }
DX11Texture::DX11Texture(const string & path, bool CPUAccess) : Texture(path), srv(NULL), resource(NULL), width(0), height(0), pixels(NULL), CPUAccess(CPUAccess) { }

bool DX11Texture::Load() {
	if(path.length() > 0) {
		ID3D11Resource * temp = NULL;
		wstring stemp = wstring(path.begin(), path.end());
		HRESULT hr = (CPUAccess ? WIC::CreateWICTextureFromFile(DirectX11App::Current()->GetDevice(), DirectX11App::Current()->GetDeviceContext(), stemp.c_str(), &temp, NULL, 0, D3D11_CPU_ACCESS_READ) : WIC::CreateWICTextureFromFile(DirectX11App::Current()->GetDevice(), DirectX11App::Current()->GetDeviceContext(), stemp.c_str(), &temp, &srv, 0));

		if(FAILED(hr))
			return false;

		resource = (ID3D11Texture2D *)temp;

		D3D11_TEXTURE2D_DESC textureDesc;
		ZeroMemory(&textureDesc, sizeof(D3D11_TEXTURE2D_DESC));

		resource->GetDesc(&textureDesc);

		width = textureDesc.Width;
		height = textureDesc.Height;

		if(CPUAccess) {
			unsigned int bitsPerPixel = WIC::GetBitsPerPixel(textureDesc.Format);
			if(bitsPerPixel != 8 && bitsPerPixel != 16 && bitsPerPixel != 32 && bitsPerPixel != 64 && bitsPerPixel != 128)
				return false;
			unsigned int bytesPerPixel = bitsPerPixel / 8;

			D3D11_MAPPED_SUBRESOURCE mappedSubResource;
			ZeroMemory(&mappedSubResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
			hr = DirectX11App::Current()->GetDeviceContext()->Map(resource, 0, D3D11_MAP_READ, 0, &mappedSubResource);

			if(FAILED(hr)) {
				DirectX11App::Current()->GetDeviceContext()->Unmap(resource, 0);
				return false;
			}

			pixels = new Colour*[width];
			for(unsigned int x = 0; x < width; x++) {
				pixels[x] = new Colour[height];
				ZeroMemory(pixels[x], sizeof(Colour) * height);
			}

			unsigned char * tempPtr = (unsigned char *)mappedSubResource.pData;

			for(unsigned int y = 0; y < height; y++) {
				for(unsigned int x = 0; x < width; x++) {
					if(bitsPerPixel == 8)
						pixels[x][y] = tempPtr[(y * mappedSubResource.RowPitch) + (x * bytesPerPixel)];
					else if(bitsPerPixel == 16) { return false; }
					else if(bitsPerPixel == 32) {
						unsigned int index = (y * mappedSubResource.RowPitch) + (x * bytesPerPixel);
						pixels[x][y].R = ((float)tempPtr[index] / 255.0f);
						pixels[x][y].G = ((float)tempPtr[index + 1] / 255.0f);
						pixels[x][y].B = ((float)tempPtr[index + 2] / 255.0f);
						pixels[x][y].A = ((float)tempPtr[index + 3] / 255.0f);
					} else if(bitsPerPixel == 64) { return false; }
					else if(bitsPerPixel == 128) {
						unsigned int index = (y * mappedSubResource.RowPitch) + (x * bytesPerPixel);
						memcpy(&pixels[x][y].R, &(tempPtr[index]), 4);
						memcpy(&pixels[x][y].G, &(tempPtr[index + 4]), 4);
						memcpy(&pixels[x][y].B, &(tempPtr[index + 8]), 4);
						memcpy(&pixels[x][y].A, &(tempPtr[index + 12]), 4);
					} else
						return false;
				}
			}

			DirectX11App::Current()->GetDeviceContext()->Unmap(resource, 0);
		}
	} else
		return false;
	return true;
}

void DX11Texture::Cleanup() {
	if(resource) {
		resource->Release();
		resource = NULL;
	}

	if(srv) {
		srv->Release();
		srv = NULL;
	}

	if(pixels != NULL) {
		for(unsigned int x = 0; x < width; x++) {
			delete[] pixels[x];
			pixels[x] = NULL;
		}

		delete[] pixels;
		pixels = NULL;
	}

	width = 0;
	height = 0;

	CPUAccess = false;

	Texture::Cleanup();
}

unsigned int DX11Texture::GetWidth() const {
	return width;
}

unsigned int DX11Texture::GetHeight() const {
	return height;
}

Colour ** DX11Texture::GetPixels() const {
	return pixels;
}

Colour DX11Texture::GetPixel(const unsigned int & X, const unsigned int & Y) const {
	return (CPUAccess && pixels != NULL && X >= 0 && X < width && Y >= 0 && Y < height) ? pixels[X][Y] : Colour::NullColour;
}

Colour DX11Texture::GetLocalisedPixel(const float & X, const float & Y) const {
	return (CPUAccess && pixels != NULL && X >= 0 && X <= 1.0f && Y >= 0 && Y <= 1.0f) ? pixels[(unsigned int)(X * (width - 1))][(unsigned int)(Y * (height - 1))] : Colour::NullColour;
}

ID3D11Texture2D * const DX11Texture::GetTexture() const {
	return resource;
}

ID3D11ShaderResourceView * const DX11Texture::GetShaderResourceView() const {
	return srv;
}

ID3D11ShaderResourceView * const * DX11Texture::GetShaderResourceViewPtr() const {
	return &srv;
}