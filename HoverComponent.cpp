#include "HoverComponent.h"

RegisterComponentType(HoverComponent);

HoverComponent::HoverComponent() : Component(), transform(NULL), upDown(0), outVal(0), prevOutVal(0) { }

HoverComponent::~HoverComponent() { }

bool HoverComponent::Initialise(XMLElement * init) {
	if(init == NULL)
		return false;

	transform = sceneObject->GetComponent<TransformComponent>();

	return true;
}

void HoverComponent::Start() { }

void HoverComponent::Shutdown() { }

void HoverComponent::Update(float dt) {
	transform->LocalMove(0, outVal - prevOutVal, 0);

	upDown += (dt * 0.25f);
	prevOutVal = outVal;
	outVal = sin(upDown) * 0.25f;
}

string HoverComponent::GetTypename() const {
	return "HoverComponent";
}