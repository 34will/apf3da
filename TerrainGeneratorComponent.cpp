#include "TerrainGeneratorComponent.h"
#include "Mesh.h"
#include "Material.h"
#include "DirectX11App.h"
#include "TerrainGeneration.h"
#include <fstream>
#include <cstdlib>

RegisterComponentType(TerrainGeneratorComponent);

TerrainGeneratorComponent::TerrainGeneratorComponent() : Component(), render(NULL) {}

TerrainGeneratorComponent::~TerrainGeneratorComponent() {}

bool TerrainGeneratorComponent::Initialise(XMLElement * init) {
	if(init == NULL)
		return false;

	render = sceneObject->GetComponent<RenderComponent>();
	if(render == NULL)
		return false;

	unsigned int numMats = 1;
	XMLError err = init->QueryUnsignedAttribute("numMats", &numMats);
	if(err != XML_SUCCESS)
		return false;

	XMLElement * materialElement = init->FirstChildElement("Material");
	if(materialElement == NULL)
		return false;

	Material * materials = new Material[numMats];
	string ** textureFilesPerMaterial = new string*[numMats], ** textureNamesPerMaterial = new string*[numMats];

	for(unsigned int i = 0; i < numMats && materialElement != NULL; i++) {
		materials[i] = Material();

		string name = materialElement->Attribute("name");
		if(name.length() <= 0)
			return false;

		materials[i].SetName(name);

		XMLElement * subElement = materialElement->FirstChildElement("Ambient");

		if(subElement == NULL)
			materials[i].SetAmbient(0.0f, 0.0f, 0.0f);
		else {
			float r, g, b;
			if(((err = subElement->QueryFloatAttribute("r", &r)) != XML_SUCCESS) || ((err = subElement->QueryFloatAttribute("g", &g)) != XML_SUCCESS) || ((err = subElement->QueryFloatAttribute("b", &b)) != XML_SUCCESS))
				return false;

			materials[i].SetAmbient(r, g, b);
		}

		subElement = materialElement->FirstChildElement("Diffuse");

		if(subElement == NULL)
			materials[i].SetDiffuse(1.0f, 1.0f, 1.0f);
		else {
			float r, g, b;
			if(((err = subElement->QueryFloatAttribute("r", &r)) != XML_SUCCESS) || ((err = subElement->QueryFloatAttribute("g", &g)) != XML_SUCCESS) || ((err = subElement->QueryFloatAttribute("b", &b)) != XML_SUCCESS))
				return false;

			materials[i].SetDiffuse(r, g, b);
		}

		subElement = materialElement->FirstChildElement("Specular");

		if(subElement == NULL)
			materials[i].SetSpecular(1.0f, 1.0f, 1.0f);
		else {
			float r, g, b;
			if(((err = subElement->QueryFloatAttribute("r", &r)) != XML_SUCCESS) || ((err = subElement->QueryFloatAttribute("g", &g)) != XML_SUCCESS) || ((err = subElement->QueryFloatAttribute("b", &b)) != XML_SUCCESS))
				return false;

			materials[i].SetSpecular(r, g, b);
		}

		subElement = materialElement->FirstChildElement("SpecularExponent");

		if(subElement == NULL)
			materials[i].SetSpecularExponent(1.0f);
		else {
			float value;

			XMLError err = subElement->QueryFloatText(&value);
			if(err != XML_SUCCESS)
				return false;

			materials[i].SetSpecularExponent(value);
		}

		XMLElement * texturesElement = materialElement->FirstChildElement("Textures");
		if(texturesElement != NULL) {
			unsigned int numTexs = 0;
			XMLError err = texturesElement->QueryUnsignedAttribute("count", &numTexs);
			if(err != XML_SUCCESS || numTexs == 0)
				return false;

			textureFilesPerMaterial[i] = new string[numTexs];
			textureNamesPerMaterial[i] = new string[numTexs];
			subElement = texturesElement->FirstChildElement("Texture");

			for(unsigned int j = 0; j < numTexs && subElement != NULL; j++) {
				string type = subElement->Attribute("type");

				textureFilesPerMaterial[i][j] = subElement->GetText();
				textureNamesPerMaterial[i][j] = type;
				subElement = subElement->NextSiblingElement("Texture");
			}

			if(!materials[i].LoadTextures(textureNamesPerMaterial[i], textureFilesPerMaterial[i], numTexs))
				return false;
		}

		materialElement = materialElement->NextSiblingElement("Material");
	}

	XMLElement * generationElement = init->FirstChildElement("Generation");
	if(generationElement == NULL)
		return false;

	Mesh * mesh = new Mesh();

	if(!TerrainGeneration::GenerateTerrainMesh(mesh, generationElement, materials, numMats))
		return false;

	render->mesh = mesh;

	return true;
}

void TerrainGeneratorComponent::Start() {}

void TerrainGeneratorComponent::Shutdown() {}

string TerrainGeneratorComponent::GetTypename() const {
	return "TerrainGeneratorComponent";
}