#ifndef VERTEXSHADER_H
#define VERTEXSHADER_H

#include "Shader.h"

class VertexShader : public Shader {
private:
	ID3D11VertexShader * shader;
	ID3D11InputLayout * vertexLayout;
	const D3D11_INPUT_ELEMENT_DESC * layout;
	const UINT numElements;

protected:
	void SetConstantBuffers(ID3D11DeviceContext * dX11DeviceContext, const unsigned int & StartSlot, const unsigned int & NumBuffers, ID3D11Buffer * const * constantBuffers) const;
	void SetTextures(ID3D11DeviceContext * dX11DeviceContext, const unsigned int & StartSlot, const unsigned int & NumBuffers, ID3D11ShaderResourceView * const * srvs) const;

public:
	VertexShader(WCHAR * file, const D3D11_INPUT_ELEMENT_DESC * layout, UINT numElements, LPCSTR entryPoint = "VS", LPCSTR version = "vs_4_0");
	~VertexShader();

	HRESULT Initialise(ID3D11Device * dX11Device);

	void RegisterInputLayout(ID3D11DeviceContext * dX11DeviceContext);

	ID3D11VertexShader * getShaderHandle();

	void Activate(ID3D11DeviceContext * dX11DeviceContext);

	VertexShader& operator=(const VertexShader& other);
};

#endif
