#ifndef PIXELSHADER_H
#define PIXELSHADER_H

#include "Shader.h"

class PixelShader : public Shader {
private:
	ID3D11PixelShader * shader;

protected:
	void SetConstantBuffers(ID3D11DeviceContext * dX11DeviceContext, const unsigned int & StartSlot, const unsigned int & NumBuffers, ID3D11Buffer * const * constantBuffers) const;
	void SetTextures(ID3D11DeviceContext * dX11DeviceContext, const unsigned int & StartSlot, const unsigned int & NumBuffers, ID3D11ShaderResourceView * const * srvs) const;

public:
	PixelShader(WCHAR * file, LPCSTR entryPoint = "PS", LPCSTR version = "ps_4_0");
	~PixelShader();

	HRESULT Initialise(ID3D11Device * dX11Device);

	ID3D11PixelShader * getShaderHandle();

	void Activate(ID3D11DeviceContext * dX11DeviceContext);

	PixelShader& operator=(const PixelShader& other);
};

#endif
