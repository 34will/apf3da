#ifndef WINDOWS_APP_H
#define WINDOWS_APP_H

#include <windows.h>
#include "resource.h"
#include "DirectX11App.h"
#include "tinyxml2.h"

typedef void (*ResizeWindowCallback)(HWND);

class WindowsApp
{
private:
	HINSTANCE hInst;
	HWND hWnd;

public:
    ResizeWindowCallback resize;
	static LRESULT CALLBACK StaticWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

	WindowsApp();
	~WindowsApp();

	LRESULT WndProc(UINT message, WPARAM wParam, LPARAM lParam);
    HRESULT InitWindow(HINSTANCE hInstance, int nCmdShow, tinyxml2::XMLElement * windowParams);

	HWND GetHwnd() { return hWnd; }
	HINSTANCE GetHInst() { return hInst; }
};

#endif

