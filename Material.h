#ifndef MATERIAL_H
#define MATERIAL_H

#include <d3d11.h>
#include <DirectXMath.h>
#include <string>
#include <unordered_map>
#include "DX11Texture.h"

using namespace std;
using namespace DirectX;

typedef unordered_map<string, DX11Texture> StringTextureUMap;

class Material {
private:
	string name;
	XMFLOAT4 ambient, diffuse, specular;
	float specularExponent;
	bool isTextured, hasAlpha;
	StringTextureUMap textures;

public:
	Material();
	~Material();

	void Cleanup();

	void SetName(string value) { name = value; }
	string GetName() const { return name; }

	void SetAmbient(XMFLOAT4 value) { ambient = value; }
	void SetAmbient(float x, float y, float z) { ambient = XMFLOAT4(x, y, z, 1.0f); }
	XMFLOAT4 GetAmbient() const { return ambient; }
	XMFLOAT4 const * GetAmbientPtr() const { return &ambient; }

	void SetDiffuse(XMFLOAT4 value) { diffuse = value; }
	void SetDiffuse(float x, float y, float z) { diffuse = XMFLOAT4(x, y, z, 1.0f); }
	XMFLOAT4 GetDiffuse() const { return diffuse; }
	XMFLOAT4 const * GetDiffusePtr() const { return &diffuse; }

	void SetSpecular(XMFLOAT4 value) { specular = value; }
	void SetSpecular(float x, float y, float z) { specular = XMFLOAT4(x, y, z, 1.0f); }
	XMFLOAT4 GetSpecular() const { return specular; }
	XMFLOAT4 const * GetSpecularPtr() const { return &specular; }

	void SetSpecularExponent(float value) { specularExponent = value; }
	float GetSpecularExponent() const { return specularExponent; }
	float const * GetSpecularExponentPtr() const { return &specularExponent; }

	void const * GetVariable(const string & name) const;

	ID3D11ShaderResourceView * GetTexture(const string & name) const;
	ID3D11ShaderResourceView * const * GetTexturePtr(const string & name) const;

	void SetTexture(const string & name, const DX11Texture & texture);
	void SetTextures(const string * const textureNames, const DX11Texture * const textures, const unsigned int & numTextures);

	bool LoadTexture(const string & name, const string & path);
	bool LoadTextures(const string * const textureNames, const string * const texturePaths, const unsigned int & numTextures);
};

#endif