#include "Component.h"
#include "Includes.h"
#include "SceneObject.h"

Component::Component() : sceneObject(NULL) { }

Component::~Component() {
	sceneObject = NULL;
}

void Component::setSceneObject(SceneObject * sceneObject) {
	this->sceneObject = sceneObject;
}

SceneObject * Component::GetSceneObject() const {
	return sceneObject;
}

void Component::Start() { }

void Component::Shutdown() { }

void Component::Update(float dt) { UnusedVariable(dt); }

void Component::Draw(ID3D11DeviceContext * immediateContext, Pass * pass, CameraComponent * currentCam) { UnusedVariable(immediateContext); UnusedVariable(pass); UnusedVariable(currentCam); }

void Component::Register(const string& name, ComponentCreator creator) {
	ComponentFactory::Register(name, creator);
}