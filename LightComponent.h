#ifndef LIGHT_COMPONENT_H
#define LIGHT_COMPONENT_H

#include <DirectXMath.h>
#include <vector>
#include "Component.h"
#include "TransformComponent.h"
#include "ComponentRegistrar.h"

using namespace std;
using namespace DirectX;
using namespace tinyxml2;

class LightComponent : public Component {
private:
	static vector<LightComponent *> lights;

	TransformComponent * transform;
	XMFLOAT3 diffuse, specular;
	int lightType;
	float lightRadius, lightConeAngleCosine, lightSpotDecay;
	bool enabled;

	LightComponent();

public:
	static vector<LightComponent *> * GetLights();

	~LightComponent();

	bool Initialise(XMLElement * init);

	void Start();
	void Shutdown();

	string GetTypename() const;

	void SetDiffuse(XMFLOAT3 diffuse);
	void SetDiffuse(float diffuseR, float diffuseG, float diffuseB);
	XMFLOAT3 GetDiffuse() const;
	XMFLOAT3 * GetDiffusePtr();

	void SetSpecular(XMFLOAT3 specular);
	void SetSpecular(float specularR, float specularG, float specularB);

	void SetEnabled(bool value);

	XMFLOAT3 GetSpecular() const;
	XMFLOAT3 * GetSpecularPtr();

	XMFLOAT3 GetPosition();
	XMFLOAT3 * GetPositionPtr();

	XMFLOAT3 GetDirection();
	XMFLOAT3 * GetDirectionPtr();

	int GetLightType();
	int * GetLightTypePtr();

	float GetLightRadius();
	float * GetLightRadiusPtr();

	float GetLightConeAngleCosine();
	float * GetLightConeAngleCosinePtr();

	float GetLightSpotDecay();
	float * GetLightSpotDecayPtr();

	bool GetEnabled();

	SetupComponentType(LightComponent);
};

DeclareType(LightComponent);

#endif