#ifndef MESH_H
#define MESH_H

// Const-correct, ignoring DirectX11

#include <d3d11.h>
#include <DirectXMath.h>
#include <string>
#include "Material.h"
#include "Pass.h"
#include "MeshStructs.h"
#include "MeshLoader.h"
#include "Includes.h"

using namespace std;
using namespace DirectX;

class Mesh {
	friend class MeshLoader;

private:
	XMFLOAT3 * tangentCalc = NULL;

protected:
	Vertex * vertices;
	unsigned int * indices, numVerts, numInds, numSubObjs, numMats;
	ID3D11Buffer * vertexBuffer, *indexBuffer;
	Material * materials;
	SubObject * subObjects;
	BoundingBox bb;
	bool modifyingVertices, modifyingIndices;
	D3D11_MAPPED_SUBRESOURCE mappedVResource, mappedIResource;

public:
	Mesh();
	~Mesh() { }

	HRESULT BuildBuffers(ID3D11Device * dX11Device);
	virtual void CleanUp();

	void Draw(ID3D11DeviceContext * immediateContext, Pass * pass);

	BoundingBox GetBoundingBox();
	BoundingBox GetAABoundingBox(XMFLOAT4X4 const & world);

	Material const * const GetMaterials() const;
	unsigned int GetNumMaterials() const;
	void SetMaterials(Material const * const materials, unsigned int numMats);

	Vertex const * const GetVertices() const;
	unsigned int GetNumVertices() const;

	unsigned int const * const GetIndices() const;
	unsigned int GetNumIndices() const;

	void CalculateAndAssignNormals();
	void CalculateAndAssignTangents();

	void Create(Vertex const * const vertices, unsigned int numVerts, unsigned int const * const indices, unsigned int numInds, Material const * const materials, unsigned int numMats);

	bool BeginModify(Vertex * * const vertices, unsigned int * * const indices);
	void EndModify();
};

#endif