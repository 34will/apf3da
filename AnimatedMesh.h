#ifndef ANIMATEDMESH_H
#define ANIMATEDMESH_H

#include <unordered_map>
#include "Mesh.h"

typedef unordered_map<string, Animation *> AnimMap;

class AnimatedMesh : public Mesh {
	friend class MeshLoader;

private:
	AnimMap animationMap;
	Joint * joints = NULL, * interpolatedJoints = NULL;
	Weight * weights = NULL;
	MD5Vertex * md5Vertices = NULL;
	Animation * animations = NULL;
	unsigned int numWeights = 0, numJoints = 0, numAnimations = 0, currentAnim = 0;
	float animTimer = 0.0f;
	bool playing = false;

	void PrepareAnimations();

public:
	void CleanUp();

	void Update(float dt);

	void Play();
	void Stop();
};

#endif