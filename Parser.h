#ifndef PARSER_H
#define PARSER_H

#include <unordered_map>

using namespace std;

template <typename T>
struct Parser {
	unordered_map<string, T> map;

	bool Parse(const string & value, T & out) const {
		unordered_map<string, T>::const_iterator iter = map.find(value);
		if(iter == map.end())
			return false;
		else {
			out = iter->second;
			return true;
		}
	}
};

#endif
