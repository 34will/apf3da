struct FinalVSInput {
	float3	Position    : POSITION;
	float2	Texel       : TEXCOORD0;
	uint	Index		: TEXCOORD1;
};

struct FinalVSOutput {
	float4	Position		: SV_POSITION;
	float2	Texel			: TEXCOORD0;
};

FinalVSOutput main(FinalVSInput input) {
	FinalVSOutput output = (FinalVSOutput)0;

	output.Position = float4(input.Position, 1.0f);
	output.Texel = input.Texel;

	return output;
}