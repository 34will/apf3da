#ifndef HOVER_COMPONENT_H
#define HOVER_COMPONENT_H

#include <DirectXMath.h>
#include "Component.h"
#include "TransformComponent.h"
#include "ComponentRegistrar.h"

using namespace std;
using namespace DirectX;
using namespace tinyxml2;

class HoverComponent : public Component {
private:
	TransformComponent * transform;
	float prevOutVal, outVal, upDown;

	HoverComponent();

public:
	~HoverComponent();

	bool Initialise(XMLElement * init);

	void Start();
	void Shutdown();

	void Update(float dt);

	string GetTypename() const;

	SetupComponentType(HoverComponent);
};

DeclareType(HoverComponent);

#endif


