#ifndef MESH_GENERATION_H
#define MESH_GENERATION_H

#include "Mesh.h"
#include "Parser.h"
#include "Material.h"

enum class MeshGenerationType {
	None,
	Plane,
	Cube,
	Sphere,
	CubeSphere
};

enum class CubeMapTextureType {
	None,
	OneTexture,
	ThreeByTwo,
	TNet,
	CrossNet
};

class MeshGeneration {
private:
	static const Parser<MeshGenerationType> mgtParser;
	static const Parser<CubeMapTextureType> cmttParser;

	static XMFLOAT2 CubeMapTextureCoords(CubeMapTextureType type, unsigned int i, unsigned int j, unsigned int numSubdivisions, unsigned int face);

public:
	static bool GeneratePlane(Mesh * mesh, Material * const materials, const unsigned int & numMaterials, float width, float depth, unsigned int numRows, unsigned int numCols, XMFLOAT3 offset);
	static bool GenerateCube(Mesh * mesh, Material * const materials, const unsigned int & numMaterials, CubeMapTextureType textureType, float diameter, unsigned int numberOfSubdivisions, XMFLOAT3 offset);
	static bool GenerateSphere(Mesh * mesh, Material * const materials, const unsigned int & numMaterials);
	static bool GenerateCubeSphere(Mesh * mesh, Material * const materials, const unsigned int & numMaterials, CubeMapTextureType textureType, float diameter, unsigned int numberOfSubdivisions, XMFLOAT3 offset);

	static MeshGenerationType StringToGenType(const string & type);
	static CubeMapTextureType StringToCubeMapType(const string & type);
};

#endif