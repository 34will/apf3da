#ifndef COMPONENT_H
#define COMPONENT_H

#include <Windows.h>
#include <d3d11.h>
#include "tinyxml2.h"
#include "Pass.h"
#include "ComponentFactory.h"

using namespace tinyxml2;

class SceneObject;
class CameraComponent;

class Component {
	friend class SceneObject;

private:
	void setSceneObject(SceneObject * sceneObject);

protected:
	SceneObject * sceneObject;

	Component();

	virtual void Start();
	virtual void Shutdown();
	virtual void Update(float dt);
	virtual void Draw(ID3D11DeviceContext * immediateContext, Pass * pass, CameraComponent * currentCam);

	static void Register(const string& name, ComponentCreator creator);

	template<typename T>
	static Component * CreateComponent();

public:
	virtual ~Component();

	virtual bool Initialise(XMLElement * init) = 0;

	SceneObject * GetSceneObject() const;
	virtual string GetTypename() const = 0;
};

template<typename T>
Component * Component::CreateComponent() { return new T; }

#endif