#include "AnimatedMesh.h"

void AnimatedMesh::PrepareAnimations() {
	for(unsigned int i = 0; i < numAnimations; i++)
		animationMap[animations[i].Name] = &animations[i];

	interpolatedJoints = new Joint[numJoints];
}

void AnimatedMesh::CleanUp() {
	animationMap.clear();

	/*if(joints != NULL) {
		delete[] joints;
		joints = NULL;
	}*/

	if(interpolatedJoints != NULL) {
		delete[] interpolatedJoints;
		interpolatedJoints = NULL;
	}

	if(weights != NULL) {
		delete[] weights;
		weights = NULL;
	}

	if(md5Vertices != NULL) {
		delete[] md5Vertices;
		md5Vertices = NULL;
	}

	if(animations != NULL) {
		for(unsigned int i = 0; i < numAnimations; i++) {
			for(unsigned int j = 0; j < animations[i].NumberOfFrames; j++)
				delete[] animations[i].FrameJoints[j];

			delete[] animations[i].FrameJoints;
			delete[] animations[i].BoundingBoxes;
		}

		delete[] animations;
		animations = NULL;
	}

	numWeights = 0;
	numJoints = 0;
	numAnimations = 0;
	currentAnim = 0;
	animTimer = 0.0f;

	Mesh::CleanUp();
}

void AnimatedMesh::Update(float dt) {
	if(playing) {
		animTimer += dt;

		while(animTimer > animations[currentAnim].AnimationLength)
			animTimer -= animations[currentAnim].AnimationLength;

		float currentFrame = animTimer * animations[currentAnim].FrameRate;
		unsigned int frame0 = (int)currentFrame, frame1 = frame0 + 1;
		float interpolation = currentFrame - frame0;

		if(frame0 >= animations[currentAnim].NumberOfFrames - 1)
			frame1 = 0;

		for(unsigned int i = 0; i < animations[currentAnim].NumberOfJoints; i++) {
			Joint joint0 = animations[currentAnim].FrameJoints[frame0][i];
			Joint joint1 = animations[currentAnim].FrameJoints[frame1][i];

			interpolatedJoints[i].ParentID = joint0.ParentID;

			XMVECTOR outJ0Orient = XMLoadFloat4(&joint0.Orientation), outJ1Orient = XMLoadFloat4(&joint1.Orientation);

			interpolatedJoints[i].Position = DXHelper::Lerp(joint0.Position, joint1.Position, interpolation);
			XMStoreFloat4(&interpolatedJoints[i].Orientation, XMQuaternionSlerp(outJ0Orient, outJ1Orient, interpolation));
		}

		for(unsigned int i = 0; i < numVerts; ++i) {
			MD5Vertex & tempMD5Vert = md5Vertices[i];
			Vertex & tempVert = vertices[i];
			tempVert.Position = XMFLOAT3(0.0f, 0.0f, 0.0f);
			tempVert.Normal = XMFLOAT3(0.0f, 0.0f, 0.0f);

			for(unsigned int j = 0; j < tempMD5Vert.WeightCount; j++) {
				Weight & tempWeight = weights[tempMD5Vert.WeightStartID + j];
				Joint & tempJoint = interpolatedJoints[tempWeight.JointID];

				XMVECTOR tempJointOrientation = XMLoadFloat4(&tempJoint.Orientation);
				XMVECTOR tempJointOrientationConjugate = XMQuaternionInverse(tempJointOrientation);
				XMVECTOR tempWeightPos = XMVectorSet(tempWeight.LocalPosition.x, tempWeight.LocalPosition.y, tempWeight.LocalPosition.z, 0.0f);

				XMFLOAT3 rotatedPoint;
				XMStoreFloat3(&rotatedPoint, XMQuaternionMultiply(XMQuaternionMultiply(tempJointOrientation, tempWeightPos), tempJointOrientationConjugate));

				tempVert.Position = DXHelper::Add(tempVert.Position, DXHelper::Multiply(DXHelper::Add(tempJoint.Position, rotatedPoint), tempWeight.Influence));

				XMVECTOR tempWeightNormal = XMVectorSet(tempWeight.Normal.x, tempWeight.Normal.y, tempWeight.Normal.z, 0.0f);
				XMStoreFloat3(&rotatedPoint, XMQuaternionMultiply(XMQuaternionMultiply(tempJointOrientation, tempWeightNormal), tempJointOrientationConjugate));

				tempVert.Normal = DXHelper::Subtract(tempVert.Normal, DXHelper::Multiply(rotatedPoint, tempWeight.Influence));
				XMVECTOR outNormal = XMLoadFloat3(&tempVert.Normal);
				outNormal = XMVector3Normalize(outNormal);
				XMStoreFloat3(&tempVert.Normal, outNormal);
			}
		}

		CalculateAndAssignTangents();
	}
}

void AnimatedMesh::Play() {
	playing = true;
}

void AnimatedMesh::Stop() {
	playing = false;
}