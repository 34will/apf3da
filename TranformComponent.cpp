#include "TransformComponent.h"
#include "DXHelperStructs.h"
#include "SceneGraph.h"

RegisterComponentType(TransformComponent);

TransformComponent::TransformComponent() : Component(), parent(NULL), children(), rotationQuaternion(0, 0, 0, 1.0f), worldRotationQuaternion(1.0f, 0, 0, 0), position(0, 0, 0), worldPos(0, 0, 0), scale(1.0f, 1.0f, 1.0f), worldScl(1.0f, 1.0f, 1.0f), forward(1.0f, 0, 0) {}

TransformComponent::~TransformComponent() {}

bool TransformComponent::Initialise(XMLElement * init) {
	if(init == NULL)
		return false;

	float posX = 0, posY = 0, posZ = 0, rotX = 0, rotY = 0, rotZ = 0, sclX = 0, sclY = 0, sclZ = 0;

	XMLError err = init->QueryFloatAttribute("posX", &posX);
	if(err != XML_SUCCESS)
		return false;

	err = init->QueryFloatAttribute("posY", &posY);
	if(err != XML_SUCCESS)
		return false;

	err = init->QueryFloatAttribute("posZ", &posZ);
	if(err != XML_SUCCESS)
		return false;

	err = init->QueryFloatAttribute("rotX", &rotX);
	if(err != XML_SUCCESS)
		return false;

	err = init->QueryFloatAttribute("rotY", &rotY);
	if(err != XML_SUCCESS)
		return false;

	err = init->QueryFloatAttribute("rotZ", &rotZ);
	if(err != XML_SUCCESS)
		return false;

	err = init->QueryFloatAttribute("sclX", &sclX);
	if(err != XML_SUCCESS)
		return false;

	err = init->QueryFloatAttribute("sclY", &sclY);
	if(err != XML_SUCCESS)
		return false;

	err = init->QueryFloatAttribute("sclZ", &sclZ);
	if(err != XML_SUCCESS)
		return false;

	SetLocal(posX, posY, posZ, rotX, rotY, rotZ, sclX, sclY, sclZ, true);

	int parentID = -1;
	if(init->QueryIntAttribute("parent", &parentID) != XML_SUCCESS)
		return false;

	if(parentID != -1) {
		SceneObject * parentObj = SceneGraph::CurrentScene()->GetSceneObject(parentID);

		if(parentObj == NULL)
			return false;

		this->SetParent(parentObj->GetComponent<TransformComponent>());

		if(parent == NULL)
			return false;
	}

	updateLocal();

	return true;
}

void TransformComponent::Start() {}

void TransformComponent::Shutdown() {}

bool TransformComponent::doDecomp() {
	if(changed) {
		changed = false;
		XMMATRIX outWorld = XMLoadFloat4x4(&world);
		XMVECTOR outScale, outQuat, outTrans, outForward = XMLoadFloat4(&DXHelper::ForwardVector4), outRight = XMLoadFloat4(&DXHelper::RightVector4);
		if(XMMatrixDecompose(&outScale, &outQuat, &outTrans, outWorld)) {
			XMStoreFloat3(&worldScl, outScale);
			XMStoreFloat4(&worldRotationQuaternion, outQuat);
			XMStoreFloat3(&worldPos, outTrans);

			outForward = XMVector4Transform(outForward, outWorld);
			outForward = XMVector4Normalize(outForward);
			XMStoreFloat3(&forward, outForward);
			outRight = XMVector4Transform(outRight, outWorld);
			outRight = XMVector4Normalize(outRight);
			XMStoreFloat3(&right, outRight);
			return true;
		} else
			return false;
	}
	return true;
}

void TransformComponent::updateLocal() {
	XMVECTOR rot = XMLoadFloat4(&rotationQuaternion);
	XMMATRIX outLocal = XMMatrixIdentity(), calc = XMMatrixScaling(scale.x, scale.y, scale.z);
	outLocal *= calc;
	calc = XMMatrixRotationQuaternion(rot);
	outLocal *= calc;
	calc = XMMatrixTranslation(position.x, position.y, position.z);
	outLocal *= calc;
	XMStoreFloat4x4(&local, outLocal);

	updateWorld((parent == NULL ? NULL : parent->GetWorldPtr()));
}

void TransformComponent::updateWorld(XMFLOAT4X4 * parentMatrix) {
	if(parentMatrix == NULL) {
		world = local;
		XMMATRIX outWorld = XMLoadFloat4x4(&world);
	} else {
		XMMATRIX outWorld = XMLoadFloat4x4(parentMatrix), outLocal = XMLoadFloat4x4(&local);
		outWorld = outLocal * outWorld;
		XMStoreFloat4x4(&world, outWorld);
	}

	changed = true;

	for(auto c : children)
		c->updateWorld(&world);

	doDecomp();
}

void TransformComponent::RotateAboutPoint(XMFLOAT3 point, float angle) {
	UnusedVariable(point);
	UnusedVariable(angle);
}

void TransformComponent::LocalMove(XMFLOAT3 offset) {
	SetLocalPosition(DXHelper::Add(position, offset));
}

void TransformComponent::LocalMove(float xOffset, float yOffset, float zOffset) {
	SetLocalPosition(DXHelper::Add(position, xOffset, yOffset, zOffset));
}

void TransformComponent::LocalRotate(XMFLOAT3 offset, bool degrees) {
	XMFLOAT3 currentRot = DXHelper::QuaternionToEuler(rotationQuaternion, degrees);
	SetLocalRotation(currentRot.x + offset.x, currentRot.y + offset.y, currentRot.z + offset.z, degrees);
}

void TransformComponent::LocalRotate(float xOffset, float yOffset, float zOffset, bool degrees) {
	XMFLOAT3 currentRot = DXHelper::QuaternionToEuler(rotationQuaternion, degrees);
	SetLocalRotation(currentRot.x + xOffset, currentRot.y + yOffset, currentRot.z + zOffset, degrees);
}

void TransformComponent::LocalScale(XMFLOAT3 offset) {
	SetLocalScale(DXHelper::Add(scale, offset));
}

void TransformComponent::LocalScale(float xOffset, float yOffset, float zOffset) {
	SetLocalScale(DXHelper::Add(scale, xOffset, yOffset, zOffset));
}

void TransformComponent::LocalTrack(float offset) {
	XMFLOAT3 f = GetForward();
	LocalMove(f.x * offset, f.y * offset, f.z * offset);
}

void TransformComponent::LocalStrafe(float offset) {
	XMFLOAT3 r = GetRight();
	LocalMove(r.x * offset, r.y * offset, r.z * offset);
}

void TransformComponent::RotateAboutGlobalUp(float angle) {
	XMVECTOR outGlobalUp = XMLoadFloat4(&DXHelper::UpVector4), outGlobalUpQuat = XMQuaternionRotationNormal(outGlobalUp, angle);
	XMFLOAT4 outGlobalUpRot = XMFLOAT4(0, 0, 0, 0);
	XMStoreFloat4(&outGlobalUpRot, outGlobalUpQuat);
	rotationQuaternion = DXHelper::CombineQuaternions(rotationQuaternion, outGlobalUpRot);

	updateLocal();
}

string TransformComponent::GetTypename() const {
	return typeof<TransformComponent>::name();
}

TransformComponent * TransformComponent::GetParent() {
	return parent;
}

void TransformComponent::SetParent(TransformComponent * parent, bool maintainWorldPosition) {
	XMFLOAT3 newLocal = GetLocalPosition();
	if(this->parent != NULL) {
		if(this->parent->children.size() > 0) {
			auto iter = this->parent->children.cbegin();
			for(; iter != this->parent->children.cend(); iter++) {
				if((*iter) == this)
					break;
			}

			if(iter != this->parent->children.cend())
				this->parent->children.erase(iter);
		}

		if(maintainWorldPosition) {
			XMFLOAT3 currWorldPos = GetWorldPosition();
			XMFLOAT3 newParentWorldPos = parent->GetWorldPosition();
			newLocal = XMFLOAT3(currWorldPos.x - newParentWorldPos.x, currWorldPos.y - newParentWorldPos.y, currWorldPos.z - newParentWorldPos.z);
		}
	}

	this->parent = parent;
	parent->children.push_back(this);

	SetLocalPosition(newLocal);

	updateLocal();
}

XMFLOAT4X4 TransformComponent::GetWorld() const {
	return world;
}

XMFLOAT4X4 * TransformComponent::GetWorldPtr() {
	return &world;
}

XMFLOAT3 TransformComponent::GetLocalPosition() const {
	return position;
}

XMFLOAT4 TransformComponent::GetLocalRotationQuaternion() const {
	return rotationQuaternion;
}

XMFLOAT3 TransformComponent::GetLocalScale() const {
	return scale;
}

XMFLOAT3 * TransformComponent::GetLocalPositionPtr() {
	return &position;
}

XMFLOAT4 * TransformComponent::GetLocalRotationQuaternionPtr() {
	return &rotationQuaternion;
}

XMFLOAT3 * TransformComponent::GetLocalScalePtr() {
	return &scale;
}

XMFLOAT3 TransformComponent::GetForward() {
	return (doDecomp() ? forward : DXHelper::InfinityVector3);
}

XMFLOAT3 * TransformComponent::GetForwardPtr() {
	return (doDecomp() ? &forward : NULL);
}

XMFLOAT3 TransformComponent::GetRight() {
	return (doDecomp() ? right : DXHelper::InfinityVector3);
}

XMFLOAT3 * TransformComponent::GetRightPtr() {
	return (doDecomp() ? &right : NULL);
}

XMFLOAT3 TransformComponent::GetWorldPosition() {
	return (doDecomp() ? worldPos : DXHelper::InfinityVector3);
}

XMFLOAT4 TransformComponent::GetWorldRotationQuaternion() {
	return (doDecomp() ? worldRotationQuaternion : DXHelper::InfinityVector4);
}

XMFLOAT3 TransformComponent::GetWorldScale() {
	return (doDecomp() ? worldScl : DXHelper::InfinityVector3);
}

XMFLOAT3 * TransformComponent::GetWorldPositionPtr() {
	return (doDecomp() ? &worldPos : NULL);
}

XMFLOAT4 * TransformComponent::GetWorldRotationQuaternionPtr() {
	return (doDecomp() ? &worldRotationQuaternion : NULL);
}

XMFLOAT3 * TransformComponent::GetWorldScalePtr() {
	return (doDecomp() ? &worldScl : NULL);
}

void TransformComponent::SetLocal(XMFLOAT3 position, XMFLOAT4 rotationQuaternion, XMFLOAT3 scale) {
	this->position = position;
	this->rotationQuaternion = rotationQuaternion;
	this->scale = scale;

	updateLocal();
}

void TransformComponent::SetLocal(float posX, float posY, float posZ, float rotX, float rotY, float rotZ, float sclX, float sclY, float sclZ, bool degrees) {
	position = XMFLOAT3(posX, posY, posZ);
	scale = XMFLOAT3(sclX, sclY, sclZ);

	SetLocalRotation(rotX, rotY, rotZ, degrees);
}

void TransformComponent::SetLocalPosition(XMFLOAT3 position) {
	this->position = position;

	updateLocal();
}

void TransformComponent::SetLocalPosition(float posX, float posY, float posZ) {
	position = XMFLOAT3(posX, posY, posZ);

	updateLocal();
}

void TransformComponent::SetLocalRotation(XMFLOAT4 rotationQuaternion) {
	this->rotationQuaternion = rotationQuaternion;

	updateLocal();
}

void TransformComponent::SetLocalRotation(float rotX, float rotY, float rotZ, bool degrees) {
	XMVECTOR rotQuat = (degrees ? XMQuaternionRotationRollPitchYaw(DXHelper::DegreesToRadians(rotX), DXHelper::DegreesToRadians(rotY), DXHelper::DegreesToRadians(rotZ)) : XMQuaternionRotationRollPitchYaw(rotX, rotY, rotZ));
	XMStoreFloat4(&rotationQuaternion, rotQuat);

	updateLocal();
}

void TransformComponent::SetLocalScale(XMFLOAT3 scale) {
	this->scale = scale;

	updateLocal();
}

void TransformComponent::SetLocalScale(float sclX, float sclY, float sclZ) {
	scale = XMFLOAT3(sclX, sclY, sclZ);

	updateLocal();
}

void TransformComponent::SetForward(XMFLOAT3 forward) {
	XMFLOAT3 oldForward = this->forward;

	XMFLOAT4 toNewQuat = DXHelper::AToBQuaternion(oldForward, forward, DXHelper::UpVector3);
	rotationQuaternion = DXHelper::CombineQuaternions(rotationQuaternion, toNewQuat);

	updateLocal();
}

void TransformComponent::SetForward(float fX, float fY, float fZ) {
	SetForward(XMFLOAT3(fX, fY, fZ));
}

bool TransformComponent::HasChanged() const {
	return changed;
}