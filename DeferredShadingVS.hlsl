cbuffer DSVSProjectionBuffer : register(b0) {
	float4x4 Projection;
};

cbuffer DSVSViewBuffer : register(b1) {
	float4x4 View;
};

cbuffer DSVSWorldBuffer : register(b2) {
	float4x4 World;
};

struct VSInput {
	float3 Position			: POSITION;
	float3 Normal			: NORMAL;
	float3 Tangent			: TANGENT;
	float2 Texel			: TEXCOORD;
};

struct VSOutput {
	float4		Position	: SV_POSITION;
	float4		WVPosition	: WV_POSITION;
	float2		Texel		: TEXCOORD0;
	float3x3	TBN			: TBNMATRIX;
};

VSOutput main(VSInput input) {
	VSOutput output = (VSOutput)0;

	float4 pos = float4(input.Position, 1.0f);
	pos = mul(pos, transpose(World));
	pos = mul(pos, transpose(View));
	output.WVPosition = pos;

	pos = mul(pos, transpose(Projection));
	output.Position = pos;

	float3 Binormal = cross(input.Normal, input.Tangent);

	output.TBN[0] = mul(input.Tangent, transpose((float3x3)World));
	output.TBN[0] = normalize(output.TBN[0]);

	output.TBN[1] = mul(Binormal, transpose((float3x3)World));
	output.TBN[1] = normalize(output.TBN[1]);

	output.TBN[2] = mul(input.Normal, transpose((float3x3)World));
	output.TBN[2] = normalize(output.TBN[2]);

	output.Texel = input.Texel;

	return output;
}