Texture2D Diffuse : register(t0);

SamplerState TextureSampler : register(s0);

cbuffer PSMaterialBuffer : register(b0) {
	float4 MaterialDiffuse;
	uniform bool IsTextured;
	float3 _b0Buffer;
};

struct VOut {
	float4		Position	: SV_POSITION;
	float4		WVPosition	: WV_POSITION;
	float2		Texel		: TEXCOORD0;
	float3x3	TBN			: TBN;
};

float4 main(VOut inputPixel) : SV_TARGET {
	if(IsTextured)
		return Diffuse.Sample(TextureSampler, inputPixel.Texel);
	else {
		float3 normal = (inputPixel.TBN[2] + 1.0f) * 0.5f;
		return MaterialDiffuse * float4(normal, 1.0f);
	}
}