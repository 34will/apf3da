#include "Texture.h"
#include <limits>

const Colour Colour::NullColour = { FLT_MAX, FLT_MAX, FLT_MAX, FLT_MAX };

bool Colour::IsNullColour(const Colour & c) {
	return (c.R == FLT_MAX && c.G == FLT_MAX && c.B == FLT_MAX && c.A == FLT_MAX);
}

Colour & Colour::operator=(const unsigned char & rhs) {
	this->R = ((float)rhs / 255.0f);
	this->G = ((float)rhs / 255.0f);
	this->B = ((float)rhs / 255.0f);
	this->A = 1.0f;

	return *this;
}

Texture::Texture(const string & path) : path(path) { }

Texture::~Texture() {
	Cleanup();
}

void Texture::Cleanup() {
	path = "";
}