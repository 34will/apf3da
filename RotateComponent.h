#ifndef ROTATE_COMPONENT_H
#define ROTATE_COMPONENT_H

#include <DirectXMath.h>
#include "Component.h"
#include "TransformComponent.h"
#include "ComponentRegistrar.h"

using namespace std;
using namespace DirectX;
using namespace tinyxml2;

class RotateComponent : public Component {
private:
	TransformComponent * transform;
	float prevRot, speed;

	RotateComponent();

public:
	~RotateComponent();

	bool Initialise(XMLElement * init);

	void Start();
	void Shutdown();

	void Update(float dt);

	string GetTypename() const;

	SetupComponentType(RotateComponent);
};

DeclareType(RotateComponent);

#endif


