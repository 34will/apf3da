#include <windows.h>
#include "WindowsApp.h"
#include "DirectX11App.h"
#include "tinyxml2.h"
#include "Timer.h"

WindowsApp wa;
DirectX11App dxa;
Timer t;

void ResizeWindow(HWND hWnd) {
    if(FAILED(dxa.ResizeWindow(hWnd)))
        PostQuitMessage(0);
}

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	wa = WindowsApp();
    wa.resize = ResizeWindow;

	tinyxml2::XMLDocument doc;
    doc.LoadFile("config.xml");

    if(FAILED(wa.InitWindow(hInstance, nCmdShow, doc.RootElement()->FirstChildElement("Window"))))
		return 0;

	HRESULT hr = dxa.InitDevice(wa.GetHInst(), wa.GetHwnd(), doc.RootElement()->FirstChildElement("DirectX"));

    if(FAILED(hr)) {
		dxa.CleanupDevice();
		return 0;
	}

	hr = dxa.InitScene(doc.RootElement()->FirstChildElement("Scene"));
	
    if(FAILED(hr)) {
		dxa.CleanupDevice();
		return 0;
	}

	MSG msg = { 0 };
	t = Timer(true);

	while(WM_QUIT != msg.message) {
		if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		} else {
            dxa.Update(t.Update().seconds);
			dxa.Render();
		}
	}

	dxa.CleanupDevice();

	return(int)msg.wParam;
}