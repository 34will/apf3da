#include "SceneObject.h"
#include "Component.h"

SceneObject::SceneObject(string name) : name(name), components(), componentsIterator() { }

SceneObject::~SceneObject() { }

void SceneObject::AddComponent(Component * c) {
	string typeName = c->GetTypename();
	unordered_map<string, vector<Component *>>::iterator found = components.find(typeName);

	if(found == components.end())
		components[typeName] = { c };
	else
		found->second.push_back(c);

	componentsIterator.push_back(c);

	c->setSceneObject(this);
}

void SceneObject::Start() {
	for(auto c : componentsIterator)
		c->Start();
}

void SceneObject::Shutdown() {
	for(auto c : componentsIterator)
		c->Shutdown();
}

void SceneObject::Update(float dt) {
	for(auto c : componentsIterator)
		c->Update(dt);
}

void SceneObject::Draw(ID3D11DeviceContext * dX11DeviceContext, Pass * pass, CameraComponent * currentCam) {
	for(auto c : componentsIterator)
		c->Draw(dX11DeviceContext, pass, currentCam);
}
