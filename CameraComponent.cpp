#include "CameraComponent.h"
#include "SceneObject.h"
#include "SceneGraph.h"
#include "DirectX11App.h"

RegisterComponentType(CameraComponent);

CameraComponent::CameraComponent() : Component(), transform(NULL) {}

CameraComponent::~CameraComponent() {}

bool CameraComponent::Initialise(XMLElement * init) {
	if(init == NULL)
		return false;

	transform = sceneObject->GetComponent<TransformComponent>();

	float upX = 0, upY = 0, upZ = 0;

	XMLError err = init->QueryFloatAttribute("upX", &upX);
	if(err != XML_SUCCESS)
		return false;

	err = init->QueryFloatAttribute("upY", &upY);
	if(err != XML_SUCCESS)
		return false;

	err = init->QueryFloatAttribute("upZ", &upZ);
	if(err != XML_SUCCESS)
		return false;

	SetUp(upX, upY, upZ);

	float aspectRatio = DirectX11App::Current()->GetAspectRatio(), fieldOfView = 0, nearZ = 0, farZ = 0;

	err = init->QueryFloatAttribute("nearZ", &nearZ);
	if(err != XML_SUCCESS)
		return false;

	err = init->QueryFloatAttribute("farZ", &farZ);
	if(err != XML_SUCCESS)
		return false;

	err = init->QueryFloatAttribute("fov", &fov);
	if(err != XML_SUCCESS) {
		string fov = init->Attribute("fov");

		if(fov == "PI")
			fieldOfView = XM_PI;
		else if(fov == "PIDIV2")
			fieldOfView = XM_PIDIV2;
		else if(fov == "PIDIV4")
			fieldOfView = XM_PIDIV4;
		else
			return false;
	}

	SetProjection(fieldOfView, aspectRatio, nearZ, farZ);

	return true;
}

void CameraComponent::Start() {}

void CameraComponent::Shutdown() {}

void CameraComponent::updateView() {
	XMVECTOR outEye = XMLoadFloat3(transform->GetWorldPositionPtr()), outAt = XMLoadFloat3(transform->GetForwardPtr()), outUp = XMLoadFloat3(&up);
	XMMATRIX outView = XMMatrixLookToLH(outEye, outAt, outUp);
	XMStoreFloat4x4(&view, outView);

	updateViewProjectionInverse();
}

void CameraComponent::updateProjection() {
	XMMATRIX outProjection = XMMatrixPerspectiveFovLH(fov, aspect, nearPlane, farPlane);
	XMStoreFloat4x4(&projection, outProjection);
	XMMATRIX outOrthographic = XMMatrixOrthographicLH((float)DirectX11App::Current()->GetWidth(), (float)DirectX11App::Current()->GetHeight(), nearPlane, farPlane);
	XMStoreFloat4x4(&ortho, outOrthographic);

	updateViewProjectionInverse();
}

void CameraComponent::updateViewProjectionInverse() {
	XMMATRIX outMatrix, outProjection = XMLoadFloat4x4(&projection), outView = XMLoadFloat4x4(&view);
	XMVECTOR det = XMMatrixDeterminant(outProjection);
	outMatrix = XMMatrixInverse(&det, outProjection);
	XMStoreFloat4x4(&inverseProjection, outMatrix);

	det = XMMatrixDeterminant(outView);
	outMatrix = XMMatrixInverse(&det, outView);
	XMStoreFloat4x4(&inverseView, outMatrix);

	outMatrix = XMMatrixMultiply(outView, outProjection);
	det = XMMatrixDeterminant(outMatrix);
	outMatrix = XMMatrixInverse(&det, outMatrix);
	XMStoreFloat4x4(&inverseViewProjection, outMatrix);

	XMFLOAT4X4 matrix, tempProj = projection;

	float zMinimum = -tempProj._43 / tempProj._33;
	float r = farPlane / (farPlane - zMinimum);
	tempProj._33 = r;
	tempProj._43 = -r * zMinimum;

	XMMATRIX outTempProjection = XMLoadFloat4x4(&tempProj);
	outMatrix = XMMatrixMultiply(outView, outTempProjection);
	XMStoreFloat4x4(&matrix, outMatrix);

	XMVECTOR outPlane;

	viewPlanes[0].x = matrix._14 + matrix._13;
	viewPlanes[0].y = matrix._24 + matrix._23;
	viewPlanes[0].z = matrix._34 + matrix._33;
	viewPlanes[0].w = matrix._44 + matrix._43;

	outPlane = XMLoadFloat4(&viewPlanes[0]);
	outPlane = XMPlaneNormalize(outPlane);
	XMStoreFloat4(&viewPlanes[0], outPlane);

	viewPlanes[1].x = matrix._14 - matrix._13;
	viewPlanes[1].y = matrix._24 - matrix._23;
	viewPlanes[1].z = matrix._34 - matrix._33;
	viewPlanes[1].w = matrix._44 - matrix._43;

	outPlane = XMLoadFloat4(&viewPlanes[1]);
	outPlane = XMPlaneNormalize(outPlane);
	XMStoreFloat4(&viewPlanes[1], outPlane);

	viewPlanes[2].x = matrix._14 + matrix._11;
	viewPlanes[2].y = matrix._24 + matrix._21;
	viewPlanes[2].z = matrix._34 + matrix._31;
	viewPlanes[2].w = matrix._44 + matrix._41;

	outPlane = XMLoadFloat4(&viewPlanes[2]);
	outPlane = XMPlaneNormalize(outPlane);
	XMStoreFloat4(&viewPlanes[2], outPlane);

	viewPlanes[3].x = matrix._14 - matrix._11;
	viewPlanes[3].y = matrix._24 - matrix._21;
	viewPlanes[3].z = matrix._34 - matrix._31;
	viewPlanes[3].w = matrix._44 - matrix._41;

	outPlane = XMLoadFloat4(&viewPlanes[3]);
	outPlane = XMPlaneNormalize(outPlane);
	XMStoreFloat4(&viewPlanes[3], outPlane);

	viewPlanes[4].x = matrix._14 - matrix._12;
	viewPlanes[4].y = matrix._24 - matrix._22;
	viewPlanes[4].z = matrix._34 - matrix._32;
	viewPlanes[4].w = matrix._44 - matrix._42;

	outPlane = XMLoadFloat4(&viewPlanes[4]);
	outPlane = XMPlaneNormalize(outPlane);
	XMStoreFloat4(&viewPlanes[4], outPlane);

	viewPlanes[5].x = matrix._14 + matrix._12;
	viewPlanes[5].y = matrix._24 + matrix._22;
	viewPlanes[5].z = matrix._34 + matrix._32;
	viewPlanes[5].w = matrix._44 + matrix._42;

	outPlane = XMLoadFloat4(&viewPlanes[5]);
	outPlane = XMPlaneNormalize(outPlane);
	XMStoreFloat4(&viewPlanes[5], outPlane);
}

string CameraComponent::GetTypename() const {
	return typeof<CameraComponent>::name();
}

XMFLOAT3 CameraComponent::GetAt() const {
	return transform->GetForward();
}

XMFLOAT3 * CameraComponent::GetAtPtr() {
	return transform->GetForwardPtr();
}

XMFLOAT3 CameraComponent::GetEye() const {
	return transform->GetWorldPosition();
}

XMFLOAT3 * CameraComponent::GetEyePtr() {
	return transform->GetWorldPositionPtr();
}

XMFLOAT3 CameraComponent::GetUp() const {
	return up;
}

XMFLOAT3 * CameraComponent::GetUpPtr() {
	return &up;
}

XMFLOAT4X4 CameraComponent::GetProjection() {
	updateProjection();

	return projection;
}

XMFLOAT4X4 * CameraComponent::GetProjectionPtr() {
	updateProjection();

	return &projection;
}

XMFLOAT4X4 CameraComponent::GetOrthographic() {
	updateProjection();

	return ortho;
}

XMFLOAT4X4 * CameraComponent::GetOrthographicPtr() {
	updateProjection();

	return &ortho;
}

XMFLOAT4X4 CameraComponent::GetInverseProjection() {
	updateProjection();

	return inverseProjection;
}

XMFLOAT4X4 * CameraComponent::GetInverseProjectionPtr() {
	updateProjection();

	return &inverseProjection;
}

XMFLOAT4X4 CameraComponent::GetInverseView() {
	updateProjection();

	return inverseView;
}

XMFLOAT4X4 * CameraComponent::GetInverseViewPtr() {
	updateProjection();

	return &inverseView;
}

XMFLOAT4X4 CameraComponent::GetInverseViewProjection() {
	updateProjection();

	return inverseViewProjection;
}

XMFLOAT4X4 * CameraComponent::GetInverseViewProjectionPtr() {
	updateProjection();

	return &inverseViewProjection;
}

XMFLOAT4X4 CameraComponent::GetView() {
	updateView();
	return view;
}

XMFLOAT4X4 * CameraComponent::GetViewPtr() {
	updateView();
	return &view;
}

XMFLOAT4X4 CameraComponent::GetWorld() const {
	return transform->GetWorld();
}

XMFLOAT4X4 * CameraComponent::GetWorldPtr() {
	return transform->GetWorldPtr();
}

void CameraComponent::SetUp(XMFLOAT3 up) {
	this->up = up;

	updateView();
}

void CameraComponent::SetUp(float upX, float upY, float upZ) {
	up = XMFLOAT3(upX, upY, upZ);

	updateView();
}

void CameraComponent::SetProjection(float fov, float aspect, float nearPlane, float farPlane) {
	this->fov = fov;
	this->aspect = aspect;
	this->nearPlane = nearPlane;
	this->farPlane = farPlane;

	updateProjection();
}

void CameraComponent::SetFieldOfView(float fov) {
	this->fov = fov;

	updateProjection();
}

void CameraComponent::SetAspectRatio(float aspect) {
	this->aspect = aspect;

	updateProjection();
}

void CameraComponent::SetNearPlane(float nearPlane) {
	this->nearPlane = nearPlane;

	updateProjection();
}

void CameraComponent::SetFarPlane(float farPlane) {
	this->farPlane = farPlane;

	updateProjection();
}

bool CameraComponent::PointInViewFrustrum(XMFLOAT3 p) {
	XMFLOAT3 data;
	XMVECTOR outData, outP = XMLoadFloat3(&p), outPlane;

	for(int i = 0; i < 6; i++) {
		outPlane = XMLoadFloat4(&viewPlanes[i]);
		outData = XMPlaneDotCoord(outPlane, outP);
		XMStoreFloat3(&data, outData);
		if(data.x < 0.0f)
			return false;
	}

	return true;
}

bool CameraComponent::PointInViewFrustrum(float x, float y, float z) {
	return PointInViewFrustrum(XMFLOAT3(x, y, z));
}

bool CameraComponent::SphereInViewFrustrum(XMFLOAT3 p, float radius) {
	XMFLOAT3 data;
	XMVECTOR outData, outP = XMLoadFloat3(&p), outPlane;

	for(int i = 0; i < 6; i++) {
		outPlane = XMLoadFloat4(&viewPlanes[i]);
		outData = XMPlaneDotCoord(outPlane, outP);
		XMStoreFloat3(&data, outData);
		if(data.x < -radius)
			return false;
	}

	return true;
}

bool CameraComponent::SphereInViewFrustrum(float x, float y, float z, float radius) {
	return SphereInViewFrustrum(XMFLOAT3(x, y, z), radius);
}

bool CameraComponent::BoundingBoxInViewFrustrum(XMFLOAT3 p, BoundingBox bb) {
	XMFLOAT3 data, point;
	XMVECTOR outData, outP, outPlane;

	for(int i = 0; i < 6; i++) {
		point = XMFLOAT3(p.x - abs(bb.minX), p.y - abs(bb.minY), p.z - abs(bb.minZ));
		outP = XMLoadFloat3(&point);
		outPlane = XMLoadFloat4(&viewPlanes[i]);
		outData = XMPlaneDotCoord(outPlane, outP);
		XMStoreFloat3(&data, outData);
		if(data.x >= 0.0f)
			continue;

		point = XMFLOAT3(p.x - abs(bb.minX), p.y - abs(bb.minY), p.z + abs(bb.maxZ));
		outP = XMLoadFloat3(&point);
		outPlane = XMLoadFloat4(&viewPlanes[i]);
		outData = XMPlaneDotCoord(outPlane, outP);
		XMStoreFloat3(&data, outData);
		if(data.x >= 0.0f)
			continue;

		point = XMFLOAT3(p.x - abs(bb.minX), p.y + abs(bb.maxY), p.z - abs(bb.minZ));
		outP = XMLoadFloat3(&point);
		outPlane = XMLoadFloat4(&viewPlanes[i]);
		outData = XMPlaneDotCoord(outPlane, outP);
		XMStoreFloat3(&data, outData);
		if(data.x >= 0.0f)
			continue;

		point = XMFLOAT3(p.x - abs(bb.minX), p.y + abs(bb.maxY), p.z + abs(bb.maxZ));
		outP = XMLoadFloat3(&point);
		outPlane = XMLoadFloat4(&viewPlanes[i]);
		outData = XMPlaneDotCoord(outPlane, outP);
		XMStoreFloat3(&data, outData);
		if(data.x >= 0.0f)
			continue;

		point = XMFLOAT3(p.x + abs(bb.maxX), p.y - abs(bb.minY), p.z - abs(bb.minZ));
		outP = XMLoadFloat3(&point);
		outPlane = XMLoadFloat4(&viewPlanes[i]);
		outData = XMPlaneDotCoord(outPlane, outP);
		XMStoreFloat3(&data, outData);
		if(data.x >= 0.0f)
			continue;

		point = XMFLOAT3(p.x + abs(bb.maxX), p.y - abs(bb.minY), p.z + abs(bb.maxZ));
		outP = XMLoadFloat3(&point);
		outPlane = XMLoadFloat4(&viewPlanes[i]);
		outData = XMPlaneDotCoord(outPlane, outP);
		XMStoreFloat3(&data, outData);
		if(data.x >= 0.0f)
			continue;

		point = XMFLOAT3(p.x + abs(bb.maxX), p.y + abs(bb.maxY), p.z - abs(bb.minZ));
		outP = XMLoadFloat3(&point);
		outPlane = XMLoadFloat4(&viewPlanes[i]);
		outData = XMPlaneDotCoord(outPlane, outP);
		XMStoreFloat3(&data, outData);
		if(data.x >= 0.0f)
			continue;

		point = XMFLOAT3(p.x + abs(bb.maxX), p.y + abs(bb.maxY), p.z + abs(bb.maxZ));
		outP = XMLoadFloat3(&point);
		outPlane = XMLoadFloat4(&viewPlanes[i]);
		outData = XMPlaneDotCoord(outPlane, outP);
		XMStoreFloat3(&data, outData);
		if(data.x >= 0.0f)
			continue;

		return false;
	}

	return true;
}