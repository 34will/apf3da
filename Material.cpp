#include "Material.h"
#include "WICTextureLoader.h"
#include "DXHelperStructs.h"

using namespace DirectX;

Material::Material() : name(""), ambient(0.0f, 0.0f, 0.0f, 0.0f), diffuse(0.0f, 0.0f, 0.0f, 0.0f), specular(0.0f, 0.0f, 0.0f, 0.0f), specularExponent(0.0f), isTextured(false), hasAlpha(false) {}

Material::~Material() {
	Cleanup();
}

void Material::Cleanup() {
	for(auto x : textures)
		x.second.Cleanup();

	textures.clear();

	ambient = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
	diffuse = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
	specular = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
	specularExponent = 0.0f;

	name = "";
	isTextured = false;
	hasAlpha = false;
}

void const * Material::GetVariable(const string & name) const {
	if(name == "MaterialSpecularExponent")
		return GetSpecularExponentPtr();
	else if(name == "MaterialAmbient")
		return GetAmbientPtr();
	else if(name == "MaterialDiffuse")
		return GetDiffusePtr();
	else if(name == "MaterialSpecular")
		return GetSpecularPtr();
	else if(name == "IsTextured")
		return (isTextured ? &DXHelper::ShaderBoolTrue : &DXHelper::ShaderBoolFalse);
	else if(name == "HasAlpha")
		return (hasAlpha ? &DXHelper::ShaderBoolTrue : &DXHelper::ShaderBoolFalse);
	else
		return NULL;
}

ID3D11ShaderResourceView * Material::GetTexture(const string & name) const {
	StringTextureUMap::const_iterator found = textures.find(name);
	return (found != textures.end() ? found->second.GetShaderResourceView() : NULL);
}

ID3D11ShaderResourceView * const * Material::GetTexturePtr(const string & name) const {
	StringTextureUMap::const_iterator found = textures.find(name);
	return (found != textures.end() ? found->second.GetShaderResourceViewPtr() : NULL);
}

void Material::SetTexture(const string & name, const DX11Texture & texture) {
	if(name.length() > 0) {
		textures[name] = texture;
		isTextured = true;
	}
}

void Material::SetTextures(const string * const textureNames, const DX11Texture * const textures, const unsigned int & numTextures) {
	for(unsigned int i = 0; i < numTextures; i++)
		SetTexture(textureNames[i], textures[i]);
}

bool Material::LoadTexture(const string & name, const string & path) {
	if(name.length() > 0 && path.length() > 0) {
		textures[name] = DX11Texture(path);
		isTextured = textures[name].Load();
		return isTextured;
	}
	return false;
}

bool Material::LoadTextures(const string * const textureNames, const string * const texturePaths, const unsigned int & numTextures) {
	bool ret = true;
	for(unsigned int i = 0; i < numTextures; i++) {
		ret = LoadTexture(textureNames[i], texturePaths[i]);

		if(!ret)
			break;
	}

	return ret;
}