Texture2D FontTexture : register(t0);

SamplerState TextureSampler : register(s0);

cbuffer FontColourBuffer : register(b2) {
	float4 FontColour;
};

struct VSOutput {
	float4	Position		: SV_POSITION;
	float2	Texel			: TEXCOORD0;
};

float4 main(VSOutput input) : SV_TARGET {
	float4 colour = FontTexture.Sample(TextureSampler, input.Texel);

	clip(colour.a < 0.1f ? -1 : 1);

	if(colour.a != 0.0f) {
		colour.a = 1.0f;
		colour = colour * FontColour;
	}

	return colour;
}