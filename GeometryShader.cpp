#include "GeometryShader.h"

GeometryShader::GeometryShader(WCHAR * file, LPCSTR entryPoint, LPCSTR version) : Shader(file, entryPoint, version), shader(NULL) { }

GeometryShader::~GeometryShader() {
	if(shader)
		shader->Release();
}

HRESULT GeometryShader::Initialise(ID3D11Device * dX11Device) {
	ID3DBlob * shaderBlob = NULL;
	HRESULT hr = Shader::CompileShaderFromFile(file, entryPoint, version, &shaderBlob);

	if(FAILED(hr)) {
		MessageBox(NULL, L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return hr;
	}

	hr = dX11Device->CreateGeometryShader(shaderBlob->GetBufferPointer(), shaderBlob->GetBufferSize(), NULL, &shader);
	shaderBlob->Release();

	if(FAILED(hr))
		return hr;

	hr = GenerateConstantBuffers(dX11Device);

	if(FAILED(hr))
		return hr;

	return hr;
}

ID3D11GeometryShader * GeometryShader::getShaderHandle() { return shader; }

void GeometryShader::Activate(ID3D11DeviceContext * dX11DeviceContext) {
	dX11DeviceContext->GSSetShader(shader, NULL, 0);
}

void GeometryShader::SetConstantBuffers(ID3D11DeviceContext * dX11DeviceContext, const unsigned int & StartSlot, const unsigned int & NumBuffers, ID3D11Buffer * const * constantBuffers) const {
	dX11DeviceContext->GSSetConstantBuffers(StartSlot, NumBuffers, constantBuffers);
}

void GeometryShader::SetTextures(ID3D11DeviceContext * dX11DeviceContext, const unsigned int & StartSlot, const unsigned int & NumBuffers, ID3D11ShaderResourceView * const * srvs) const {
	dX11DeviceContext->GSSetShaderResources(StartSlot, NumBuffers, srvs);
}

GeometryShader& GeometryShader::operator=(const GeometryShader& other) {
	shader = other.shader;
	return *this;
}
